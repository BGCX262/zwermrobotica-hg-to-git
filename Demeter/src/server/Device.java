package server;

import java.io.IOException;
import java.util.HashSet;

import org.teleal.cling.UpnpService;
import org.teleal.cling.UpnpServiceImpl;
import org.teleal.cling.binding.LocalServiceBindingException;
import org.teleal.cling.binding.annotations.AnnotationLocalServiceBinder;
import org.teleal.cling.model.DefaultServiceManager;
import org.teleal.cling.model.ValidationException;
import org.teleal.cling.model.meta.DeviceDetails;
import org.teleal.cling.model.meta.DeviceIdentity;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.LocalService;
import org.teleal.cling.model.meta.ManufacturerDetails;
import org.teleal.cling.model.meta.ModelDetails;
import org.teleal.cling.model.types.DeviceType;
import org.teleal.cling.model.types.ServiceId;
import org.teleal.cling.model.types.ServiceType;
import org.teleal.cling.model.types.UDADeviceType;
import org.teleal.cling.model.types.UDN;

import roombacomm.RoombaCommSerial;
import services.ContextService;
import services.ControlService;
import services.SemanticService;

public class Device implements Runnable {

	public static RoombaCommSerial roombaComm;

	private static String protocol;
	private static String serialPort;

	public static void main(String[] args) throws Exception {
		// Startup of the communication with the Roomba.
		System.out.println("[DEBUG] Starting Demeter.");
		// roombaStart();
		// Start a user thread that runs the UPnP stack.
		Thread serverThread = new Thread(new Device());
		serverThread.setDaemon(false);
		serverThread.start();
	}

	public void run() {
		try {
			final UpnpService upnpService = new UpnpServiceImpl();
			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					System.out.println("[DEBUG] Stopping Demeter.");
					//roombaStop();
					upnpService.shutdown();
				}
			});
			// Add the bound local device to the register.
			upnpService.getRegistry().addDevice(createDevice());
		} catch (Exception ex) {
			System.err.println("Exception occured: " + ex);
			ex.printStackTrace(System.err);
			System.exit(1);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	LocalDevice createDevice() throws ValidationException,
			LocalServiceBindingException, IOException {

		// Fill in the name of your device e.g. Zeus. Unique!
		DeviceIdentity identity = new DeviceIdentity(
				UDN.uniqueSystemIdentifier("Demeter"));

		// Fill in the name of your device e.g. Zeus.
		DeviceType type = new UDADeviceType("Demeter", 1);

		DeviceDetails details = new DeviceDetails("Demeter",
				new ManufacturerDetails("iRobot"), new ModelDetails("520",
						"Vacuum cleaning robot.", "1.0"));

		// Adding controlService to the device.
		ServiceId serviceIDControl = new ServiceId("robot", "Control");
		ServiceType serviceTypeControl = new ServiceType("robot",
				"ControlService");
		LocalService<ControlService> controlService = new AnnotationLocalServiceBinder()
				.read(ControlService.class, serviceIDControl,
						serviceTypeControl, true, new HashSet<Class>());

		controlService.setManager(new DefaultServiceManager(controlService,
				ControlService.class));

		// Adding semanticService to the device.
		ServiceId serviceIDSemantic = new ServiceId("robot", "Semantic");
		ServiceType serviceTypeSemantic = new ServiceType("robot",
				"SemanticService");
		LocalService<SemanticService> semanticService = new AnnotationLocalServiceBinder()
				.read(SemanticService.class, serviceIDSemantic,
						serviceTypeSemantic, true, new HashSet<Class>());

		semanticService.setManager(new DefaultServiceManager(semanticService,
				SemanticService.class));
		
		// Adding contextService to the device.
		ServiceId serviceIDContext = new ServiceId("robot", "Context");
		ServiceType serviceTypeContext = new ServiceType("robot", "ContextService");
		LocalService<ContextService> contextService = new AnnotationLocalServiceBinder()
				.read(ContextService.class, serviceIDContext,
						serviceTypeContext, true, new HashSet<Class>());

		contextService.setManager(new DefaultServiceManager(contextService,
				ContextService.class));

		LocalService[] services = new LocalService[3];
		services[0] = controlService;
		services[1] = semanticService;
		services[2] = contextService;

		LocalDevice localDevice = new LocalDevice(identity, type, details,
				services);

		return localDevice;
	}

	// Startup function for communications with the Roomba.
	private static void roombaStart() {
		protocol = "OI";
		serialPort = "/dev/ttyUSB0";
		//serialPort = "COM5";
		roombaComm = new RoombaCommSerial();
		roombaComm.setProtocol(protocol);
		if (!roombaComm.connect(serialPort)) {
			System.out.println("[DEBUG] Can't connect to Roomba. Wrong port?");
			System.exit(9);
		}
		// Wake up signal.
		roombaComm.port.setRTS(false);
		roombaComm.pause(500);
		roombaComm.port.setRTS(true);
		// Debugging.
		// roombaComm.debug = true;
		roombaComm.startup();
		roombaComm.pause(100);
		roombaComm.control();
		roombaComm.pause(100);
		roombaComm.pause(500);
		roombaComm.sensors();
		roombaComm.updateSensors();
	}

	// Stop function for communications with Roomba.
	private static void roombaStop() {
		roombaComm.stop();
		roombaComm.pause(100);
		roombaComm.disconnect();
		roombaComm.pause(100);
	}
}
