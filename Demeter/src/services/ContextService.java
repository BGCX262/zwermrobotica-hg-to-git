package services;

import model.DataModel;

import org.teleal.cling.binding.annotations.UpnpAction;
import org.teleal.cling.binding.annotations.UpnpOutputArgument;
import org.teleal.cling.binding.annotations.UpnpService;
import org.teleal.cling.binding.annotations.UpnpServiceId;
import org.teleal.cling.binding.annotations.UpnpServiceType;
import org.teleal.cling.binding.annotations.UpnpStateVariable;

@UpnpService(serviceId = @UpnpServiceId("ContextService"), serviceType = @UpnpServiceType(value = "ContextService", version = 1))
public class ContextService {

	public static DataModel dataModel;

	public ContextService() {
		if (dataModel == null) {
			dataModel = DataModel.getInstance();
			dataModel.setAngle(0);
			dataModel.setX(0);
			dataModel.setY(0);
		}
	}

	public static void init() {
		dataModel = DataModel.getInstance();
		dataModel.setAngle(0);
		dataModel.setX(0);
		dataModel.setY(0);
	}

	@UpnpStateVariable(defaultValue = "0", name = "XCoord")
	private int XCoord = 0;

	@UpnpStateVariable(defaultValue = "0", name = "YCoord")
	private int YCoord = 0;

	@UpnpStateVariable(defaultValue = "0", name = "Angle")
	private int Angle = 0;

	@UpnpStateVariable(defaultValue = "0", name = "Speed")
	private int Speed = 0;

	@UpnpStateVariable(defaultValue = "0", name = "BatteryLife")
	private int BatteryLife = 70; // In procenten.

	@UpnpAction(out = @UpnpOutputArgument(name = "XCoord"))
	public int getXCoord() {
		XCoord = dataModel.getX();
		return XCoord;
	}

	@UpnpAction(out = @UpnpOutputArgument(name = "YCoord"))
	public int getYCoord() {
		YCoord = dataModel.getY();
		return YCoord;
	}

	@UpnpAction(out = @UpnpOutputArgument(name = "Angle"))
	public int getAngle() {
		Angle = dataModel.getAngle();
		return Angle;
	}

	@UpnpAction(out = @UpnpOutputArgument(name = "Speed"))
	public int getSpeed() {
		Speed = 70;
		// TODO Speed van Roomba.
		return Speed;
	}

	@UpnpAction(out = @UpnpOutputArgument(name = "BatteryLife"))
	public int getBatteryLife() {
		BatteryLife = 100;
		// TODO BatteryLife van Roomba.
		return BatteryLife;
	}

}
