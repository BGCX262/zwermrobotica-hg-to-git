package services;

import java.beans.PropertyChangeSupport;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import org.teleal.cling.binding.annotations.UpnpAction;
import org.teleal.cling.binding.annotations.UpnpInputArgument;
import org.teleal.cling.binding.annotations.UpnpOutputArgument;
import org.teleal.cling.binding.annotations.UpnpService;
import org.teleal.cling.binding.annotations.UpnpServiceId;
import org.teleal.cling.binding.annotations.UpnpServiceType;
import org.teleal.cling.binding.annotations.UpnpStateVariable;

import server.Device;

@UpnpService(serviceId = @UpnpServiceId("ControlService"), serviceType = @UpnpServiceType(value = "ControlService", version = 1))
public class ControlService {

	private final PropertyChangeSupport propertyChangeSupport;

	private Thread commandThread = null;
	private static String commandStatus = "Default";

	public ControlService() {
		ContextService.init();
		this.propertyChangeSupport = new PropertyChangeSupport(this);
	}

	public PropertyChangeSupport getPropertyChangeSupport() {
		return propertyChangeSupport;
	}

	@SuppressWarnings("unused")
	@UpnpStateVariable(name = "MoveForward")
	private int MoveForward;

	@SuppressWarnings("unused")
	@UpnpStateVariable(name = "Rotate")
	private int Rotate;

	@SuppressWarnings("unused")
	@UpnpStateVariable(name = "Tune")
	private int Tune;

	@SuppressWarnings("unused")
	@UpnpStateVariable(defaultValue = "Demeter", name = "Robot", sendEvents = true)
	private String Robot = "Demeter";

	@UpnpStateVariable(defaultValue = "Idle", name = "Status", sendEvents = true)
	private String Status = "Idle";

	@SuppressWarnings("unused")
	@UpnpStateVariable(defaultValue = "Idle", name = "Status", sendEvents = false)
	private String Stop = "Idle";

	@UpnpAction(name = "setStatus")
	public void setStatus(@UpnpInputArgument(name = "Status") String status) {
		System.out.println("[DEBUG] Calling setStatus(" + status + ").");
		String old = Status;
		Status = status;
		getPropertyChangeSupport().firePropertyChange("Status", old, Status);
	}

	@UpnpAction(out = @UpnpOutputArgument(name = "Status"))
	public String getStatus() {
		System.out.println("[DEBUG] Calling getStatus.");
		return Status;
	}

	@UpnpAction(name = "stopRoomba")
	public void setStop(){
		System.out.println("[DEBUG] Calling stop on roomba !");
		if(commandThread != null){
			commandThread.interrupt();
			Device.roombaComm.reset();
		}
	}
	
	@UpnpAction(name = "moveForward")
	public void setMoveForward(@UpnpInputArgument(name = "Distance") int distance) {
		System.out.println("[DEBUG] Calling moveForward(" + distance + ").");

		setStatus("Busy");
		
		final int finalDistance = distance;

		// End recalculation.
		
		commandThread = new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Device.roombaComm.goForward(finalDistance);
				commandStatus = "commandDone";
				recalculateLocation(finalDistance);
				setStatus("Finished");
			}
		};

		/*
		 * while niet arrived, lees bumperdinges uit.
		 */
		commandStatus = "started";
		commandThread.start();
		Thread monitorThread = new Thread() {
			@Override
			public void run() {
				long start = Calendar.getInstance().getTimeInMillis();
				while (!commandStatus.contains("commandDone")) {
					Device.roombaComm.updateSensors();
					if (Device.roombaComm.bump()) {
						commandThread.interrupt(); // stop de commandthread
						Device.roombaComm.reset(); // stop het forwardcommando
						long stop = Calendar.getInstance().getTimeInMillis();
						double time = (stop - start + 0.0) / 1000.0;
						int speed = Device.roombaComm.getSpeed()/10;
						int distanceTravelled = (int) time * speed;
						recalculateLocation(distanceTravelled);
						setStatus("Bumped");
						break; // stop de while
					}
				}
			}
		};
		
		monitorThread.start();
		System.out.println("[DEBUG] Calling moveForward finished. ");

		System.out.println("[DEBUG] Finished at " + DateFormat.getTimeInstance().format(new Date()) + ".");
	}

	@UpnpAction(name = "rotate")
	public void setRotate(@UpnpInputArgument(name = "Angle") int angle) {
		System.out.println("[DEBUG] Calling rotate(" + angle + ").");

		setStatus("Busy");

		// Recalculation of the angle.
		int startAngle = ContextService.dataModel.getAngle();

		int stopAngle = startAngle + angle;
		ContextService.dataModel.setAngle(stopAngle);
		// End recalculation.

		final int angleToRotate = angle;

		Thread command = new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Device.roombaComm.spin((int) angleToRotate);
				setStatus("Finished");
			}
		};
		command.start();
		
		/*
		 * while niet arrived, lees bumperdinges uit.
		 */
		commandStatus = "started";
		commandThread.start();
		Thread monitorThread = new Thread() {
			@Override
			public void run() {				
				while (!commandStatus.contains("commandDone")) {
					Device.roombaComm.updateSensors();
					if (Device.roombaComm.bump()) {
						commandThread.interrupt(); // stop de commandthread
						Device.roombaComm.reset(); // stop het forwardcommando												
						setStatus("Bumped");
						break; // stop de while
					}
				}
			}
		};
		
		monitorThread.start();
		System.out.println("[DEBUG] Calling rotate finished.");
		System.out.println("[DEBUG] Finished at " + DateFormat.getTimeInstance().format(new Date()) + ".");
	}
	
	private void recalculateLocation(int distance) {
		// Recalculation of position.
		double tang = Math.tan(ContextService.dataModel.getAngle() * Math.PI / 180);

		int angle_mod = ContextService.dataModel.getAngle() % 360;
		if (angle_mod < 0) {
			angle_mod = angle_mod + 360;
		}
		int x_rel;
		int y_rel;
		if (angle_mod == 90 || angle_mod == 270) {
			y_rel = distance;
			x_rel = 0;
		} else if (angle_mod == 0 || angle_mod == 180) {
			y_rel = 0;
			x_rel = distance;
		} else {
			y_rel = (int) Math.sqrt((Math.pow(distance, 2)) / (Math.pow(tang, 2) + 1));
			x_rel = (int) (y_rel * Math.abs(tang));
		}

		if (0 <= angle_mod && angle_mod <= 90) {

		} else if (angle_mod <= 180) {
			x_rel = -x_rel;
		} else if (angle_mod <= 270) {
			x_rel = -x_rel;
			y_rel = -y_rel;
		} else {
			y_rel = -y_rel;
		}

		int x_stop = ContextService.dataModel.getX() + x_rel;
		int y_stop = ContextService.dataModel.getY() + y_rel;

		ContextService.dataModel.setX(x_stop);
		ContextService.dataModel.setY(y_stop);
	}

	@UpnpAction(name = "playTune")
	public void setTune(@UpnpInputArgument(name = "Note") int note, @UpnpInputArgument(name = "Duration") int duration) {
		System.out.println("[DEBUG] Calling playTune(" + note + ", " + duration + ").");

		setStatus("Busy");

		final int finalNote = note;
		final int finalDuration = duration;
		Thread command = new Thread() {
			@Override
			public void run() {
				System.out.println("[DEBUG] Waiting for ten seconds... then playing tune.");
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// Device.roombaComm.playNote(finalNote, finalDuration); // C
				setStatus("Finished");
			}
		};
		command.start();
		System.out.println("[DEBUG] Calling playTune finished.");
	}
}
