package model;

public class DataModel {
	private int[] location;

	public DataModel() {
		this.location = new int[3];
		this.location[0] = 0;
		this.location[1] = 0;
		this.location[2] = 0;
	}

	private static DataModel myInstance;

	public static DataModel getInstance() {
		if (myInstance == null)
			myInstance = new DataModel();
		return myInstance;
	}

	public int[] getLocation() {
		return location;
	}

	public int getX() {
		return location[0];
	}

	public int getY() {
		return location[1];
	}

	public int getAngle() {
		return location[2];
	}

	public void setX(int x) {
		location[0] = x;
	}

	public void setY(int y) {
		location[1] = y;
	}

	public void setAngle(int angle) {
		location[2] = angle;
	}

	public void setBaselocation(int x_loc, int y_loc, int heading) {
		this.location[0] = x_loc;
		this.location[1] = y_loc;
		this.location[2] = heading;
	}
}
