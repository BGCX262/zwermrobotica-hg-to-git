package services;

import java.util.HashMap;

import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.meta.Action;
import org.teleal.cling.model.types.InvalidValueException;

@SuppressWarnings("rawtypes")
public class UpnpActionInvocation extends ActionInvocation {

	@SuppressWarnings("unchecked")
	public
	UpnpActionInvocation(Action action, HashMap<String, Object> arguments) {
		super(action);
		try {
			if (arguments != null) {
				for (String s : arguments.keySet()) {
					setInput(s, arguments.get(s)); // CAST NAAR INTEGER = UnsignedIntegerTwoBytesDatatype
				}
			}

		} catch (InvalidValueException ex) {
			System.out.println("[DEBUG] InvalidValueException (" + ex.getLocalizedMessage()
					+ ").");
			ex.printStackTrace();
			System.exit(1);
		}
	}
}
