package services;

import java.io.File;
import java.io.FilenameFilter;

import org.apache.commons.io.FileUtils;
import org.teleal.cling.binding.annotations.UpnpAction;
import org.teleal.cling.binding.annotations.UpnpInputArgument;
import org.teleal.cling.binding.annotations.UpnpOutputArgument;
import org.teleal.cling.binding.annotations.UpnpService;
import org.teleal.cling.binding.annotations.UpnpServiceId;
import org.teleal.cling.binding.annotations.UpnpServiceType;
import org.teleal.cling.binding.annotations.UpnpStateVariable;

@UpnpService(serviceId = @UpnpServiceId("SemanticService"), serviceType = @UpnpServiceType(value = "SemanticService", version = 1))
public class SemanticService {	
	@UpnpStateVariable(name = "HasOWL")
	private boolean HasOWL = false;

	@SuppressWarnings("unused")
	@UpnpStateVariable(name = "OWLFile")
	private String OWLFile;

	@SuppressWarnings("unused")
	@UpnpStateVariable(name = "OWLFileName")
	private String OWLFileName;

	@UpnpAction(name = "setOWL")
	public void setHasOWL(@UpnpInputArgument(name = "HasOWL") final boolean hasOWL) {
		System.out.println("[DEBUG] Calling setOWL.");
		HasOWL = hasOWL;
	}
	
	@UpnpAction(name = "hasOWL", out = @UpnpOutputArgument(name = "HasOWL"))
	public boolean getHasOWL() {
		System.out.println("[DEBUG] Calling hasOWL.");
		// Depends on present OWL-S on device.
		return HasOWL;
	}

	@UpnpAction(out = @UpnpOutputArgument(name = "OWLFile"))
	public String getOWLFile(
			@UpnpInputArgument(name = "OWLFileName") final String OWLFileName) {
		System.out.println("[DEBUG] Calling getOWLFile(" + OWLFileName + ").");

		String cwd = System.getProperty("user.dir");
		File directory = new File(cwd + "//owl//");
		
		String[] OWLFiles = directory.list(new FilenameFilter() {

			@Override
			public boolean accept(File directory, String name) {
				if (name.contains(OWLFileName + ".owl")
						&& !name.contains("searchOWL")) {
					return true;
				} else {
					return false;
				}
			}
		});

		System.out.println("[DEBUG] Matched OWL files:");
		for (String s : OWLFiles) {
			System.out.println("[DEBUG] " + s);
			String URL = cwd + "//owl//" + OWLFiles[0];
			File file = new File(URL);
			// Read the content of the file.
			try {
				String content = FileUtils.readFileToString(file);
				return content;
			} catch (Exception ex) {
				System.out.println("[DEBUG] " + ex + ".");
				ex.printStackTrace();
				// return "[DEBUG] " + ex + ".";
				return null;
			}
		}
		System.out.println("[DEBUG] No OWL file found for action" + OWLFileName
				+ ".");
		return null;
	}
}
