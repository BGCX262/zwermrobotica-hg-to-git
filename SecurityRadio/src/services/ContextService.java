package services;



import org.teleal.cling.binding.annotations.UpnpAction;
import org.teleal.cling.binding.annotations.UpnpOutputArgument;
import org.teleal.cling.binding.annotations.UpnpService;
import org.teleal.cling.binding.annotations.UpnpServiceId;
import org.teleal.cling.binding.annotations.UpnpServiceType;
import org.teleal.cling.binding.annotations.UpnpStateVariable;



@UpnpService(serviceId = @UpnpServiceId("ContextService"), serviceType = @UpnpServiceType(value = "ContextService", version = 1))
public class ContextService {

	
	@UpnpStateVariable(defaultValue = "0", name = "Speed")
	private int Speed = 0;

	@UpnpStateVariable(defaultValue = "0", name = "BatteryLife")
	private int BatteryLife = 70; // In procenten.

	

	@UpnpAction(out = @UpnpOutputArgument(name = "Speed"))
	public int getSpeed() {
		Speed = 0;
		return Speed;
	}

	@UpnpAction(out = @UpnpOutputArgument(name = "BatteryLife"))
	public int getBatteryLife() {
		return BatteryLife;
	}

}
