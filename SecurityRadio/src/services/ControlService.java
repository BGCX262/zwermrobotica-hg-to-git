package services;

import java.beans.PropertyChangeSupport;
import java.util.HashMap;

import org.teleal.cling.binding.annotations.UpnpAction;
import org.teleal.cling.binding.annotations.UpnpInputArgument;
import org.teleal.cling.binding.annotations.UpnpOutputArgument;
import org.teleal.cling.binding.annotations.UpnpService;
import org.teleal.cling.binding.annotations.UpnpServiceId;
import org.teleal.cling.binding.annotations.UpnpServiceType;
import org.teleal.cling.binding.annotations.UpnpStateVariable;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Action;
import org.teleal.cling.model.types.UnsignedIntegerTwoBytesDatatype;

import device.RadioDevice;



@UpnpService(serviceId = @UpnpServiceId("ControlService"), serviceType = @UpnpServiceType(value = "ControlService", version = 1))
public class ControlService {

	private final PropertyChangeSupport propertyChangeSupport;
	
	public ControlService() {
		this.propertyChangeSupport = new PropertyChangeSupport(this);
	}

	/*@UpnpStateVariable(defaultValue = "Idle", name = "Status", sendEvents = true)
	private String Status = "Idle";*/
	
	public PropertyChangeSupport getPropertyChangeSupport() {
		return propertyChangeSupport;
	}
/*
	@UpnpAction(name = "setStatus")
	public void setStatus(@UpnpInputArgument(name = "Status") String status) {
		System.out.println("[DEBUG] Calling setStatus(" + status + ").");
		String old = Status;
		Status = status;
		getPropertyChangeSupport().firePropertyChange("Status", old, Status);
	}
	*/
	@SuppressWarnings("unused")
	@UpnpStateVariable(name = "Volume")
	private int Volume;
	
	@UpnpAction(name = "SetVolume")
	public void setVolume(@UpnpInputArgument(name = "Volume") int volume) {
		System.out.println("[DEBUG] Calling SetVolume ( " + volume + " ).");
		
		final Integer fvolume = volume;
		
		Thread command = new Thread() {
			@Override
			public void run() {
						
				System.out.println(RadioDevice.upnpRadio.getDisplayString());
				org.teleal.cling.model.meta.Service control = null;
				for(org.teleal.cling.model.meta.Service s : RadioDevice.upnpRadio.getServices()){
					System.out.println(s.getServiceId().getId() +  " , " + s.getServiceId().getNamespace());
					if(s.getServiceId().getId().contains("Control")){
						control = s;
						break;
					}
				}
				
				/*
				 * service terugkrijgen werkt al !
				 */
				
				Action setVolume = null;
				
				for(Action action : control.getActions()){
					System.out.println(action.getName());
					if(action.getName().contains("Volume")){
						setVolume = action;
						break;
					}
				}
				System.out.println("action gotten : " + setVolume.getName());
				
				HashMap<String,Object> inputs = new HashMap<String,Object>();
				String volstring = fvolume.toString();
				UnsignedIntegerTwoBytesDatatype type = new UnsignedIntegerTwoBytesDatatype();
				inputs.put("DesiredVolume", type.valueOf(volstring));
				executeAction(setVolume, inputs, "bleh");
				//setStatus("Finished");				
			}
		};
		command.start();
		System.out.println("[DEBUG] Calling SetVolume() finished.");
	}	 

	@UpnpAction(name = "GetVolume",out = @UpnpOutputArgument(name = "Volume"))
	public void getVolume() {
		System.out.println("[DEBUG] Calling GetVolume().");
		
		Thread command = new Thread() {
			@Override
			public void run() {
									
				
				
			}
		};
		command.start();
		System.out.println("[DEBUG] Calling GetVolume finished.");
	}
	
	@SuppressWarnings("rawtypes")
	private void executeAction(Action action, HashMap<String, Object> inputArguments, String robotName) {

		System.out.println("starting action !");
		@SuppressWarnings("rawtypes")
		UpnpActionInvocation invocation = new UpnpActionInvocation(action, inputArguments);
		final String kopieName = robotName;
		// Executes asynchronous in the background
		RadioDevice.upnpService.getControlPoint().execute(new ActionCallback(invocation) {

			@Override
			public void success(ActionInvocation invocation) {			
				System.out.println(kopieName + " has successfully called " + invocation.getAction().getName() + " wait for the event to get next action !");
			}

			@Override
			public void failure(ActionInvocation invocation, UpnpResponse operation, String defaultMsg) {
				System.err.println(defaultMsg);
				
			}
		});
	}

}
