package device;

import java.util.HashSet;

import org.teleal.cling.UpnpService;
import org.teleal.cling.UpnpServiceImpl;
import org.teleal.cling.binding.annotations.AnnotationLocalServiceBinder;
import org.teleal.cling.model.DefaultServiceManager;
import org.teleal.cling.model.ValidationException;
import org.teleal.cling.model.message.header.STAllHeader;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.DeviceDetails;
import org.teleal.cling.model.meta.DeviceIdentity;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.LocalService;
import org.teleal.cling.model.meta.ManufacturerDetails;
import org.teleal.cling.model.meta.ModelDetails;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.model.types.DeviceType;
import org.teleal.cling.model.types.ServiceId;
import org.teleal.cling.model.types.ServiceType;
import org.teleal.cling.model.types.UDADeviceType;
import org.teleal.cling.model.types.UDN;
import org.teleal.cling.registry.DefaultRegistryListener;
import org.teleal.cling.registry.Registry;
import org.teleal.cling.registry.RegistryListener;

import services.ContextService;
import services.ControlService;
import services.SemanticService;




public class RadioDevice implements Runnable{

	public static UpnpService upnpService = new UpnpServiceImpl();
	public static Device upnpRadio = null;
	
	public RadioDevice(){
		Runtime.getRuntime().addShutdownHook(new Thread() {

			@Override
			public void run() {
				upnpService.shutdown();
			}
		});

		// Add a listener for device registration events.
		upnpService.getRegistry().addListener(createRegistryListener(upnpService));

		// Broadcast a search message (STAllHeader) for all devices.
		upnpService.getControlPoint().search(new STAllHeader());
	}
	
	private static RegistryListener createRegistryListener(final UpnpService upnpService) {
		return new DefaultRegistryListener() {

			@Override
			public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
				try {
					// Make unique string out of the device, needed to map the
					// device. We assume that every friendly name of a device is
					// unique.
					System.out.println("[DEBUG] Device " + device.getDisplayString() + " came online.");

					if (device.getDisplayString().contains("NP 2900")) {
						System.out.println("[DEBUG] We have found our security radio.");
						DeviceDetails details = device.getDetails();
						String friendlyName = details.getFriendlyName();
						if(upnpRadio == null){
							upnpRadio = device;
							System.out.println("device : " + device.getDisplayString());
							Device dev = createRadioDevice();
							registry.addDevice((LocalDevice) dev);
						}
						
					}
				} catch (Exception ex) {
					System.out.println("[DEBUG] Exception (" + ex.getLocalizedMessage() + ").");
					ex.printStackTrace();
				}
			}

		};
	}
	
	@SuppressWarnings("unchecked")
	private static Device createRadioDevice() throws ValidationException {

		// Fill in the name of your device e.g. Zeus. Unique!
		DeviceIdentity identity = new DeviceIdentity(UDN.uniqueSystemIdentifier("radio"));

		// Fill in the name of your device e.g. Zeus.
		DeviceType type = new UDADeviceType("radio", 1);

		DeviceDetails details = new DeviceDetails("SecuRadio", new ManufacturerDetails("SecurityRadio"), new ModelDetails("model 2", "UPnP Radio", "1.0"));

		// Adding controlService to the device.
		ServiceId serviceIDControl = new ServiceId("robot", "Control");
		ServiceType serviceTypeControl = new ServiceType("robot", "ControlService");
		@SuppressWarnings("unchecked")
		LocalService<ControlService> controlService = new AnnotationLocalServiceBinder().read(ControlService.class, serviceIDControl, serviceTypeControl, true, new HashSet<Class>());

		controlService.setManager(new DefaultServiceManager(controlService, ControlService.class));

		// Adding semanticService to the device.
		ServiceId serviceIDSemantic = new ServiceId("radio", "Semantic");
		ServiceType serviceTypeSemantic = new ServiceType("radio", "Semantic");
		LocalService<SemanticService> semanticService = new AnnotationLocalServiceBinder().read(SemanticService.class, serviceIDSemantic, serviceTypeSemantic, true, new HashSet<Class>());

		semanticService.setManager(new DefaultServiceManager(semanticService, SemanticService.class));
		

		// Adding contextService to the device.
		ServiceId serviceIDContext = new ServiceId("robot", "Context");
		ServiceType serviceTypeContext = new ServiceType("robot", "Context");
		LocalService<ContextService> contextService = new AnnotationLocalServiceBinder().read(ContextService.class, serviceIDContext, serviceTypeContext, true, new HashSet<Class>());

		contextService.setManager(new DefaultServiceManager(contextService, ContextService.class));
		
		LocalService[] services = new LocalService[3];
		services[0] = controlService;
		services[1] = semanticService;
		services[2] = contextService;

		LocalDevice localDevice = new LocalDevice(identity, type, details, services);
		return localDevice;
	}
	
	public static void main(String[] args){
		RadioDevice rd = new RadioDevice();
		Thread serverThread = new Thread(new RadioDevice());
		serverThread.setDaemon(false);
		serverThread.start();
	}

	@Override
	public void run() {
		try{
		upnpService = new UpnpServiceImpl();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				System.out.println("[DEBUG] Stopping Zeus.");				
				upnpService.shutdown();
			}
		});
		// Add the bound local device to the register.
		//upnpService.getRegistry().addDevice(createDevice());
		}catch(Exception ex){
			System.out.println("Exception occured");
			ex.printStackTrace();
		}
	}
	
}
