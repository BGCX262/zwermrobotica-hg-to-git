package tasks.taskAI;

import ga.RobotFitnessFunction;
import ga.bo.Robot;
import impl.owls.process.constructs.SequenceImpl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.jgap.FitnessFunction;
import org.jgap.Gene;
import org.jgap.Genotype;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.impl.DefaultConfiguration;
import org.jgap.impl.IntegerGene;
import org.mindswap.owl.OWLFactory;
import org.mindswap.owl.OWLKnowledgeBase;
import org.mindswap.owls.process.CompositeProcess;
import org.mindswap.owls.process.ControlConstruct;
import org.mindswap.owls.process.ControlConstructList;
import org.mindswap.owls.process.Perform;
import org.mindswap.owls.process.Process;
import org.mindswap.owls.service.Service;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionArgumentValue;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Action;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.types.ServiceId;
import org.teleal.cling.model.types.UnsignedIntegerTwoBytesDatatype;

import tasks.bo.CheckOutPlaceTask;
import tasks.bo.CoverageTask;
import tasks.bo.DefaultTask;
import tasks.bo.Task;
import upnp.ControlPoint;
import upnp.UpnpActionInvocation;

public class TaskAI {
	
	private static final int MAX_ALLOWED_EVOLUTIONS = 100;
	public static ControlPoint controlPoint;
	private static HashMap<String, Task> predefinedTasks = new HashMap<String, Task>();
	private final static String COVERAGE_TASK = "Patrol.owl";
	private final static String CHECK_OUT = "CheckOutPlace.owl";
	private final static String TASKNAME = COVERAGE_TASK;
	private final static String[] PARAMETERS = { "BatteryLife", "Speed" };
	/*
	 * tasks mapped onto its taskname
	 */
	private static HashMap<String, Task> tasks = new HashMap<String, Task>();
	private static ArrayList<String> priorityTasks = new ArrayList<String>();

	/*
	 * robot states holds the states of robots, i.e. Zeus -> true => Zeus is
	 * available to be assigned to a task
	 */
	
	public TaskAI() {

	}

	public static void init() throws InterruptedException {
		
		/*
		 * add predefined tasks
		 */
		predefinedTasks.put("Scan", CoverageTask.init());
		predefinedTasks.put("CheckOutPlace", CheckOutPlaceTask.init());
		/*
		 * read tasks to complete -> init tasks
		 */
		String taskURI = "http://localhost:9090/tasks/" + TASKNAME;
		
		try {
			HashMap<String, ArrayList<Process>> sequenceOfTasks = addTask(taskURI);
			/*
			 * testredenen 
			 */
			
			//ArrayList<Process> tasks = sequenceOfTasks.get("0_GoForwardProcess");
			//System.out.println("tasks : " + tasks.size());
			//long start = Calendar.getInstance().getTimeInMillis();
			//ControlPoint.testMatchingDuration(tasks);
			//long stop = Calendar.getInstance().getTimeInMillis();
			//System.out.println("duration : " + (stop-start));
			/*
			 * einde testdinges
			 */
	
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}finally{
			controlPoint = new ControlPoint(); //start cp nadat de taak is ingelezen
		}
	}

	public static HashMap<String, ArrayList<Process>> addTask(String URI) throws IOException, URISyntaxException{
		HashMap<String, ArrayList<Process>> sequenceOfTasks = new HashMap<String,ArrayList<Process>>();
		sequenceOfTasks = readTask(URI);
		for (String taskName : sequenceOfTasks.keySet()) {
			Task task = findPredefinedEntry(taskName);
			if (task != null) {
				tasks.put(taskName, task);
				tasks.get(taskName).setTasks(sequenceOfTasks.get(taskName));
				if (priorityTasks.contains(taskName)) {
					tasks.get(taskName).setIsPrior(true);
				}
			} else {
				tasks.put(taskName, new DefaultTask(taskName));
				if (taskName.contains("lay")) { // if the action is
												// playtune, inputs are 70
												// (note) and 1000 duration
					LinkedList<HashMap<String, Object>> inputs = new LinkedList<HashMap<String, Object>>();
					HashMap<String, Object> inputparams = new HashMap<String, Object>();
					inputparams.put("Duration", 1000);
					inputparams.put("Note", 70);
					inputs.add(inputparams);
					((DefaultTask) tasks.get(taskName)).setInputs(inputs);
					if (priorityTasks.contains(taskName)) {
						tasks.get(taskName).setIsPrior(true);
					}
				}else if(taskName.contains("Radio")){
					
					LinkedList<HashMap<String, Object>> inputs = new LinkedList<HashMap<String, Object>>();
					HashMap<String, Object> inputparams = new HashMap<String, Object>();
					inputparams.put("Volume",new Integer(14));
					inputs.add(inputparams);
					((DefaultTask) tasks.get(taskName)).setInputs(inputs);
					if (priorityTasks.contains(taskName)) {
						tasks.get(taskName).setIsPrior(true);
					}					
				}
				tasks.get(taskName).setTasks(sequenceOfTasks.get(taskName));
			}
		}
		return sequenceOfTasks;
	}
	
	public static void main(String[] args) throws InterruptedException {
		init();
		ArrayList<HashMap<String, Object>> input = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> inputs = new HashMap<String, Object>();
		inputs.put("Distance", 1000);
		System.out.println("TaskAI ready to perform tasks !");
		// Thread.sleep(5000);
		// performTask("http://localhost:9090/tasks/nemesis2.owl", input);
	}

	public static Task getTask(String taskName) {
		return tasks.get(taskName);
	}

	/*
	 * search for predefined task, if no entry exists return null
	 */
	private static Task findPredefinedEntry(String task) {

		Task taskThread = null;
		for (String predefTask : predefinedTasks.keySet()) {
			if (task.contains(predefTask)) {
				taskThread = predefinedTasks.get(predefTask);
			}
		}
		return taskThread;
	}

	public synchronized static void updateEnvironment(boolean isAdded)
			throws URISyntaxException, IOException,
			InvalidConfigurationException {
		System.out.println("updating environment");
		try {
			/*
			 * stop alle taken, en zorg ervoor dat alle taken geen robots meer
			 * hebben . called action
			 */
			for (String taskName : tasks.keySet()) {
				tasks.get(taskName).stopTask();
				tasks.get(taskName).setRobots(
						new HashMap<String, ArrayList<String>>());
			}

			Thread.sleep(2000);

		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		if (isAdded) {
			System.out.println("robot has been added");

			HashMap<String, Task> tasksForChromosome = tasks;
			/*
			 * mapping van maken taken met matchende robots.
			 */
			ArrayList<Task> tasksToStart = new ArrayList<Task>();
			boolean stopAssigningRobots = false;
			/*
			 * keep track of the robots that are already assigned to a certain
			 * task
			 */
			HashMap<String, Boolean> assignedRobotState = new HashMap<String, Boolean>();
			for (String deviceName : ControlPoint.deviceMappings.keySet()) {
				assignedRobotState.put(deviceName, new Boolean(false));
			}
			if (tasks.size() != 0) {
				/*
				 * keep assigning robots until the chromosome returns nothing
				 * but nothingrobots, or no more fitting robots can be found
				 * note that fitting means that the environment, task, and other
				 * filters can hold a robot from doing a task, so not only
				 * mapping is used to assign a robot
				 */
				while (!stopAssigningRobots) {
					// Taskname -> robotname -> Service_Action[]
					HashMap<String, HashMap<String, ArrayList<String>>> robotsWithActionsFittingTasks = new HashMap<String, HashMap<String, ArrayList<String>>>();

					ArrayList<String> parameters = new ArrayList<String>();
					for (String parameter : PARAMETERS) {
						parameters.add(parameter);
					}
					Robot.parameters = parameters;

					ArrayList<Robot> robots = new ArrayList<Robot>();

					/*
					 * voor elke robot gaan we na of hij een bepaalde taak
					 * aankan
					 */
					for (String deviceName : assignedRobotState.keySet()) {
						if (!assignedRobotState.get(deviceName)) {
							Robot tempRobot = new Robot(false, deviceName);

							HashMap<String, Boolean> taskMap = new HashMap<String, Boolean>();

							/*
							 * for every task, check wether or not the robot is
							 * qualified to execute it.
							 */
							for (String taskName : tasksForChromosome.keySet()) {
								Task task = tasksForChromosome.get(taskName);
								HashMap<String, ArrayList<String>> matchResult = task
										.isQualifiedRobot(deviceName);

								if (matchResult != null) {
									taskMap.put(taskName, true);
									System.out
											.println(deviceName
													+ " is qualified to do "
													+ taskName);
									/*
									 * voeg bij de taak entry, voor wie de robot
									 * een geschikte match is, de robot toe +
									 * zijn lijst van acties die nodig zijn om
									 * de taak te vervullen. .put : taskname ->
									 * robotname-> service_action[]
									 */
									if (!robotsWithActionsFittingTasks
											.containsKey(taskName)) {
										robotsWithActionsFittingTasks
												.put(taskName,
														new HashMap<String, ArrayList<String>>());
									}
									if (robotsWithActionsFittingTasks
											.get(taskName) == null) {
										robotsWithActionsFittingTasks
												.put(taskName,
														new HashMap<String, ArrayList<String>>());
									}

									robotsWithActionsFittingTasks
											.get(taskName)
											.put(deviceName,
													matchResult.get(deviceName));
								} else {
									System.out.println(deviceName
											+ " is not qualified to do "
											+ taskName);
									taskMap.put(taskName, false);
								}
							}

							tempRobot.setTaskMap(taskMap);
							/*
							 * get parameters from the robot
							 */
							HashMap<String, Integer> robotLiveParams = getParameters(
									tempRobot.getRobotName(), parameters);
							tempRobot.setParameters(robotLiveParams);
							robots.add(tempRobot);
						}
					}
					// Adding NothingRobot, nothing robot is need for the GA,
					// because it fills in all the blank spots.
					// So that populations can be made, even if not every task
					// can be executed.
					Robot tempNothingRobot = new Robot(true);

					HashMap<String, Boolean> taskMap = new HashMap<String, Boolean>();

					for (String taskName : tasks.keySet()) {
						Task task = tasks.get(taskName);
						taskMap.put(taskName, false);
					}
					tempNothingRobot.setTaskMap(taskMap);
					robots.add(tempNothingRobot);

					/*
					 * get the best ( best in an acceptable timeframe )
					 */
					IChromosome fittestChromosome = calculateOptimalTaskDivision(
							robots, tasksForChromosome);

					System.out.println("FittestChromosome:");

					for (int i = 0; i < fittestChromosome.getGenes().length; i++) {

						System.out.println("\tTaskGene ("
								+ fittestChromosome.getGene(i)
										.getApplicationData() // taskname
								+ "):");

						String taskName = fittestChromosome.getGene(i)
								.getApplicationData().toString();

						System.out.println("\t\tAssignedRobot("
								+ robots.get(
										(Integer) fittestChromosome.getGene(i)
												.getAllele()).getRobotName()
								+ ")"); // robotname
						String nameRobot = robots.get(
								(Integer) fittestChromosome.getGene(i)
										.getAllele()).getRobotName();
						System.out.println("\t"
								+ robots.get((Integer) fittestChromosome
										.getGene(i).getAllele()));
						// taskname(true/false) geeft aan of de robot de taak al
						// dan niet kan uitvoeren
						Robot tempRobot = robots
								.get((Integer) fittestChromosome.getGene(i)
										.getAllele());

						/*
						 * kan de robot de taak aan + het is niet de default
						 * "nothingrobot"
						 */
						if (tempRobot.getTask(taskName)
								&& !tempRobot.isNothingRobot()) {
							System.out.println("task can be executed by "
									+ nameRobot);
							Task task = tasksForChromosome.get(taskName);
							if (!tasksToStart.contains(task)) {
								tasksToStart.add(task);
							}
							ArrayList<String> serviceAction = robotsWithActionsFittingTasks
									.get(taskName).get(nameRobot);
							task.getRobots().put(nameRobot, serviceAction);
							assignedRobotState
									.put(nameRobot, new Boolean(true));
						}
					}
					System.out.println("ChromosomeFitness("
							+ fittestChromosome.getFitnessValue() + ")\n");
					
					/*
					 * herbereken de stopvoorwaarde, op basis van aantal
					 * beschikbare robots, vorig chromosoom en aantal taken die
					 * nog robots "willen"
					 */
					boolean robotsAvailable = false;
					for (String key : assignedRobotState.keySet()) {
						if (assignedRobotState.get(key)) {// robot beschikbaar
							robotsAvailable = true;
						}
					}
					if (!robotsAvailable) {
						stopAssigningRobots = true;
					}

					tasksForChromosome = new HashMap<String, Task>();
					for (String taskName : tasks.keySet()) {
						if (tasks.get(taskName).getIsScalable()) {
							tasksForChromosome.put(taskName,
									tasks.get(taskName));
						}
					}
					if (tasksForChromosome.size() == 0) {
						stopAssigningRobots = true;
					}

					/*
					 * als het chromosoom enkel bestaat uit nothing robots dan
					 * is er geen enkele robot geschikte voor een taak
					 */
					for (int i = 0; i < fittestChromosome.getGenes().length; i++) {

						String nameRobot = robots.get(
								(Integer) fittestChromosome.getGene(i)
										.getAllele()).getRobotName();
						System.out.println("\t"
								+ robots.get((Integer) fittestChromosome
										.getGene(i).getAllele()));

						Robot tempRobot = robots
								.get((Integer) fittestChromosome.getGene(i)
										.getAllele());

						/*
						 * kan de robot de taak aan + het is niet de default
						 * "nothingrobot"
						 */
						boolean emptyChromosome = true;
						if (!tempRobot.isNothingRobot()) {
							emptyChromosome = false;
						}
						if (emptyChromosome) {
							stopAssigningRobots = true;
						}
					}
				}
				for (Task t : tasksToStart) {
					// setRobots triggers the function that will get all the
					// Action objects needed from the robots to execute the
					// task.
					// Action objects are need to talk with the UPnP-stack
					// on the robot
					HashMap<String, ArrayList<String>> rs = t.getRobots();
					t.reset();
					t.setRobots(rs); // trigger making actions in the task
										// for
										// the robots themselves
					t.executeTask();
				}
			}
		}
	}

	private static IChromosome calculateOptimalTaskDivision(
			ArrayList<Robot> robots, HashMap<String, Task> tasksForChromosome)
			throws InvalidConfigurationException {
		// Make a new configuration for our Genotype aka population.
		Configuration.reset();
		Configuration configuration = new DefaultConfiguration();

		// Set the fitness function we want to use, which is our
		// RobotFitnessFunction.
		FitnessFunction fitnessFunction = new RobotFitnessFunction(robots);

		configuration.setFitnessFunction(fitnessFunction);

		Gene[] taskGenes = new Gene[tasksForChromosome.size()];

		int index = 0;
		for (String taskName : tasksForChromosome.keySet()) {
			IntegerGene taskGene = new IntegerGene(configuration, 0,
					robots.size() - 1);
			taskGene.setApplicationData(taskName);
			taskGenes[index] = taskGene;
			index++;
		}

		Chromosome chromosome = new Chromosome(configuration, taskGenes);

		configuration.setSampleChromosome(chromosome);

		// Finally, we need to tell the Configuration object how many
		// Chromosomes we want in our population. The more Chromosomes,
		// the larger the number of potential solutions (which is good
		// for finding the answer), but the longer it will take to
		// evolve
		// the population each round. We'll set the population size to
		// 500 here.
		configuration.setPopulationSize(500);

		Genotype population = Genotype.randomInitialGenotype(configuration);

		for (int i = 0; i < MAX_ALLOWED_EVOLUTIONS; i++) {
			population.evolve();
		}
		return population.getFittestChromosome();
	}

	private static HashMap<String, Integer> getParameters(String robotName,
			ArrayList<String> parameters) {

		HashMap<String, Integer> parametersFromRobot = new HashMap<String, Integer>();

		Device device = ControlPoint.deviceMappings.get(robotName);
		org.teleal.cling.model.meta.Service contextService = device
				.findService(new ServiceId("robot", "Context"));

		for (String param : parameters) {
			Action actie = null;

			for (Action act : contextService.getActions()) {
				if (act.getName().contains(param)) {
					actie = act;
					break;
				}
			}
			/*
			 * get outputparams
			 */
			if (actie != null) {
				UpnpActionInvocation invocation = new UpnpActionInvocation(
						actie, null);
				CallbackWrapper wrapper = new CallbackWrapper(invocation, param);

				ControlPoint.upnpService.getControlPoint().execute(wrapper);

				Object o = wrapper.getResponse();
				/*
				 * async call !!!! so we wait for the response
				 */

				ActionArgumentValue[] args = (ActionArgumentValue[]) o;
				int maxTries = 4;
				int teller = 0;
				while (o == null && teller < maxTries) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// System.out.println("trying to get the response again");
					teller++;
					o = wrapper.getResponse();
					args = (ActionArgumentValue[]) o;
				}
				Integer value;
				if (args != null) {
					value = (Integer) args[0].getValue();
				} else {
					value = 0; // dummy
				}
				parametersFromRobot.put(param, value);

			}

		}

		return parametersFromRobot;
	}

	public static void removeTask(String taskName) {
		tasks.remove(taskName);
		System.out.println(taskName + " has been removed from the tasks !");
		try {
			System.out.println("updating environment!");
			updateEnvironment(true);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static HashMap<String, ArrayList<Process>> readTask(String taskOWL)
			throws IOException, URISyntaxException {

		HashMap<String, ArrayList<Process>> subTasks = new HashMap<String, ArrayList<Process>>();

		final OWLKnowledgeBase kb = OWLFactory.createKB();
		kb.getReader().getCache().setForced(true); // als iets gevonden wordt in
													// de cache dan zal hij
													// ZEKER niet online gaan
													// zoeken
		kb.getReader().getCache().setLocalCacheDirectory("cache");
		kb.setReasoner("Pellet");

		@SuppressWarnings("deprecation")
		Service service = kb.readService(taskOWL);

		List<? extends Process> processes = kb.getProcesses(Process.COMPOSITE);
		// System.out.println("aantal p's : " + processes.size());

		Process p = processes.get(0); // er is maar 1 taak, evt. nog verder
										// opgedeeld in een reeks taken.
		System.out.println("we got compositeProcess : " + p.getLocalName());
		CompositeProcess cp = p.castTo(CompositeProcess.class);
		SequenceImpl seq = (SequenceImpl) cp.getComposedOf();

		ControlConstructList ccList = seq.getComponents();
		int index = 0;
		for (int i = 0; i < ccList.size(); i++) {

			// System.out.println("upper level");
			ControlConstruct component = ccList.get(i);
			Perform perform = (Perform) component;
			Process proc = perform.getProcess();
			String processName = proc.getLocalName();
			/*
			 * er stelt zich een probleem als er sequentie zich voordoet bv X ,Y
			 * ,X ; X zal tweemaal voorkomen maar kan natuurlijk 1 keer gemapt
			 * worden .... we plaatsen er voorlopig index_X van
			 */
			processName = index + "_" + processName;
			index++;
			subTasks.put(processName, new ArrayList<Process>());
			priorityTasks.add(processName);
			parseProcess(proc, processName, subTasks, index);
		}

		// System.out.println(cp.getLocalName()+" has been parsed into : ");

		for (String key : subTasks.keySet()) {
			System.out.println(key + " bevat volgende processen : ");
			for (Process process : subTasks.get(key)) {
				System.out.println("\t" + process.getLocalName());
			}
		}
		return subTasks;
	}

	/**
	 * @param p
	 * @param upperLevelParentProcessName
	 * @param subTasks
	 */
	private static void parseProcess(Process p,
			String upperLevelParentProcessName,
			HashMap<String, ArrayList<Process>> subTasks, int index) {
		if (p instanceof CompositeProcess) {
			// System.out.println("\t"+p.getLocalName() +
			// " is a composite process !!");
			CompositeProcess cp = p.castTo(CompositeProcess.class);
			/*
			 * get components of the sequence
			 */
			SequenceImpl seq = (SequenceImpl) cp.getComposedOf();
			ControlConstructList ccList = seq.getComponents();
			for (int i = 0; i < ccList.size(); i++) {
				ControlConstruct component = ccList.get(i);
				Perform perform = (Perform) component;
				Process proc = perform.getProcess();

				if (proc instanceof CompositeProcess) {
					// System.out.println(proc.getLocalName()
					// + " is a composite process !!");
					if (proc.getType().toString().contains("RobotAtomic")) {
						upperLevelParentProcessName = proc.getLocalName();
						index++;
						subTasks.put(index + "_" + upperLevelParentProcessName,
								new ArrayList<Process>());
					}
					if (upperLevelParentProcessName.contains("_")) {
						parseProcess(proc, upperLevelParentProcessName,
								subTasks, index);
					} else {
						parseProcess(proc, index + "_"
								+ upperLevelParentProcessName, subTasks, index);
					}
				} else {
					System.out.println("upperlevel : "
							+ upperLevelParentProcessName);
					subTasks.get(upperLevelParentProcessName).add(proc);
					// System.out.println(proc.getLocalName()
					// + " is an atomic process !");
				}
			}
		} else {
			subTasks.get(upperLevelParentProcessName).add(p);
			// System.out.println("\t" + p.getLocalName()
			// + " is an atomic process !!");
		}
	}

	@SuppressWarnings("rawtypes")
	private void executeAction(Action action,
			HashMap<String, Object> inputArguments, String robotName) {

		@SuppressWarnings("rawtypes")
		UpnpActionInvocation invocation = new UpnpActionInvocation(action,
				inputArguments);
		final String kopieName = robotName;
		// Executes asynchronous in the background
		ControlPoint.upnpService.getControlPoint().execute(
				new ActionCallback(invocation) {

					@Override
					public void success(ActionInvocation invocation) {

						System.out.println(kopieName
								+ " has successfully called "
								+ invocation.getAction().getName()
								+ " wait for the event to get next action !");
					}

					@Override
					public void failure(ActionInvocation invocation,
							UpnpResponse operation, String defaultMsg) {
						System.err.println(defaultMsg);
						// notifyTask(robotName);
					}
				});
	}

}
