package tasks.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.controlpoint.SubscriptionCallback;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.gena.CancelReason;
import org.teleal.cling.model.gena.GENASubscription;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Action;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.state.StateVariableValue;
import org.teleal.cling.model.types.ServiceId;

import tasks.taskAI.TaskAI;
import upnp.ControlPoint;
import upnp.UpnpActionInvocation;

public class DefaultTask extends Task {

	private LinkedList<HashMap<String, Object>> inputs;
	private LinkedList<Action> actions = new LinkedList<Action>();
	private HashMap<String, ArrayList<Service>> subscribedServices = new HashMap<String, ArrayList<Service>>();

	private SubscriptionCallback cb;
	private boolean taskDone = false;

	public DefaultTask(String taskName) {
		super(false, false, taskName);
	}

	public void reset() {
		if (cb != null)
			cb.end();
		subscribedServices = new HashMap<String, ArrayList<Service>>();
	}

	private void subscriptionLifecycle(Service serv) throws Exception {
		/*
		 * @params SubscriptionCallback : UPnPService, timeout : seconds
		 */

		System.out.println("subscribing to service ");
		cb = new SubscriptionCallback(serv, 1) {

			@Override
			public void established(GENASubscription sub) {
				System.out.println("Established subscription : " + sub.getSubscriptionId());
			}

			@Override
			protected void failed(GENASubscription subscription, UpnpResponse responseStatus, Exception exception, String defaultMsg) {
				System.err.println("failed : " + defaultMsg + "from service with id : " + subscription.getService().getServiceId());
			}

			@Override
			public void ended(GENASubscription sub, CancelReason reason, UpnpResponse response) {
				System.out.println("subscription to event ended");
				TaskAI.removeTask(taskName);
			}

			public void eventReceived(GENASubscription sub) {
				try {
				Map<String, StateVariableValue> values = sub.getCurrentValues();
				StateVariableValue status = values.get("Status");
				StateVariableValue robotName = values.get("Robot");
				System.out.println("event received , status is  : " + status + " coming from robot " + robotName);
				
					if (status.toString().contains("Finished")) { // old value
																	// was
																	// busy
						notifyTask(robotName.toString());
						System.out.println(robotName + " has been notified to execute his next task, or end this defaulttask");
					} else {
						System.out.println("event received , status is : " + status);
						if (robotName != null)
							System.out.println("robotname : " + robotName);

					}
				} catch (Exception ex) {
					System.out.println("device has no status!");
					System.out.println("stopping this task !");
					taskDone = true;
					TaskAI.removeTask(taskName);
					cb.end();
				}
			}

			public void eventsMissed(GENASubscription sub, int numberOfMissedEvents) {
				System.out.println("Missed events: " + numberOfMissedEvents);
			}
		};

		// async eventlistenere
		ControlPoint.upnpService.getControlPoint().execute(cb);
	}

	public DefaultTask(LinkedList<Action> actions, LinkedList<HashMap<String, Object>> inputs, String taskName) {
		super(false, false, taskName);
		this.inputs = inputs;
	}

	public void setInputs(LinkedList<HashMap<String, Object>> inputs) {
		this.inputs = inputs;
	}

	@Override
	public void executeTask() {

		/*
		 * inschrijven op de service events
		 */
		for (String key : subscribedServices.keySet()) {
			for (Service serv : subscribedServices.get(key)) {
				try {
					System.out.println("subscribing to lifecycles");
					subscriptionLifecycle(serv);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		/*
		 * follow the tasks, and give along the inputarguments.
		 */

		System.out.println("we got " + actions.size() + " tasks to execute !");
		Action action = actions.pop();
		System.out.println("inputs are -> " + inputs.toString());
		HashMap<String, Object> input = inputs.pop();
		System.out.println("start DEFAULT TASK");
		executeAction(action, input);

	}

	@Override
	public void setRobots(HashMap<String, ArrayList<String>> robots) {
		super.setRobots(robots);
		makeRobotsWithActions(robots);
	}

	private void makeRobotsWithActions(HashMap<String, ArrayList<String>> robots) {
		try {

			String robotName = "";
			boolean controlAdded = false;
			for (String robot : robots.keySet()) {
				robotName = robot;
				System.out.println("robot : " + robot);
				Device dev = ControlPoint.deviceMappings.get(robotName);

				if (dev == null) {
					// robots.remove(robotName);
					System.out.println("here in making actions, no device got.");
				} else {
					System.out.println("got robot : " + dev.getDisplayString());
					/*
					 * TODO : overloop alle services, normaliter weten we de
					 * namespace niet ( robot )
					 */

					LinkedList<Action> actionList = new LinkedList<Action>();

					for (String actionString : robots.get(robotName)) {
						String[] serviceAction = actionString.split("_");
						org.teleal.cling.model.meta.Service serv = dev.findService(new ServiceId("robot", serviceAction[0]));

						System.out.println("got service : " + serv.getServiceId().getId());

						/*
						 * TODO : overloop alle actions, normaliter weten we de
						 * namespace niet ( robot )
						 */
						Action action = serv.getAction(serviceAction[1]);
						System.out.println("got action : " + action.getName());

						/*
						 * subscribe to events from the service so that we know
						 * when we can give the robot a next task
						 */
						if (subscribedServices.get(robotName) == null) {
							subscribedServices.put(robotName, new ArrayList<Service>());
							subscribedServices.get(robotName).add(serv);
							System.out.println(robotName + " " + serv.getServiceId().getId() + " added to eventlistener.");

						} else if (!subscribedServices.get(robotName).contains(serv)) {
							System.out.println(robotName + " " + serv.getServiceId().getId() + " added to eventlistener.");
							subscribedServices.get(robotName).add(serv);
						}

						// actionList.add(action);
						System.out.println("ADDING ACTION TO LIST OF " + robotName + " action -> " + action.getName());
						/*
						 * invokeer actie, gebeurt reeds asynchroon TODO :
						 * roombabuffer voor commands ??
						 */
						actions.add(action);
					}// voor elke action

				}
				break;
			}
		} catch (Exception ex) {
			System.out.println("something went wrong whilst fetching the robot, service and the action");
			ex.printStackTrace();
		}

	}

	private void notifyTask(String robotName) {

		if (!actions.isEmpty()) {
			System.out.println(robotName + " has done his task, moving to next task");
			Action action = actions.pop();
			// robotTasks.get(robotName).add(action); // eerste actie is om
			// vanaf zijn (random)start positie
			// naar zijn beginpositie te gaan
			// !!
			HashMap<String, Object> input = inputs.pop();

			for (String str : input.keySet()) {
				System.out.println("inputs : " + str + " -> " + input.get(str));
			}
			// System.out.println(robotName + " his next task will be "
			// + action.getName() + " timestamp : "+
			// GregorianCalendar.getInstance().getTime().getMinutes() +
			// GregorianCalendar.getInstance().getTime().getSeconds());
			executeAction(action, input);
		} else {
			System.out.println("stopping this task !");
			taskDone = true;
			TaskAI.removeTask(taskName);
			cb.end();
		}
	}

	public void stopTask() {
		super.stopTask();
		this.reset();
		
	}

	private void changeStateTask(Boolean b) {
		this.taskEndedNaturally = b;
	}

	private void executeAction(Action action, HashMap<String, Object> inputArguments) {

		@SuppressWarnings("rawtypes")
		UpnpActionInvocation invocation = new UpnpActionInvocation(action, inputArguments);

		// Executes asynchronous in the background
		ControlPoint.upnpService.getControlPoint().execute(new ActionCallback(invocation) {

			@Override
			public void success(ActionInvocation invocation) {
				// assert invocation.getOutput().length == 0;
				System.out.println("Successfully called action in " + taskName);
			}

			@Override
			public void failure(ActionInvocation invocation, UpnpResponse operation, String defaultMsg) {
				System.err.println(defaultMsg);
			}
		});
	}

}
