package tasks.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.mindswap.exceptions.NotImplementedException;
import org.mindswap.owl.OWLClass;
import org.mindswap.owl.OWLDataProperty;
import org.mindswap.owl.OWLIndividual;
import org.mindswap.owl.OWLIndividualList;
import org.mindswap.owl.OWLObjectProperty;
import org.mindswap.owl.OWLProperty;
import org.mindswap.owl.OWLType;
import org.mindswap.owl.OWLValue;
import org.mindswap.owl.vocabulary.RDF;
import org.mindswap.owl.vocabulary.SWRLB;
import org.mindswap.owls.generic.expression.Expression;
import org.mindswap.owls.process.Condition;
import org.mindswap.owls.process.Input;
import org.mindswap.owls.process.Output;
import org.mindswap.owls.process.Parameter;
import org.mindswap.owls.process.Process;
import org.mindswap.owls.process.Result;
import org.mindswap.owls.vocabulary.OWLS;
import org.mindswap.swrl.Atom;
import org.mindswap.swrl.AtomList;
import org.mindswap.swrl.BuiltinAtom;
import org.mindswap.swrl.ClassAtom;
import org.mindswap.swrl.DataPropertyAtom;
import org.mindswap.swrl.DifferentIndividualsAtom;
import org.mindswap.swrl.IndividualPropertyAtom;
import org.mindswap.swrl.SWRLDataObject;
import org.mindswap.swrl.SWRLIndividualObject;
import org.mindswap.swrl.SameIndividualAtom;

import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDFS;

/**
 * Query that finds all services having a certain
 * OWLType as output
 *
 * @author MashedUp
 */
public class Match {

    public static int exact = 0; /* compared types are exact matches */
    public static int subsume = 1; /* t2 is a subclass of t1 */
    public static int relaxed = 2; /* t1 is a subclass of t2 */
    public static int fail = 3; /* compared types don't match at all */

    public static int exactList = 4;
    public static int subsumeList = 5;
    public static int relaxedList = 6; /* t2 is a list with element subclass of t1 */

    public static int exactOnCondition = 7; /* matching effect with condition requires if-condition in workflow*/


    public static int compareParamType(OWLType t1, OWLType t2) {
        if (t1.isEquivalent(t2)) {
            return Match.exact;
        } else if (t2.isSubTypeOf(t1)) {
            return Match.subsume;
        } else if (t1.isSubTypeOf(t2)) {
            return Match.relaxed;
        } else if (t2.isSubTypeOf(RDF.ListVocabulary.List()) || t2.isSubTypeOf(OWLS.ObjList.List())) {
            Map<OWLProperty, List<OWLValue>> properties = ((OWLClass) t2).getProperties();
            for (Entry<OWLProperty, List<OWLValue>> property : properties.entrySet()) {
                OWLProperty prop = property.getKey();
                if (prop.getURI().toString().equals(RDFS.subClassOf.getURI())) {
                    List<OWLValue> values = property.getValue();
                    for (int i = 0; i < values.size(); i++) {
                        OWLIndividual value = (OWLIndividual) values.get(i);
                        for (Entry<OWLProperty, List<OWLValue>> ind_properties : value.getProperties().entrySet()) {
                            OWLProperty ind_prop = (OWLProperty) ind_properties.getKey();
                            if (ind_prop.getURI().toString().equals(OWL.allValuesFrom.getURI())) {
                                for (int j = 0; j < ind_properties.getValue().size(); j++) {
                                    OWLIndividual ind = (OWLIndividual) ind_properties.getValue().get(j);
                                    OWLType type = ind.castTo(OWLType.class);
                                    if (type.equals(t2)) {
                                        continue;
                                    }
                                    int m = compareParamType(type, t1);
                                    if (m == Match.exact) {
                                        return Match.exactList;
                                    } else if (m == Match.subsume) {
                                        return Match.relaxedList;
                                    } else if (m == Match.relaxed) {
                                        return Match.subsumeList;
                                    } else if (m == Match.exactList) {
                                        return Match.exact;
                                    } else if (m == Match.subsumeList) {
                                        return Match.relaxed;
                                    } else if (m == Match.relaxedList) {
                                        return Match.subsume;
                                    } else {
                                        return Match.fail;
                                    }
                                }
                            }
                        }
                    }
                }

            }
            return Match.fail;
        } else if (t1.isSubTypeOf(RDF.ListVocabulary.List()) || t1.isSubTypeOf(OWLS.ObjList.List())) {
            int m = compareParamType(t2, t1);
            if (m == Match.exactList) {
                return Match.exact;
            } else if (m == Match.subsumeList) {
                return Match.relaxed;
            } else if (m == Match.relaxedList) {
                return Match.subsume;
            } else {
                return Match.fail;
            }
        } else {
            return Match.fail;
        }
    }

    public static boolean compareParamTypeBoolExact(OWLType t1, OWLType t2) {

    	//System.out.println("comparing paramtypes : " + t1.getURI() + " , " + t2.getURI());
        int m = compareParamType(t2, t1);
        //System.out.println("comparing gave : " +  m);
        if (m == Match.exact) {
            return true;
        }

        return false;
    }

    public static boolean compareParamTypeBoolRelaxed(OWLType t1, OWLType t2) {

        int m = compareParamType(t2, t1);
        if (m != Match.fail) {
            return true;
        }
        return false;
    }

    public static int compareAtom(
            Atom a1, Atom a2) {
        // if condition and result are class attribute
        if (a1 instanceof ClassAtom && a2 instanceof ClassAtom) {
            //condition classatom
            ClassAtom classAtom1 = (ClassAtom) a1;
            OWLClass classpred1 = classAtom1.getClassPredicate();
            SWRLIndividualObject arg1 = classAtom1.getArgument1();
            //result classatom
            ClassAtom classAtom2 = (ClassAtom) a2;
            OWLClass classpred2 = classAtom2.getClassPredicate();
            SWRLIndividualObject arg2 = classAtom2.getArgument1();

            //check if same classatom
            if (classpred1.equals(classpred2)) {
                int m = compareParamType(((Parameter) arg2.castTo(Parameter.class)).getParamType(), ((Parameter) arg1.castTo(Parameter.class)).getParamType());
                return m;
            } else {
                return Match.fail;
            }

        } // else if condition and result are objectproperty
        else if (a1 instanceof IndividualPropertyAtom && a2 instanceof IndividualPropertyAtom) {
            //condition individualproperty
            IndividualPropertyAtom indPropAtom1 = (IndividualPropertyAtom) a1;
            OWLObjectProperty propred1 = indPropAtom1.getPropertyPredicate();

            //result individualproperty
            IndividualPropertyAtom indPropAtom2 = (IndividualPropertyAtom) a2;
            OWLObjectProperty propred2 = indPropAtom2.getPropertyPredicate();


            //check if same property
            if (propred2.equals(propred1)) {

                // get arguments
                SWRLIndividualObject arg11 = indPropAtom1.getArgument1();
                SWRLIndividualObject arg12 = indPropAtom1.getArgument2();
                SWRLIndividualObject arg21 = indPropAtom2.getArgument1();
                SWRLIndividualObject arg22 = indPropAtom2.getArgument2();

                OWLType type11 = ((Parameter) arg11.castTo(Parameter.class)).getParamType();
                OWLType type12 = ((Parameter) arg12.castTo(Parameter.class)).getParamType();
                OWLType type21 = ((Parameter) arg21.castTo(Parameter.class)).getParamType();
                OWLType type22 = ((Parameter) arg22.castTo(Parameter.class)).getParamType();

                int m1 = compareParamType(type11, type21);
                if (m1 != Match.fail) {
                    int m2 = compareParamType(type12, type22);
                    if (m2 != Match.fail) {
                        return (m1 < m2) ? m2 : m1;
                    }
                } else {
                    return Match.fail;
                }
            } else {
                return Match.fail;
            }

        } else if (a1 instanceof DataPropertyAtom && a2 instanceof DataPropertyAtom) {
            DataPropertyAtom dataPropAtom1 = (DataPropertyAtom) a1;
            DataPropertyAtom dataPropAtom2 = (DataPropertyAtom) a2;

            OWLDataProperty propred1 = dataPropAtom1.getPropertyPredicate();
            OWLDataProperty propred2 = dataPropAtom2.getPropertyPredicate();

            //check if same property
            if (propred2.equals(propred1)) {
                SWRLIndividualObject arg11 = dataPropAtom1.getArgument1();
                SWRLIndividualObject arg21 = dataPropAtom2.getArgument1();

                SWRLDataObject arg12 = dataPropAtom1.getArgument2();
                SWRLDataObject arg22 = dataPropAtom2.getArgument2();

                OWLType type11 = ((Parameter) arg11.castTo(Parameter.class)).getParamType();
                OWLType type21 = ((Parameter) arg21.castTo(Parameter.class)).getParamType();

                if (compareParamTypeBoolExact(type11, type21)) {
                    if (arg12.equals(arg22)) {
                        return Match.exact;
                    }
                }
            }
        } else if (a1 instanceof SameIndividualAtom) {
            SameIndividualAtom sameAtom1 = (SameIndividualAtom) a1;
            sameAtom1.getArgument1();
            sameAtom1.getArgument2();
        } else if (a1 instanceof DifferentIndividualsAtom) {
//            DifferentIndividualsAtom diffAtom1 = (DifferentIndividualsAtom) a1;
//            SWRLIndividualObject arg11 = diffAtom1.getArgument1();
//            SWRLIndividualObject arg12 = diffAtom1.getArgument2();
        } else if (a1 instanceof BuiltinAtom) {
            BuiltinAtom builtinAtom1 = (BuiltinAtom) a1;
            OWLIndividual builtin1 = builtinAtom1.getBuiltin();
            int argCount1 = a1.getArgumentCount();
            SWRLDataObject[] args = new SWRLDataObject[argCount1];
            for (int i = 0; i
                    < argCount1; i++) {
                SWRLDataObject arg = (SWRLDataObject) a1.getArgument(i);
                args[i] = arg;
            }

            if (builtin1.equals(SWRLB.equal)) {
                //args[0];
                //args[1] ;
            } else if (builtin1.equals(SWRLB.notEqual)) {
                //args[0];
                //args[1];
            } else if (builtin1.equals(SWRLB.greaterThan)) {
                //args[0];
                //args[1] ;
            } else if (builtin1.equals(SWRLB.greaterThanOrEqual)) {
                //args[0];
                //args[1] ;
            } else if (builtin1.equals(SWRLB.lessThan)) {
                //args[0];
                //args[1] ;
            } else if (builtin1.equals(SWRLB.lessThanOrEqual)) {
                //args[0];
                //args[1] ;
            } else if (builtin1.equals(SWRLB.add)) {
                //args[0];
                //args[1] ;
                //args[2] ;
            } else if (builtin1.equals(SWRLB.subtract)) {
                //args[0];
                //args[1] ;
                //args[2] ;
            } else {
                throw new NotImplementedException(builtin1.toPrettyString());
            }

        }
        return Match.fail;
    }

    public static boolean compareAtomBool(Atom a1, Atom a2) {
        int m = compareAtom(a2, a1);
        if (m == Match.exact) {
            return true;
        }
        return false;
    }

    public static int compareInputs(
            OWLIndividualList<Input> i1, OWLIndividualList<Input> i2) {

    	//System.out.println("comparing inputs");    	
        int m = Match.fail;
        for (Input o : i1) {
            m = Match.fail;

            for (Input a : i2) {
                if (compareParamTypeBoolExact(a.getParamType(), o.getParamType())) {
                    m = Match.exact;
                    break;
                }
            }
            if (m == Match.fail) {
                return m;
            }
        }
        return m;
    }

    public static int compareOutputs(List<Output> o1, List<Output> o2) {

        int m = Match.fail;
        for (Output o : o1) {
            m = Match.fail;

            for (Output a : o2) {
                if (compareParamTypeBoolExact(a.getParamType(), o.getParamType())) {
                    m = Match.exact;
                    break;
                }
            }
            if (m == Match.fail) {
                return m;
            }
        }
        return m;
    }

    public static int compareConditionBody(
            AtomList l1, AtomList l2) {

        int m = Match.fail;

        for (int i = 0; i
                < l1.size(); i++) {

            m = Match.fail;

            for (int j = 0; j
                    < l2.size(); j++) {

                if (compareAtomBool(l1.get(i), l2.get(j))) {
                    m = Match.exact;
                    break;
                }
            }
            if (m == Match.fail) {
                return m;
            }
        }
        return m;
    }

    public static int comparePreconditions(
            OWLIndividualList<Condition> c1, OWLIndividualList<Condition> c2) {

        int m = Match.fail;

        for (Condition o1 : c1) {
            m = Match.fail;

            for (Condition o2 : c2) {

                AtomList l1 = o1.getBody();
                AtomList l2 = o2.getBody();

                if (l1.size() != l2.size()) {
                    break;
                }

                if (l1.size() == 0 || compareConditionBody(l1, l2) == Match.exact) {
                    m = Match.exact;
                    break;
                }
            }
            if (m == Match.fail) {
                return m;
            }
        }
        return m;
    }

    public static int compareEffects(OWLIndividualList<Expression> e1, OWLIndividualList<Expression> e2) {

        int m = Match.fail;
        for (Expression o1 : e1) {

            m = Match.fail;

            for (Expression o2 : e2) {

                AtomList l1 = o1.getBody();
                AtomList l2 = o2.getBody();

                if (l1.size() != l2.size()) {
                    break;
                }

                if (l1.size() == 0 || compareConditionBody(l1, l2) == Match.exact) {
                    m = Match.exact;
                    break;
                }
            }
            if (m == Match.fail) {
                return m;
            }
        }
        return m;
    }

    public static boolean haveMatchingEffects(OWLIndividualList<Expression> e1, OWLIndividualList<Expression> e2) {

        for (Expression o1 : e1) {

            AtomList l1 = o1.getBody();

            for (Expression o2 : e2) {

                AtomList l2 = o2.getBody();

                for (int i = 0; i < l1.size(); i++) {

                    for (int j = 0; j < l2.size(); j++) {

                        if (compareAtomBool(l1.get(i), l2.get(j))) {
                            return true;
                        }
                    }
                }

            }

        }
        return false;
    }

    public static int compareResults(List<Result> r1, List<Result> r2) {

        int m = Match.fail;
        for (Result o1 : r1) {

            m = Match.fail;

            for (Result o2 : r2) {

                OWLIndividualList<Expression> e1 = o1.getEffects();
                OWLIndividualList<Expression> e2 = o2.getEffects();

                if (e1.size() != e2.size()) {
                    break;
                }

                if (e1.size() == 0 || compareEffects(e1, e2) == Match.exact) {
                    m = Match.exact;
                    break;
                }
            }
            if (m == Match.fail) {
                return m;
            }
        }
        return m;
    }

    public static boolean haveMatchingResult(List<Result> r1, List<Result> r2) {

        for (Result o1 : r1) {

            OWLIndividualList<Expression> e1 = o1.getEffects();
//            OWLIndividualList<Condition> c1 = o1.getConditions();

            for (Result o2 : r2) {
                OWLIndividualList<Expression> e2 = o2.getEffects();
//                OWLIndividualList<Condition> c2 = o2.getConditions();

                if (haveMatchingEffects(e1, e2)) {
                    return true;
                }

            }
        }
        return false;
    }

    public static boolean haveMatchingOutput(List<Output> r1, List<Output> r2) {

        for (Output o1 : r1) {
            for (Output o2 : r2) {

                if (compareParamTypeBoolExact(o1.getParamType(), o2.getParamType())) {
                    return true;
                }

            }
        }
        return false;
    }

    public static int compareProcesses(Process p1, Process p2, boolean conditions) {

        OWLIndividualList<Output> o1 = p1.getOutputs();
        OWLIndividualList<Output> o2 = p2.getOutputs();
        OWLIndividualList<Input> i1 = p1.getInputs();
        OWLIndividualList<Input> i2 = p2.getInputs();

        // return early if input & output count don't match
        if (o1.size() != o2.size() || i1.size() != i2.size()) {
            return Match.fail;
        }
        if (i1.size() != 0 && compareInputs(i1, i2) == Match.fail) {
            return Match.fail;
        }
        if (o1.size() != 0 && compareOutputs(o1, o2) == Match.fail) {
            return Match.fail;
        }
        if (conditions) {
            OWLIndividualList<Condition> c1 = p1.getConditions();
            OWLIndividualList<Condition> c2 = p2.getConditions();
            OWLIndividualList<Result> r1 = p1.getResults();
            OWLIndividualList<Result> r2 = p2.getResults();

            // return early if precondition & result count don't match
            if (c1.size() != c2.size() || r1.size() != r2.size()) {
                return Match.fail;
            }
            if (c1.size() != 0 && comparePreconditions(c1, c2) == Match.fail) {
                return Match.fail;
            }
            if (r1.size() != 0 && compareResults(r1, r2) == Match.fail) {
                return Match.fail;
            }
        }

        return Match.exact;
    }

    public static List<Parameter> containsType(@SuppressWarnings("rawtypes") List list, OWLType type) {

        List<Parameter> param = new ArrayList<Parameter>();
        if (type == null) {
            return param;
        }
        for (Object t : list) {
            OWLType type2 = ((Parameter) t).getParamType();
            if (type2 == null) {
                continue;
            }
            int m = compareParamType(type, type2);
            if (m == Match.exact || m == Match.subsume) {
                param.add((Parameter) t);
            }
        }
        return param;
    }

    public static Atom containsCondition(@SuppressWarnings("rawtypes") List list, Atom atom) {
        Atom returnAtom = null;

        for (Object cond : list) {

            AtomList l1 = ((Expression) cond).getBody();

            for (int i = 0; i
                    < l1.size(); i++) {
                int m = compareAtom(atom, l1.get(i));
                if (m == Match.exact || m == Match.subsume) {
                    returnAtom = l1.get(i);
                }
            }
        }
        return returnAtom;
    }

    public static List<Result> containsResult(List<Result> list, Atom atom) {
        List<Result> returnAtom = new ArrayList<Result>();
        for (Result res : list) {
            if (containsCondition(res.getEffects(), atom) != null) {
                returnAtom.add(res);
            }
        }
        return returnAtom;
    }
}
