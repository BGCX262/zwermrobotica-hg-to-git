package tasks.bo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

import upnp.ControlPoint;

public abstract class Task  {

	private boolean isInfinite; // Should this task be repeated over and over again?
	private boolean isScalable;	// Does the task differ with less or more robots?
	private boolean isPrior = false;
	protected String taskName;
	protected boolean isActive = false; // Is the task already being executed by a swarm of robots.
	protected boolean taskEndedNaturally = false;
	protected Thread logic = null;
	protected ArrayList<org.mindswap.owls.process.Process> tasks;
	protected HashMap<String,ArrayList<String>> robots; // Map of robots with their actions they have to perform for this task.
	protected HashMap<String, Integer> contextMultipliers = new HashMap<String,Integer>();// gives a certain importance to a certain parameter
	// i.e. for coverage the batterylife could be important, so the batterylife will receive a bigger multiplier then another contextparameter
	
	
	public Task(boolean isInfinite, boolean isScalable ,String taskName){		
		this.isInfinite = isInfinite;
		this.isScalable = isScalable;
		this.taskName = taskName;		
	}
	
	public void reset(){
		
	}
	
	public boolean getIsPrior(){
		return isPrior;
	}
	
	public void setIsPrior(boolean b){
		isPrior  = b;
	}
	
	public Task(boolean isInfinite, boolean isScalable ,String taskName,ArrayList<org.mindswap.owls.process.Process> processen){		
		this.isInfinite = isInfinite;
		this.isScalable = isScalable;
		this.taskName = taskName;		
		this.tasks = processen;		
	}
	
	public ArrayList<org.mindswap.owls.process.Process> getTasks() {
		return tasks;
	}

	public void setTasks(ArrayList<org.mindswap.owls.process.Process> tasks) {
		this.tasks = tasks;
	}

	public void setRobots(HashMap<String,ArrayList<String>> robots){
		this.robots = robots;
	}
	
	public HashMap<String,ArrayList<String>> getRobots(){
		return robots;
	}
	
	public boolean getIsScalable(){
		return isScalable;
	}
	
	public boolean getIsInfinite(){
		return isInfinite;
	}
	
	public void stopTask(){
		taskEndedNaturally = false;
		if(logic != null)
			logic.interrupt();
	}
	
	public String getTaskName(){
		return this.taskName;
	}
	
	public boolean getIsActive(){
		return isActive;
	}
	
	public HashMap<String,ArrayList<String>> isQualifiedRobot(String robotName) throws URISyntaxException, IOException{
		/*
		 * to make the system somewhat more intelligent in a modular way
		 * add your code here to wether or not a robot is fit for the task.
		 * i.e. it could be that next to matching, batterylife is also a factor for this task.
		 */
		return ControlPoint.isRobotQualified(robotName, tasks);		
	}
	
	public HashMap<String,Integer> getContextMultipliers(){
		return this.contextMultipliers;
	}
	
	public void setContextMultipliers(HashMap<String,Integer> contextmultipliers){//protected ?
		this.contextMultipliers = contextmultipliers;
	}
	
	public abstract void executeTask();
}
