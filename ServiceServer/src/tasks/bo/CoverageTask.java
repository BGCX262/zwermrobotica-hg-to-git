package tasks.bo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.jgap.InvalidConfigurationException;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.controlpoint.SubscriptionCallback;
import org.teleal.cling.model.action.ActionArgumentValue;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.gena.CancelReason;
import org.teleal.cling.model.gena.GENASubscription;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Action;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.state.StateVariableValue;
import org.teleal.cling.model.types.ServiceId;

import tasks.taskAI.TaskAI;
import upnp.ControlPoint;
import upnp.UpnpActionInvocation;
import AI.src.global.commands.robot.Command;
import AI.src.global.commands.robot.Command.Type;
import AI.src.global.commands.robot.ForwardCommand;
import AI.src.global.commands.robot.RotateCommand;
import AI.src.global.commands.robot.SequenceCommand;
import AI.src.pathfinding.PathModule;
import AI.src.pathfinding.algorithms.Path;
import AI.src.robot.AIModule;
import AI.src.robot.DataModel;

public class CoverageTask extends Task {

	private static boolean singletonCreated = false;
	private static CoverageTask singleton;
	private HashMap<String, LinkedList<Action>> robotTasks = new HashMap<String, LinkedList<Action>>();
	private HashMap<String, ArrayList<Service>> subscribedServices = new HashMap<String, ArrayList<Service>>();

	private ArrayList<SubscriptionCallback> eventCallbacks = new ArrayList<SubscriptionCallback>();
	
	private HashMap<String, HashMap<String, HashMap<String, Object>>> inputs = new HashMap<String, HashMap<String, HashMap<String, Object>>>();
	/*
	 * inputs are deprecated !!!!! new structures are below !
	 */
	HashMap<String, LinkedList<Action>> robotActionSequence = new HashMap<String, LinkedList<Action>>();
	HashMap<String, LinkedList<HashMap<String, Object>>> robotActionInputSequence = new HashMap<String, LinkedList<HashMap<String, Object>>>();

	private final int LENGTH = 400;// zie Default.xml -> map

	/*
	 * 															.
	 *														   / \
	 * 															|
	 * 															|
	 * 															x
	 * 															|	
	 * PATH module -> X en Y axis - >           <---------y-----+
	 * 	 
	 * 
	 */
	private static PathModule module;
	private static DataModel dm;

	public static CoverageTask init() {
		if (!singletonCreated) {
			singleton = new CoverageTask();
		}
		return singleton;
	}

	private CoverageTask() {
		super(true, true, "CoverageTask");
		dm = DataModel.getInstance();
		module = DataModel.getInstance().getPathmodule();
		new AIModule();
		DataModel dm = DataModel.getInstance();
		PathModule module = dm.getPathmodule();
		/*
		 * adding contextparameters
		 */
		this.contextMultipliers.put("BatteryLife", 2);
		this.contextMultipliers.put("Speed", 3);
	}

	public void reset() {
		inputs = new HashMap<String, HashMap<String, HashMap<String, Object>>>();
		robotActionSequence = new HashMap<String, LinkedList<Action>>();
		robotActionInputSequence = new HashMap<String, LinkedList<HashMap<String, Object>>>();
		robotTasks = new HashMap<String, LinkedList<Action>>();
		subscribedServices = new HashMap<String, ArrayList<Service>>();
		/*
		 * nu nog de effectieve listeners
		 */
		for (SubscriptionCallback cb : eventCallbacks) {
			System.out.println("end subscriptions");
			cb.end();
		}
		eventCallbacks = new ArrayList<SubscriptionCallback>();
	}

	private void subscriptionLifecycle(Service serv) throws Exception {
		/*
		 * @params SubscriptionCallback : UPnPService, timeout : seconds
		 */

		System.out.println("subscribing to service ");
		SubscriptionCallback callback = new SubscriptionCallback(serv, 100) {

			@Override
			public void established(GENASubscription sub) {
				System.out.println("Established subscription : " + sub.getSubscriptionId());
			}

			@Override
			protected void failed(GENASubscription subscription, UpnpResponse responseStatus, Exception exception, String defaultMsg) {
				System.err.println("failed : " + defaultMsg + "from service with id : " + subscription.getService().getServiceId());

			}

			@Override
			public void ended(GENASubscription sub, CancelReason reason, UpnpResponse response) {
				System.out.println("subscription to event ended");

			}

			public void eventReceived(GENASubscription sub) {

				Map<String, StateVariableValue> values = sub.getCurrentValues();
				StateVariableValue status = values.get("Status");
				StateVariableValue robotName = values.get("Robot");								
				 
				if (status.toString().contains("Finished")) { // old value was busy
					notifyTask(robotName.toString());
					System.out.println(robotName + " has been notified to execute his next task");
				} else if(status.toString().contains("Bumped")) {
					System.out.println("[DEBUG] READ CHECKOUTPLACE-TASK !!");					
					try {
						TaskAI.addTask("http://localhost:9090/tasks/CheckOutPlace.owl");
						TaskAI.addTask("http://localhost:9090/tasks/BoostRadio.owl");
						try {
							TaskAI.updateEnvironment(true);
						} catch (InvalidConfigurationException e) {
							System.out.println("Failed to update environment");
							e.printStackTrace();
						}
					} catch (IOException e) {
						System.out.println("[DEBUG] could not read checkoutplace - task owl file");
						e.printStackTrace();
					} catch (URISyntaxException e) {
						System.out.println("[DEBUG] could not read checkoutplace - task owl file");
						e.printStackTrace();
					}
				}
			}

			public void eventsMissed(GENASubscription sub, int numberOfMissedEvents) {
				System.out.println("Missed events: " + numberOfMissedEvents);
			}
		};
		ControlPoint.upnpService.getControlPoint().execute(callback); // asyn
																		// executions
																		// of
																		// events
		eventCallbacks.add(callback);
	}

	public void stopTask() {
		super.stopTask();
		this.reset();
	}

	/*
	 * Later on we can use the Pathfinding module to find a sequence of commands
	 * that need to be followed to get to a certain point. For now we are just
	 * going to divide the full length of the area in n ( = number of robots )
	 * and let every robot follow the goTo ( Distance ) and TurnLeft( 90� )
	 */
	@Override
	public void executeTask() {
		/*
		 * TODO create businesslogic to cover a certain area -> only needing
		 * inputs and invoking the series of actions.
		 */
		System.out.println("EXECUTING COVERAGE PATTERN !!");
		int numberOfRobots = robots.size();
		System.out.println("number of robots in taskcoverage : " + numberOfRobots);
		int distancex = LENGTH / numberOfRobots;
		int distancey = LENGTH;
		int angle = 90;

		/*
		 * inputs verdelen, om niet te hardcoded te werk te gaan kan het zijn
		 * dat een coverage task eerst bestaat uit roteren en dan vooruit gaan,
		 * ipv vooruit gaan en dan roteren, dus moeten we bijhouden per
		 * mogelijke actie wat de inputs ervan zijn, en dus niet blindelings op
		 * de eerste actie de eerste set gegenereerde inputs plaatsen, voorlopig
		 * doen we het nog zo
		 */
		/*
		 * TODO nog kijken welke inputs er zijn, en ahdv de inputs van Distance,
		 * en Angle enz. beslissen welke actie het is. Nu nog op naam
		 */
		try {
			System.out.println("calculating inputs for robots");
			calculateInputsForRobots(distancex,distancey, angle);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("something went wrong during calculations of coordinates");
			e.printStackTrace();
		}

		/*
		 * inschrijven op de service events
		 */
		for (String key : subscribedServices.keySet()) {
			for (Service serv : subscribedServices.get(key)) {
				try {
					System.out.println("subscribing to lifecycles");
					subscriptionLifecycle(serv);
				} catch (Exception e) {					
					e.printStackTrace();
				}
			}
		}

		/*
		 * voor elke robot die voldoet, start de chain van acties
		 */
		for (String robotName : robotActionSequence.keySet()) {
			Action action = robotActionSequence.get(robotName).pop();
			// robotTasks.get(robotName).add(action); // eerste actie is om
			// vanaf zijn (random)start positie
			// naar zijn beginpositie te gaan
			// !!
			System.out.println("starting initial action for " + robotName);

			HashMap<String, Object> input = robotActionInputSequence.get(robotName).pop();
			executeAction(action, input, robotName);
		}
	}

	private void calculateInputsForRobots(int distancex,int distancey, int angle) throws Exception {
		
		/*
		 * de updates zorgen ervoor dat de volgende coordinaten naar waar een
		 * pad moet gezocht worden geupdate worden bv : 0,0 -> 500,0 -> 500,500
		 * -> 0,500 -> 0,0
		 */

		
		
		int updateIndex = 0;
		ArrayList<Integer> updateX = new ArrayList<Integer>();
		updateX.add(distancex);
		updateX.add(0);
		updateX.add((-1 * distancex));
		updateX.add(0);

		ArrayList<Integer> updateY = new ArrayList<Integer>();
		updateY.add(0);
		updateY.add(distancey);
		updateY.add(0);
		updateY.add((-1 * distancey));

		int robotNummer = 0;

		for (String robotName : robotTasks.keySet()) {

			System.out.println("calcing");
			robotActionSequence.put(robotName, new LinkedList<Action>());
			robotActionInputSequence.put(robotName, new LinkedList<HashMap<String, Object>>());

			Device dev = ControlPoint.deviceMappings.get(robotName);
			Service context = dev.findService(new ServiceId("robot", "Context"));

			Action getX = null;
			Action getY = null;
			Action getAngle = null;

			for (Action action : context.getActions()) {
				if (action.getName().contains("XCoord")) {
					getX = action;
				} else if (action.getName().contains("YCoord")) {
					getY = action;
				} else if (action.getName().contains("Angle")) {
					getAngle = action;
				}
			}
			/*
			 * context kan soms wat laggen, even wachten dus !
			 */
			/*
			 * Action getX = null; int stop = 0; while(getX==null && stop < 10){
			 * getX = context.getAction("getXCoord"); try { Thread.sleep(500);
			 * stop++; } catch (InterruptedException e) { // TODO Auto-generated
			 * catch block e.printStackTrace(); } } Action getY =
			 * context.getAction("getYCoord"); Action getAngle =
			 * context.getAction("getAngle");
			 */
			/*
			 * roep de acties op
			 */
			System.out.println("ACTION GET : " + getX);
			System.out.println("ACTION GET : " + getY);
			System.out.println("ACTION GET : " + getAngle);

			UpnpActionInvocation invocationx = new UpnpActionInvocation(getX, null);
			int robotx = getResponse(invocationx);

			UpnpActionInvocation invocationy = new UpnpActionInvocation(getY, null);
			int roboty = getResponse(invocationy);

			UpnpActionInvocation invocationAngle = new UpnpActionInvocation(getAngle, null);
			int robotAngle = getResponse(invocationAngle);

			dm.setX(robotx);
			dm.setY(roboty);
			dm.setAngle(robotAngle);

			/*
			 * per nieuwe robot maken we nu ook een nieuwe sequentie van actions
			 * die hij moet volgen deze sequentie vervangt de ingelezen
			 * sequentie van actions !!
			 */
			// HashMap<String,HashMap<String,Object>> inputs = new
			// HashMap<String,HashMap<String,Object>>();
			int goalX = robotNummer * distancex;
			int goalY = 0; // niet langs de randen gaan, geeft fouten, bv terug
							// naar 0,0-> error
			/*
			 * ga naar start positie
			 */
			System.out.println("path van " + dm.getX() + "," + dm.getY() + " naar " + goalX + "," + goalY);
			Path path = module.calculatePath(dm.getX(), dm.getY(), goalX, goalY);
			robotAngle = generateInputsForRobot(robotName, dev, path, robotAngle);

			/*
			 * ga naar rechts
			 */
			dm.setX(goalX);
			dm.setY(goalY);
			dm.setAngle(robotAngle);

			goalX += updateX.get(updateIndex);
			goalY += updateY.get(updateIndex);
			System.out.println("path van " + dm.getX() + "," + dm.getY() + " naar " + goalX + "," + goalY);
			updateIndex++;
			/*
			 * -->
			 */
			Path path2 = module.calculatePath(dm.getX(), dm.getY(), goalX, goalY);
			robotAngle = generateInputsForRobot(robotName, dev, path2, robotAngle);

			/*
			 * ga naar boven
			 */
			dm.setX(goalX);
			dm.setY(goalY);
			dm.setAngle(robotAngle);

			goalX += updateX.get(updateIndex);
			goalY += updateY.get(updateIndex);
			System.out.println("path van " + dm.getX() + "," + dm.getY() + " naar " + goalX + "," + goalY);
			updateIndex++;
			/*
			 *    .
			 *   / \
			 *    | 
			 *    |
			 */
			Path path3 = module.calculatePath(dm.getX(), dm.getY(), goalX, goalY);
			robotAngle =generateInputsForRobot(robotName, dev, path3, robotAngle);

			/*
			 * ga naar links
			 */
			dm.setX(goalX);
			dm.setY(goalY);
			dm.setAngle(robotAngle);

			goalX += updateX.get(updateIndex);
			goalY += updateY.get(updateIndex);
			System.out.println("path van " + dm.getX() + "," + dm.getY() + " naar " + goalX + "," + goalY);
			updateIndex++;
			/*
			 * <--
			 */
			Path path4 = module.calculatePath(dm.getX(), dm.getY(), goalX, goalY);
			robotAngle = generateInputsForRobot(robotName, dev, path4, robotAngle);

			/*
			 * ga naar onder
			 */
			dm.setX(goalX);
			dm.setY(goalY);
			dm.setAngle(robotAngle);

			goalX += updateX.get(updateIndex);
			goalY += updateY.get(updateIndex);
			System.out.println("path van " + dm.getX() + "," + dm.getY() + " naar " + goalX + "," + goalY);
			/*
			 * <--
			 */
			Path path5 = module.calculatePath(dm.getX(), dm.getY(), goalX, goalY);
			robotAngle = generateInputsForRobot(robotName, dev, path5, robotAngle);
			updateIndex = 0;
			robotNummer++;
			/*
			 * robotnummer zorgt ervoor dat de startx telkens wordt opgeschoven
			 * eerst robot start op plaats 0,0 , tweede op plaats 50,0 (bv),
			 * derde op plaats 100,0 etc.
			 */
			
			/*
			 * voeg nu nog een laatste rotate toe, zodat er cyclisch kan gereden worden
			 */
			addAdditionAngle(robotName, dev);
		}

	}

	/*
	 * angle
	 */
	@SuppressWarnings("rawtypes")
	private int generateInputsForRobot(String robotName, Device dev, Path path, int angle) throws Exception {
		if (path != null) {
			// dm.setSequenceCommand(module.getSequenceRoomba(path));
			SequenceCommand comm = module.getSequenceRoomba(path);
			for (Command com : comm.getCommands()) {
				System.out.println(com.toCommandString());
				if (com.getType() == Type.FORWARD) {
					ForwardCommand fwd = (ForwardCommand) com;
					System.out.println(fwd.getDistance());
					/*
					 * zoek juiste Action die een distance als input nodig heeft
					 */
					org.teleal.cling.model.meta.Service serv = dev.findService(new ServiceId("robot", "Control"));

					/*
					 * subscribe to events from the service so that we know when
					 * we can give the robot a next task
					 */
					if (subscribedServices.get(robotName) == null) {
						subscribedServices.put(robotName, new ArrayList<Service>());
						subscribedServices.get(robotName).add(serv);
						System.out.println(robotName + " " + serv.getServiceId().getId() + " added to eventlistener.");

					} else if (!subscribedServices.get(robotName).contains(serv)) {
						System.out.println(robotName + " " + serv.getServiceId().getId() + " added to eventlistener.");
						subscribedServices.get(robotName).add(serv);
					}
					Action moveForwardAction = null;
					for (Action action : serv.getActions()) {
						if (action.getInputArgument("Distance") != null) {
							moveForwardAction = action;
							break;
						}
					}
					/*
					 * we hebben onze gepaste action, deze is er zeker, want er
					 * is al semantisch bepaald dat deze robot dergelijke action
					 * heeft !!
					 */
					if (moveForwardAction != null) {
						robotActionSequence.get(robotName).add(moveForwardAction);
						HashMap<String, Object> inputs = new HashMap<String, Object>();
						inputs.put("Distance", fwd.getDistance());
						robotActionInputSequence.get(robotName).add(inputs);
					}
				} else {
					RotateCommand rot = (RotateCommand) com;
					angle += rot.getAngle();
					System.out.println(rot.getAngle());
					/*
					 * zoek juiste Action die een distance als input nodig heeft
					 */
					org.teleal.cling.model.meta.Service serv = dev.findService(new ServiceId("robot", "Control"));
					/*
					 * subscribe to events from the service so that we know when
					 * we can give the robot a next task
					 */
					if (subscribedServices.get(robotName) == null) {
						subscribedServices.put(robotName, new ArrayList<Service>());
						subscribedServices.get(robotName).add(serv);
						System.out.println(robotName + " " + serv.getServiceId().getId() + " added to eventlistener.");
					} else if (!subscribedServices.get(robotName).contains(serv)) {
						System.out.println(robotName + " " + serv.getServiceId().getId() + " added to eventlistener.");
						subscribedServices.get(robotName).add(serv);
					}

					Action moveForwardAction = null;
					for (Action action : serv.getActions()) {
						if (action.getInputArgument("Angle") != null) {
							moveForwardAction = action;
							break;
						}
					}
					/*
					 * we hebben onze gepaste action, deze is er zeker, want er
					 * is al semantisch bepaald dat deze robot dergelijke action
					 * heeft !!
					 */
					robotActionSequence.get(robotName).add(moveForwardAction);
					HashMap<String, Object> inputs = new HashMap<String, Object>();
					inputs.put("Angle", rot.getAngle());
					robotActionInputSequence.get(robotName).add(inputs);
				}
			}
		} else {
			System.out.println("path null");
		}
		return angle % 360;
	}
	
	private void addAdditionAngle(String robotName,Device dev){
		org.teleal.cling.model.meta.Service serv = dev.findService(new ServiceId("robot", "Control"));
		/*
		 * subscribe to events from the service so that we know when
		 * we can give the robot a next task
		 */
		if (subscribedServices.get(robotName) == null) {
			subscribedServices.put(robotName, new ArrayList<Service>());
			subscribedServices.get(robotName).add(serv);
			System.out.println(robotName + " " + serv.getServiceId().getId() + " added to eventlistener.");
		} else if (!subscribedServices.get(robotName).contains(serv)) {
			System.out.println(robotName + " " + serv.getServiceId().getId() + " added to eventlistener.");
			subscribedServices.get(robotName).add(serv);
		}

		Action moveForwardAction = null;
		for (Action action : serv.getActions()) {
			if (action.getInputArgument("Angle") != null) {
				moveForwardAction = action;
				break;
			}
		}
		/*
		 * we hebben onze gepaste action, deze is er zeker, want er
		 * is al semantisch bepaald dat deze robot dergelijke action
		 * heeft !!
		 */
		robotActionSequence.get(robotName).add(moveForwardAction);
		HashMap<String, Object> inputs = new HashMap<String, Object>();
		inputs.put("Angle", 90);
		robotActionInputSequence.get(robotName).add(inputs);
	}

	private int getResponse(UpnpActionInvocation invocation) {
		CallbackWrapper wrapper = new CallbackWrapper(invocation, null);
		ControlPoint.upnpService.getControlPoint().execute(wrapper);

		Object o = wrapper.getResponse();
		/*
		 * async call !!!! so we wait for the response
		 */

		ActionArgumentValue[] args = (ActionArgumentValue[]) o;
		int maxruns = 4;
		int teller = 0;
		while (o == null && teller < maxruns) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// System.out.println("trying to get the response again");
			o = wrapper.getResponse();
			args = (ActionArgumentValue[]) o;
			teller++;
		}
		return (Integer) args[0].getValue();
	}

	@Override
	public void setRobots(HashMap<String, ArrayList<String>> robots) {
		super.setRobots(robots);
		// updateRobotStates(new Boolean(false)); // assuming we use every
		// single
		// robot !!
		makeRobotsWithActions(robots);
	}

	/*
	 * in this function we get all the Action objects from the robot in the
	 * sequence that they should be executed. we also subscribe to each service
	 * of which the action is a part of, so that we can get events from them.
	 * These events give us information about the original robot, and wether we
	 * can start a next action execution or not.
	 */
	private void makeRobotsWithActions(HashMap<String, ArrayList<String>> robots) {
		try {

			String robotName = "";
			boolean controlAdded = false;
			for (String robot : robots.keySet()) {
				robotName = robot;
				System.out.println("robot : " + robot);
				Device dev = ControlPoint.deviceMappings.get(robotName);

				if (dev == null) {
					// robots.remove(robotName);
					System.out.println("here in making actions, no device got.");
				} else {
					System.out.println("got robot : " + dev.getDisplayString());
					/*
					 * TODO : overloop alle services, normaliter weten we de
					 * namespace niet ( robot )
					 */

					LinkedList<Action> actionList = new LinkedList<Action>();

					for (String actionString : robots.get(robotName)) {
						String[] serviceAction = actionString.split("_");
						org.teleal.cling.model.meta.Service serv = dev.findService(new ServiceId("robot", serviceAction[0]));

						System.out.println("got service : " + serv.getServiceId().getId());

						/*
						 * TODO : overloop alle actions, normaliter weten we de
						 * namespace niet ( robot )
						 */
						Action action = serv.getAction(serviceAction[1]);
						System.out.println("got action : " + action.getName());

						actionList.add(action);
						System.out.println("ADDING ACTION TO LIST OF " + robotName + " action -> " + action.getName());
						/*
						 * invokeer actie, gebeurt reeds asynchroon TODO :
						 * roombabuffer voor commands ??
						 */
					}// voor elke action
					robotTasks.put(robotName, actionList);
					/*
					 * init de inputs met de robotnamen en actienamen
					 */
					HashMap<String, HashMap<String, Object>> actieOpInputsMap = new HashMap<String, HashMap<String, Object>>();
					for (Action action : actionList) {
						actieOpInputsMap.put(action.getName(), new HashMap<String, Object>());

					}
					inputs.put(robotName, actieOpInputsMap);
				}
			}
		} catch (Exception ex) {
			System.out.println("something went wrong whilst fetching the robot, service and the action");
			ex.printStackTrace();
		}

	}

	private void updateRobotStates(Boolean value) {
		/*
		 * put the robots in the "available state"
		 */
		for (String robotName : robots.keySet()) {
			// TaskAI.robotStates.put(robotName, value);
		}
		/*
		 * update environment
		 */
		// TaskAI.updateEnvironment(robotName, value);

	}

	private void notifyTask(String robotName) {

		System.out.println(robotName + " has done his task, moving to next task");

		Action action = robotActionSequence.get(robotName).pop();
		// robotTasks.get(robotName).add(action); // eerste actie is om vanaf
		// zijn (random)start positie
		// naar zijn beginpositie te gaan
		// !!
		HashMap<String, Object> input = robotActionInputSequence.get(robotName).pop();
		/*
		 * werk cyclisch -> aantal punten x2 ( op elk punt moet gedraaid worden en vooruit gegaan worden )
		 * -> grootte van de cyclische buffer
		 */
		if(robotActionSequence.get(robotName).size() < 8){
			robotActionSequence.get(robotName).add(action);
			robotActionInputSequence.get(robotName).add(input);
		}
		for (String str : input.keySet()) {
			System.out.println("inputs : " + str + " -> " + input.get(str));
		}
		// System.out.println(robotName + " his next task will be "
		// + action.getName() + " timestamp : "+
		// GregorianCalendar.getInstance().getTime().getMinutes() +
		// GregorianCalendar.getInstance().getTime().getSeconds());
		executeAction(action, input, robotName);
	}

	@SuppressWarnings("rawtypes")
	private void executeAction(Action action, HashMap<String, Object> inputArguments, String robotName) {

		System.out.println("starting action !");
		@SuppressWarnings("rawtypes")
		UpnpActionInvocation invocation = new UpnpActionInvocation(action, inputArguments);
		final String kopieName = robotName;
		// Executes asynchronous in the background
		ControlPoint.upnpService.getControlPoint().execute(new ActionCallback(invocation) {

			@Override
			public void success(ActionInvocation invocation) {

				// assert invocation.getOutput().length == 0;
				// notifyTask(robotName);

				System.out.println(kopieName + " has successfully called " + invocation.getAction().getName() + " wait for the event to get next action !");
			}

			@Override
			public void failure(ActionInvocation invocation, UpnpResponse operation, String defaultMsg) {
				System.err.println(defaultMsg);
				// notifyTask(robotName);
			}
		});
	}

}
