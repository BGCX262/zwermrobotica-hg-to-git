package tasks.bo;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;

import upnp.UpnpActionInvocation;



public class CallbackWrapper extends ActionCallback{

		private Object response;
		
		private String outputName;
		
		public CallbackWrapper(UpnpActionInvocation invocation,String outputName){
			super(invocation);
			this.outputName = outputName;
		}
		
		@Override
		public void failure(ActionInvocation invocation,
				UpnpResponse operation, String defaultMsg) {
			System.out.println(defaultMsg);
			
		}

		@Override
		public void success(ActionInvocation invocation) {
			//assert invocation.getOutput().length == 0;
			
			//System.out.println("output : " + invocation.getOutput().length);
			response = invocation.getOutput().clone();
			
			//System.out.println("outputresponse : " + ((ActionArgumentValue[])response).length);
			System.out.println("Successfully called action : " + invocation.getAction().getName());
		}
		
		public Object getResponse(){			
			return response;
		}
					
	}
