package tasks.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.controlpoint.SubscriptionCallback;
import org.teleal.cling.model.action.ActionArgumentValue;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.gena.CancelReason;
import org.teleal.cling.model.gena.GENASubscription;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Action;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.state.StateVariableValue;
import org.teleal.cling.model.types.ServiceId;

import tasks.taskAI.TaskAI;
import upnp.ControlPoint;
import upnp.UpnpActionInvocation;

import AI.src.global.commands.robot.Command;
import AI.src.global.commands.robot.ForwardCommand;
import AI.src.global.commands.robot.RotateCommand;
import AI.src.global.commands.robot.SequenceCommand;
import AI.src.global.commands.robot.Command.Type;
import AI.src.pathfinding.PathModule;
import AI.src.pathfinding.algorithms.Path;
import AI.src.robot.AIModule;
import AI.src.robot.DataModel;

public class CheckOutPlaceTask extends Task {

	private static boolean singletonCreated = false;
	private static CheckOutPlaceTask singleton;
	private HashMap<String, LinkedList<Action>> robotTasks = new HashMap<String, LinkedList<Action>>();
	private HashMap<String, ArrayList<Service>> subscribedServices = new HashMap<String, ArrayList<Service>>();

	private ArrayList<SubscriptionCallback> eventCallbacks = new ArrayList<SubscriptionCallback>();

	private HashMap<String, HashMap<String, HashMap<String, Object>>> inputs = new HashMap<String, HashMap<String, HashMap<String, Object>>>();
	/*
	 * inputs are deprecated !!!!! new structures are below !
	 */
	HashMap<String, LinkedList<Action>> robotActionSequence = new HashMap<String, LinkedList<Action>>();
	HashMap<String, LinkedList<HashMap<String, Object>>> robotActionInputSequence = new HashMap<String, LinkedList<HashMap<String, Object>>>();

	private static PathModule module;
	private static DataModel dm;

	private int xCoord = 70;
	private int yCoord = 92;

	private CheckOutPlaceTask() {
		super(false, false, "CheckOutPlace");
		dm = DataModel.getInstance();
		module = DataModel.getInstance().getPathmodule();
		new AIModule();
		DataModel dm = DataModel.getInstance();
		PathModule module = dm.getPathmodule();
		/*
		 * adding contextparameters
		 */
		this.contextMultipliers.put("BatteryLife", 2);
		this.contextMultipliers.put("Speed", 3);
	}

	public static CheckOutPlaceTask init() {
		if (!singletonCreated) {
			singleton = new CheckOutPlaceTask();
		}
		return singleton;
	}

	public int getxCoord() {
		return xCoord;
	}

	public void setxCoord(int xCoord) {
		this.xCoord = xCoord;
	}

	public int getyCoord() {
		return yCoord;
	}

	public void setyCoord(int yCoord) {
		this.yCoord = yCoord;
	}

	@Override
	public void executeTask() {

		System.out.println("EXECUTING checkoutplace PATTERN !!");
		int numberOfRobots = robots.size();
		System.out.println("number of robots in task checkoutplace : " + numberOfRobots);

		try {
			System.out.println("calculating inputs for robots");
			calculateInputsForRobots(xCoord, yCoord);
			/*
			 * TODO putin the inputs for the playtune action
			 */
		} catch (Exception e) {
			System.out.println("something went wrong during calculations of coordinates");
			e.printStackTrace();
		}

		/*
		 * inschrijven op de service events
		 */
		for (String key : subscribedServices.keySet()) {
			for (Service serv : subscribedServices.get(key)) {
				try {
					System.out.println("subscribing to lifecycles");
					subscriptionLifecycle(serv);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		/*
		 * voor elke robot die voldoet, start de chain van acties
		 */
		for (String robotName : robotActionSequence.keySet()) {
			Action action = robotActionSequence.get(robotName).pop();
			// robotTasks.get(robotName).add(action); // eerste actie is om
			// vanaf zijn (random)start positie
			// naar zijn beginpositie te gaan
			// !!
			System.out.println("starting initial action for " + robotName);

			HashMap<String, Object> input = robotActionInputSequence.get(robotName).pop();
			executeAction(action, input, robotName);
		}
	}

	public void reset() {
		inputs = new HashMap<String, HashMap<String, HashMap<String, Object>>>();
		robotActionSequence = new HashMap<String, LinkedList<Action>>();
		robotActionInputSequence = new HashMap<String, LinkedList<HashMap<String, Object>>>();
		robotTasks = new HashMap<String, LinkedList<Action>>();
		subscribedServices = new HashMap<String, ArrayList<Service>>();
		/*
		 * nu nog de effectieve listeners
		 */
		for (SubscriptionCallback cb : eventCallbacks) {
			System.out.println("end subscriptions");
			cb.end();
		}
		eventCallbacks = new ArrayList<SubscriptionCallback>();
	}

	private void subscriptionLifecycle(Service serv) throws Exception {
		/*
		 * @params SubscriptionCallback : UPnPService, timeout : seconds
		 */

		System.out.println("subscribing to service ");
		SubscriptionCallback callback = new SubscriptionCallback(serv, 100) {

			@Override
			public void established(GENASubscription sub) {
				System.out.println("Established subscription : " + sub.getSubscriptionId());
			}

			@Override
			protected void failed(GENASubscription subscription, UpnpResponse responseStatus, Exception exception, String defaultMsg) {
				System.err.println("failed : " + defaultMsg + "from service with id : " + subscription.getService().getServiceId());

			}

			@Override
			public void ended(GENASubscription sub, CancelReason reason, UpnpResponse response) {
				System.out.println("subscription to event ended");

			}

			public void eventReceived(GENASubscription sub) {

				// System.out.println("Event received : " +
				// sub.getCurrentSequence().getValue());

				Map<String, StateVariableValue> values = sub.getCurrentValues();
				StateVariableValue status = values.get("Status");
				StateVariableValue robotName = values.get("Robot");

				if (status.toString().contains("Finished")) { // old value was
																// busy
					notifyTask(robotName.toString());
					System.out.println(robotName + " has been notified to execute his next task");
				} else if (status.toString().contains("Bumped")) {
					System.out.println("READ CHECKOUTPLACE-TASK !!");
					/*
					 * System.out .println("event received , status is : " +
					 * status); if (robotName != null)
					 * System.out.println("robotname : " + robotName);
					 */

				}
			}

			public void eventsMissed(GENASubscription sub, int numberOfMissedEvents) {
				System.out.println("Missed events: " + numberOfMissedEvents);
			}
		};
		ControlPoint.upnpService.getControlPoint().execute(callback); // asyn
																		// executions
																		// of
																		// events
		eventCallbacks.add(callback);
	}

	public void stopTask() {
		super.stopTask();
		this.reset();
		TaskAI.removeTask(taskName);
	}

	private void calculateInputsForRobots(int x, int y) throws Exception {

		int robotNummer = 0;

		for (String robotName : robotTasks.keySet()) {

			System.out.println("calcing");
			robotActionSequence.put(robotName, new LinkedList<Action>());
			robotActionInputSequence.put(robotName, new LinkedList<HashMap<String, Object>>());

			Device dev = ControlPoint.deviceMappings.get(robotName);
			Service context = dev.findService(new ServiceId("robot", "Context"));

			Action getX = null;
			Action getY = null;
			Action getAngle = null;

			for (Action action : context.getActions()) {
				if (action.getName().contains("XCoord")) {
					getX = action;
				} else if (action.getName().contains("YCoord")) {
					getY = action;
				} else if (action.getName().contains("Angle")) {
					getAngle = action;
				}
			}

			System.out.println("ACTION GET : " + getX);
			System.out.println("ACTION GET : " + getY);
			System.out.println("ACTION GET : " + getAngle);

			UpnpActionInvocation invocationx = new UpnpActionInvocation(getX, null);
			int robotx = getResponse(invocationx);

			UpnpActionInvocation invocationy = new UpnpActionInvocation(getY, null);
			int roboty = getResponse(invocationy);

			UpnpActionInvocation invocationAngle = new UpnpActionInvocation(getAngle, null);
			int robotAngle = getResponse(invocationAngle);

			dm.setX(robotx);
			dm.setY(roboty);
			dm.setAngle(robotAngle);

			/*
			 * ga naar doel positie
			 */
			System.out.println("path van " + dm.getX() + "," + dm.getY() + " naar " + x + "," + y);
			Path path = module.calculatePath(dm.getX(), dm.getY(), x, y);
			robotAngle = generateInputsForRobot(robotName, dev, path, robotAngle);
		}

	}

	@SuppressWarnings("rawtypes")
	private int generateInputsForRobot(String robotName, Device dev, Path path, int angle) throws Exception {

		if (path != null) {
			SequenceCommand comm = module.getSequenceRoomba(path);
			for (Command com : comm.getCommands()) {
				System.out.println(com.toCommandString());
				if (com.getType() == Type.FORWARD) {
					ForwardCommand fwd = (ForwardCommand) com;
					System.out.println(fwd.getDistance());
					/*
					 * zoek juiste Action die een distance als input nodig heeft
					 */
					org.teleal.cling.model.meta.Service serv = dev.findService(new ServiceId("robot", "Control"));

					/*
					 * subscribe to events from the service so that we know when
					 * we can give the robot a next task
					 */
					if (subscribedServices.get(robotName) == null) {
						subscribedServices.put(robotName, new ArrayList<Service>());
						subscribedServices.get(robotName).add(serv);
						System.out.println(robotName + " " + serv.getServiceId().getId() + " added to eventlistener.");

					} else if (!subscribedServices.get(robotName).contains(serv)) {
						System.out.println(robotName + " " + serv.getServiceId().getId() + " added to eventlistener.");
						subscribedServices.get(robotName).add(serv);
					}
					Action moveForwardAction = null;
					for (Action action : serv.getActions()) {
						if (action.getInputArgument("Distance") != null) {
							moveForwardAction = action;
							break;
						}
					}
					/*
					 * we hebben onze gepaste action, deze is er zeker, want er
					 * is al semantisch bepaald dat deze robot dergelijke action
					 * heeft !!
					 */
					if (moveForwardAction != null) {
						robotActionSequence.get(robotName).add(moveForwardAction);
						HashMap<String, Object> inputs = new HashMap<String, Object>();
						inputs.put("Distance", fwd.getDistance());
						robotActionInputSequence.get(robotName).add(inputs);
					}
				} else {
					RotateCommand rot = (RotateCommand) com;
					angle += rot.getAngle();
					System.out.println(rot.getAngle());
					/*
					 * zoek juiste Action die een distance als input nodig heeft
					 */
					org.teleal.cling.model.meta.Service serv = dev.findService(new ServiceId("robot", "Control"));
					/*
					 * subscribe to events from the service so that we know when
					 * we can give the robot a next task
					 */
					if (subscribedServices.get(robotName) == null) {
						subscribedServices.put(robotName, new ArrayList<Service>());
						subscribedServices.get(robotName).add(serv);
						System.out.println(robotName + " " + serv.getServiceId().getId() + " added to eventlistener.");
					} else if (!subscribedServices.get(robotName).contains(serv)) {
						System.out.println(robotName + " " + serv.getServiceId().getId() + " added to eventlistener.");
						subscribedServices.get(robotName).add(serv);
					}

					Action moveForwardAction = null;
					for (Action action : serv.getActions()) {
						if (action.getInputArgument("Angle") != null) {
							moveForwardAction = action;
							break;
						}
					}
					/*
					 * we hebben onze gepaste action, deze is er zeker, want er
					 * is al semantisch bepaald dat deze robot dergelijke action
					 * heeft !!
					 */
					robotActionSequence.get(robotName).add(moveForwardAction);
					HashMap<String, Object> inputs = new HashMap<String, Object>();
					inputs.put("Angle", rot.getAngle());
					robotActionInputSequence.get(robotName).add(inputs);
				}
			}
		} else {
			System.out.println("path null");
		}
		return angle % 360;
	}

	private int getResponse(UpnpActionInvocation invocation) {
		CallbackWrapper wrapper = new CallbackWrapper(invocation, null);
		ControlPoint.upnpService.getControlPoint().execute(wrapper);

		Object o = wrapper.getResponse();
		/*
		 * async call !!!! so we wait for the response
		 */

		ActionArgumentValue[] args = (ActionArgumentValue[]) o;
		int maxruns = 4;
		int teller = 0;
		while (o == null && teller < maxruns) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// System.out.println("trying to get the response again");
			o = wrapper.getResponse();
			args = (ActionArgumentValue[]) o;
			teller++;
		}
		return (Integer) args[0].getValue();
	}

	@Override
	public void setRobots(HashMap<String, ArrayList<String>> robots) {
		super.setRobots(robots);
		// updateRobotStates(new Boolean(false)); // assuming we use every
		// single
		// robot !!
		makeRobotsWithActions(robots);
	}

	/*
	 * in this function we get all the Action objects from the robot in the
	 * sequence that they should be executed. we also subscribe to each service
	 * of which the action is a part of, so that we can get events from them.
	 * These events give us information about the original robot, and wether we
	 * can start a next action execution or not.
	 */
	private void makeRobotsWithActions(HashMap<String, ArrayList<String>> robots) {
		try {

			String robotName = "";
			boolean controlAdded = false;
			for (String robot : robots.keySet()) {
				robotName = robot;
				System.out.println("robot : " + robot);
				Device dev = ControlPoint.deviceMappings.get(robotName);

				if (dev == null) {
					// robots.remove(robotName);
					System.out.println("here in making actions, no device got.");
				} else {
					System.out.println("got robot : " + dev.getDisplayString());
					/*
					 * TODO : overloop alle services, normaliter weten we de
					 * namespace niet ( robot )
					 */

					LinkedList<Action> actionList = new LinkedList<Action>();

					for (String actionString : robots.get(robotName)) {
						String[] serviceAction = actionString.split("_");
						org.teleal.cling.model.meta.Service serv = dev.findService(new ServiceId("robot", serviceAction[0]));

						System.out.println("got service : " + serv.getServiceId().getId());

						/*
						 * TODO : overloop alle actions, normaliter weten we de
						 * namespace niet ( robot )
						 */
						Action action = serv.getAction(serviceAction[1]);
						System.out.println("got action : " + action.getName());

						actionList.add(action);
						System.out.println("ADDING ACTION TO LIST OF " + robotName + " action -> " + action.getName());
						/*
						 * invokeer actie, gebeurt reeds asynchroon TODO :
						 * roombabuffer voor commands ??
						 */
					}// voor elke action
					robotTasks.put(robotName, actionList);
					/*
					 * init de inputs met de robotnamen en actienamen
					 */
					HashMap<String, HashMap<String, Object>> actieOpInputsMap = new HashMap<String, HashMap<String, Object>>();
					for (Action action : actionList) {
						actieOpInputsMap.put(action.getName(), new HashMap<String, Object>());

					}
					inputs.put(robotName, actieOpInputsMap);
				}
			}
		} catch (Exception ex) {
			System.out.println("something went wrong whilst fetching the robot, service and the action");
			ex.printStackTrace();
		}

	}

	private void notifyTask(String robotName) {

		System.out.println(robotName + " has done his task, moving to next task");
		if (!robotActionSequence.get(robotName).isEmpty()) {
			Action action = robotActionSequence.get(robotName).pop();
			// robotTasks.get(robotName).add(action); // eerste actie is om
			// vanaf
			// zijn (random)start positie
			// naar zijn beginpositie te gaan
			// !!
			HashMap<String, Object> input = robotActionInputSequence.get(robotName).pop();

			for (String str : input.keySet()) {
				System.out.println("inputs : " + str + " -> " + input.get(str));
			}
			// System.out.println(robotName + " his next task will be "
			// + action.getName() + " timestamp : "+
			// GregorianCalendar.getInstance().getTime().getMinutes() +
			// GregorianCalendar.getInstance().getTime().getSeconds());
			executeAction(action, input, robotName);
		} else {
			this.stopTask();
		}
	}

	@SuppressWarnings("rawtypes")
	private void executeAction(Action action, HashMap<String, Object> inputArguments, String robotName) {

		System.out.println("starting action !");
		@SuppressWarnings("rawtypes")
		UpnpActionInvocation invocation = new UpnpActionInvocation(action, inputArguments);
		final String kopieName = robotName;
		// Executes asynchronous in the background
		ControlPoint.upnpService.getControlPoint().execute(new ActionCallback(invocation) {

			@Override
			public void success(ActionInvocation invocation) {

				System.out.println(kopieName + " has successfully called " + invocation.getAction().getName() + " wait for the event to get next action !");
			}

			@Override
			public void failure(ActionInvocation invocation, UpnpResponse operation, String defaultMsg) {
				System.err.println(defaultMsg);

			}
		});
	}

}
