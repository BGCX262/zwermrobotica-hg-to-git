package ga;
import ga.bo.Robot;

import java.util.ArrayList;

import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.jgap.FitnessFunction;
import org.jgap.Gene;
import org.jgap.Genotype;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.impl.DefaultConfiguration;
import org.jgap.impl.IntegerGene;

public class Test {
	private static ArrayList<Robot> robots;
	private static ArrayList<String> tasks;
	private static ArrayList<String> parameters;
	private final static int ROBOT_COUNT = 10;
	private static final int MAX_ALLOWED_EVOLUTIONS = 1000;

	// TODO Instead of only using a ArrayList<String> tasks with the names of
	// the task, use a HashMap<String, Action> tasks.

	public static void main(String[] args) throws InvalidConfigurationException {
		final String[] TASKS = {"alarm", "moveForward", "rotate"};

		// Making the tasks.
		tasks = new ArrayList<String>();
		for (String task : TASKS) {
			tasks.add(task);
		}
		Robot.tasks = tasks;

		final String[] PARAMETERS = { "batteryLife", "speed" };

		// Making the parameters.
		parameters = new ArrayList<String>();
		for (String parameter : PARAMETERS) {
			parameters.add(parameter);
		}
		Robot.parameters = parameters;

		// TODO Make robots for the 'population'.
		robots = new ArrayList<Robot>();
		for (int i = 0; i < ROBOT_COUNT; i++) {
			//robots.add(new Robot());
			// Output robots.
//			System.out.println(robots.get(i).getRobotName());
//			System.out.println(robots.get(i));
		}

		// Make a new configuration for our Genotype aka population.
		Configuration configuration = new DefaultConfiguration();

		// Set the fitness function we want to use, which is our
		// RobotFitnessFunction.
		FitnessFunction fitnessFunction = new RobotFitnessFunction(robots);

		configuration.setFitnessFunction(fitnessFunction);

		// Now we need to tell the Configuration object how we want our
		// Chromosomes to be setup. We do that by actually creating a
		// sample Chromosome and then setting it on the Configuration
		// object. We want our Chomosone to have a gene for each task. The
		// values of these genes will be integers, representing a robot from the
		// swarm. We therefore use the IntegerGene class to represent each of
		// the genes. This gene holds the task as application data. That class
		// also lets us specify a lower and upper bound. This is used for
		// picking a valable robot from our swarm.
		Gene[] taskGenes = new Gene[tasks.size()];
		for (int i = 0; i < tasks.size(); i++) {
			IntegerGene taskGene = new IntegerGene(configuration, 0,
					ROBOT_COUNT - 1);
			taskGene.setApplicationData(tasks.get(i));
			taskGenes[i] = taskGene;
		}

		Chromosome chromosome = new Chromosome(configuration, taskGenes);

		configuration.setSampleChromosome(chromosome);

		// Finally, we need to tell the Configuration object how many
		// Chromosomes we want in our population. The more Chromosomes,
		// the larger the number of potential solutions (which is good
		// for finding the answer), but the longer it will take to evolve
		// the population each round. We'll set the population size to
		// 500 here.
		configuration.setPopulationSize(500);

		Genotype population = Genotype.randomInitialGenotype(configuration);

		for( int i = 0; i < MAX_ALLOWED_EVOLUTIONS; i++ )
		{
		    population.evolve();
		}
		
		IChromosome fittestChromosome = population.getFittestChromosome();
		
		System.out.println("FittestChromosome:");
		for (int i = 0; i < fittestChromosome.getGenes().length; i++) {
			// Output.
			System.out.println("\tTaskGene ("
					+ fittestChromosome.getGene(i).getApplicationData() + "):");
			System.out.println("\t\tAssignedRobot("
					+ robots.get((Integer) fittestChromosome.getGene(i).getAllele())
							.getRobotName() + ")");
			System.out.println("\t"
					+ robots.get((Integer) fittestChromosome.getGene(i).getAllele()));
		}
		System.out.println("ChromosomeFitness(" + fittestChromosome.getFitnessValue() + ")\n");
	}
}
