package ga.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Robot {
	public static ArrayList<String> parameters = new ArrayList<String>();
	public static ArrayList<String> tasks = new ArrayList<String>();

	private static int robotCount;
	private String robotName;
	private boolean isNothingRobot;

	private HashMap<String, Integer> parametersMap = new HashMap<String,Integer>(); //TODO : moet dit niet best een String,Object zijn
	private HashMap<String, Boolean> taskMap = new HashMap<String,Boolean>();

	private static Random random = new Random();

	public Robot(boolean nothingRobot) {
		setNothingRobot(nothingRobot);
		if (!nothingRobot) {
			setRobotName("Robot" + robotCount);
			robotCount++;

			// Randomising the tasks.
			taskMap = new HashMap<String, Boolean>();
			for (String task : tasks) {
				taskMap.put(task, random.nextBoolean());
			}

			// Randomising the parameters.
			// TODO Currently only randomised parameters.
			parametersMap = new HashMap<String, Integer>();
			for (String parameter : parameters) {
				parametersMap.put(parameter, random.nextInt(101));
			}
		} else {
			setRobotName("NothingRobot");

			// Randomising the tasks.
			taskMap = new HashMap<String, Boolean>();
			for (String task : tasks) {
				taskMap.put(task, false);
			}

			// Randomising the parameters.
			// TODO Currently only randomised parameters.
			parametersMap = new HashMap<String, Integer>();
			for (String parameter : parameters) {
				parametersMap.put(parameter, 0);
			}
		}
	}

	
	public void setParameters(HashMap<String,Integer> params){
		parametersMap = params;
		 for(String key : parametersMap.keySet()){
			 System.out.println("PARAMETERS FROM ROBOT : " + key + " -> " + parametersMap.get(key));
		 }
	}
	
	public Robot(boolean nothingRobot, String robotName){
		setNothingRobot(nothingRobot);
		if (!nothingRobot) {
			setRobotName(robotName);
			robotCount++;

			// Randomising the tasks.
			taskMap = new HashMap<String, Boolean>();
			for (String task : tasks) {
				taskMap.put(task, random.nextBoolean());
			}

			// Randomising the parameters.
			// TODO Currently only randomised parameters.
			parametersMap = new HashMap<String, Integer>();
			for (String parameter : parameters) {
				parametersMap.put(parameter, random.nextInt(101));
			}
		} else {
			setRobotName("NothingRobot");

			// Randomising the tasks.
			taskMap = new HashMap<String, Boolean>();
			for (String task : tasks) {
				taskMap.put(task, false);
			}

			// Randomising the parameters.
			// TODO Currently only randomised parameters.
			parametersMap = new HashMap<String, Integer>();
			for (String parameter : parameters) {
				parametersMap.put(parameter, 0);
			}
		}
	}
	
	public boolean isNothingRobot() {
		return isNothingRobot;
	}

	public void setNothingRobot(boolean isNothingRobot) {
		this.isNothingRobot = isNothingRobot;
	}

	
	public HashMap<String, Boolean> getTaskMap() {
		return taskMap;
	}

	public void setTaskMap(HashMap<String, Boolean> taskMap) {
		this.taskMap = taskMap;
	}
	
	public HashMap<String, Integer> getParameters() {
		return parametersMap;
	}

	public int getParameter(String parameterName) {
		return parametersMap.get(parameterName);
	}

	public void setRobotName(String robotName) {
		this.robotName = robotName;
	}

	public String getRobotName() {
		return robotName;
	}
	
	public boolean getTask(String taskName){		
		return taskMap.get(taskName);
	}

	@Override
	public String toString() {
		String outputString = "";
		for(String parameter : parametersMap.keySet()){
			outputString += "\t" + parameter + "(" + parametersMap.get(parameter) + ")";
		}
		outputString += "\n\t";
		for(String task : taskMap.keySet()){
			outputString += "\t" + task + "(" + taskMap.get(task) + ")";
		}
		return outputString;
	}
}
