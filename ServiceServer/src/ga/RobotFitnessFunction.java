package ga;

import ga.bo.Robot;

import java.util.ArrayList;
import java.util.HashMap;

import org.jgap.FitnessFunction;
import org.jgap.IChromosome;

import tasks.bo.Task;
import tasks.taskAI.TaskAI;

public class RobotFitnessFunction extends FitnessFunction {

	private static final long serialVersionUID = 2320643103117116691L;
	private ArrayList<Robot> robots;
	private HashMap<String, Robot> taskRobotMapping;

	@Override
	protected double evaluate(IChromosome chromosome) {
		taskRobotMapping = new HashMap<String, Robot>();
//		System.out.println("Chromosome:");
		for (int i = 0; i < chromosome.getGenes().length; i++) {
			// Output.
//			System.out.println("\tTaskGene ("
//					+ chromosome.getGene(i).getApplicationData() + "):");
//			System.out.println("\t\tAssignedRobot("
//					+ robots.get((Integer) chromosome.getGene(i).getAllele())
//							.getRobotName() + ")");
//			System.out.println("\t"
//					+ robots.get((Integer) chromosome.getGene(i).getAllele()));

			Integer alleleValue = (Integer) chromosome.getGene(i).getAllele();
			Robot tempRobot = robots.get(alleleValue);
			//System.out.println(alleleValue);
			taskRobotMapping.put((String) chromosome.getGene(i)
					.getApplicationData(), tempRobot);
		}

		double score = 0;
		// Calculate score via parameters.
		for(String taskName : taskRobotMapping.keySet()) {
			Robot robot = taskRobotMapping.get(taskName);
			for (String parameterName : Robot.parameters){
				
				// depending on the task a certain parameter gets a better score
				int addition = robot.getParameter(parameterName);
				Task task = TaskAI.getTask(taskName);
				if(task != null && task.getContextMultipliers().containsKey(parameterName)){
					addition*=task.getContextMultipliers().get(parameterName);
				}
				if(task.getIsPrior()){
					addition*=2;
				}
					score += addition;				
			}
		}

		for (String taskName : taskRobotMapping.keySet()) {
			Robot robot = taskRobotMapping.get(taskName);
			//System.out.println("getting taskname : " + taskName);
			if (robot.getTask(taskName)) {
				score += 1000;
			}
		}
//		System.out.println("ChromosomeFitness(" + score + ")\n");

		ArrayList<Robot> usedRobots = new ArrayList<Robot>();
		for(Robot robot : taskRobotMapping.values()){
			if(!robot.isNothingRobot() && !usedRobots.contains(robot)){
				usedRobots.add(robot);
			} else if(!robot.isNothingRobot() && usedRobots.contains(robot)){
				return 0;
			}
		}		
		return score;
	}

	public RobotFitnessFunction(ArrayList<Robot> robots) {
		super();
		this.robots = robots;
	}
}
