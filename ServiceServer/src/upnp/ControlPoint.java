package upnp;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.jgap.InvalidConfigurationException;
import org.mindswap.owl.OWLCache;
import org.mindswap.owl.OWLFactory;
import org.mindswap.owl.OWLKnowledgeBase;
import org.mindswap.owls.process.Process;
import org.mindswap.owls.service.Service;
import org.teleal.cling.UpnpService;
import org.teleal.cling.UpnpServiceImpl;
import org.teleal.cling.model.action.ActionArgumentValue;
import org.teleal.cling.model.message.header.STAllHeader;
import org.teleal.cling.model.meta.Action;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.DeviceDetails;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.model.types.ServiceId;
import org.teleal.cling.registry.DefaultRegistryListener;
import org.teleal.cling.registry.Registry;
import org.teleal.cling.registry.RegistryListener;

import tasks.bo.Match;
import tasks.taskAI.TaskAI;

import com.ibm.icu.util.Calendar;

/**
 * 
 * @author Jan
 */

public class ControlPoint {
	// AANPASSEN NAAR JOUW APACHE DIRECTORY!
	private static final String APACHE_DIMITRI = "C:\\Program Files\\Apache Tomcat 6.0.32\\webapps\\devicerepository\\";
	@SuppressWarnings("unused")
	private static final String APACHE_JAN = "C:\\Users\\Jan\\apache-tomcat-6.0.32\\webapps\\devicerepository\\";
	public static final String APACHE_DIRECTORY = APACHE_JAN;
	public static final String DEVICES_REPOSITORY = "http://localhost:9090/devicerepository/";
	private static FileWriter out;
	@SuppressWarnings("rawtypes")
	public static HashMap<String, Device> deviceMappings = new HashMap<String, Device>();

	public static final UpnpService upnpService = new UpnpServiceImpl();

	public ControlPoint() throws InterruptedException {

		clearLocalRepository();
		Runtime.getRuntime().addShutdownHook(new Thread() {

			@Override
			public void run() {
				upnpService.shutdown();
			}
		});

		// Add a listener for device registration events.
		upnpService.getRegistry().addListener(createRegistryListener(upnpService));

		// Broadcast a search message (STAllHeader) for all devices.
		upnpService.getControlPoint().search(new STAllHeader());
	}

	public static void main(String[] args) throws InterruptedException {
		ControlPoint cp = new ControlPoint();
	}

	private static void clearLocalRepository() {
		File dir = new File(APACHE_DIRECTORY);

		String[] owlFiles = dir.list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				if (name.contains(".owl")) {
					return true;
				} else {
					return false;
				}
			};
		});

		System.out.println("listing of owl-files in directory");
		for (String str : owlFiles) {
			String fullName = APACHE_DIRECTORY + str;
			File f = new File(fullName);
			f.delete();
		}
	}

	private static RegistryListener createRegistryListener(final UpnpService upnpService) {
		return new DefaultRegistryListener() {

			// Device comes online.
			@Override
			public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
				try {
					// Make unique string out of the device, needed to map the
					// device. We assume that every friendly name of a device is
					// unique.
					System.out.println("[DEBUG] Device " + device.getDisplayString() + " came online.");

					if (device.getDisplayString().contains("Roomba") || device.getDisplayString().contains("iRobot") ||
							device.getDisplayString().contains("Zeus") || device.getDisplayString().contains("Demeter")
							|| device.getDisplayString().contains("SecurityRadio")) {
						DeviceDetails details = device.getDetails();
						String friendlyName = details.getFriendlyName();

						// Add device to our registry and local mapping.
						registry.addDevice(device);
						deviceMappings.put(friendlyName, device);

						/*
						 * make the semantic mapping
						 */
						if (hasOWLFile(device)) {
							System.out.println("[DEBUG] Device " + device.getDisplayString() + " has OWL-files!");
							fetchOWLFiles(device);
							writeToLog("device " + device.getDisplayString() + " came online: " + GregorianCalendar.getInstance().getTimeInMillis());
						} else {
							System.out.println("[DEBUG] Device " + device.getDisplayString() + " doesn't have OWL-files!");
							makeOWLFiles(device);
							writeToLog("device " + device.getDisplayString() + " came online: " + GregorianCalendar.getInstance().getTimeInMillis());
						}
						/*
						 * update the environment for the taskAI
						 */

						TaskAI.updateEnvironment(true);
					}
				} catch (Exception ex) {
					System.out.println("[DEBUG] Exception (" + ex.getLocalizedMessage() + ").");
					ex.printStackTrace();
				}
			}

			@SuppressWarnings("rawtypes")
			private void fetchOWLFiles(RemoteDevice device) {
				System.out.println("[DEBUG] Fetching OWL-files of device " + device.getDisplayString() + ".");

				// The device has an method called hasOWL that returns true so
				// we can fetch the OWL-files. We assume there shall be an
				// action in the semanticService that is called getOWL.

				org.teleal.cling.model.meta.Service semanticService = device.findService(new ServiceId("robot", "Semantic"));

				Action getOWLAction = null;
				System.out.println("semantic service : " + semanticService.getServiceId().getId());
				for (Action action : semanticService.getActions()) {
					System.out.println("[DEBUG] Action " + action.getName() + " found!");
					if (action.getName().contains("ile")) {
						getOWLAction = action;
						System.out.println("OWLAction = " + getOWLAction);
						break;
					}
				}

				if (semanticService == null)
					System.out.println("[DEBUG] semanticService is null!");

				if (getOWLAction == null) {
					System.out.println("[DEBUG] getOWLAction is null!");
				} else {
					System.out.println("[DEBUG] getOWLAction = " + getOWLAction.getName());
				}

				// For each action in a service, different from semanticService,
				// we get the OWL-file. Thus we invoke getOWL with inputargument
				// actionName.

				String URL = null;

				for (org.teleal.cling.model.meta.Service service : device.getServices()) {
					// All services except SemanticService
					String serviceID = service.getServiceId().getId();
					if (!serviceID.equals("Semantic")) {
						System.out.println("[DEBUG] Service " + serviceID + " found!");
						// Foreach action in that service.
						for (Action action : service.getActions()) {
							System.out.println("[DEBUG] Action " + action.getName() + " found!");
							URL = device.getDetails().getFriendlyName() + "_" + serviceID + "_" + action.getName();
							try {
								System.out.println("OWLAction = " + getOWLAction);
								fetchOWLFile(getOWLAction, URL);
							} catch (InterruptedException e) {
								System.out.println("[DEBUG] IOException (" + e.getLocalizedMessage() + ".");
								e.printStackTrace();
							}
						}
					}
				}
			}

			@SuppressWarnings("rawtypes")
			private void fetchOWLFile(Action getOWLAction, String URL) throws InterruptedException {
				System.out.println("[DEBUG] Fetching OWL-file for " + URL + ".");

				HashMap<String, Object> inputs = new HashMap<String, Object>();
				inputs.put("OWLFileName", URL);
				UpnpActionInvocation getOWLActionInvocation = new UpnpActionInvocation(getOWLAction, inputs);

				CallbackWrapper callBackWrapper = new CallbackWrapper(getOWLActionInvocation, null);

				// Asynchronous call.
				upnpService.getControlPoint().execute(callBackWrapper);

				// Wait for response.
				Object o = callBackWrapper.getResponse();
				ActionArgumentValue[] args = (ActionArgumentValue[]) o;
				while (o == null) {
					Thread.sleep(100);
					o = callBackWrapper.getResponse();
					args = (ActionArgumentValue[]) o;
				}
				// getOWLAction finished.
				if (callBackWrapper.getResponse() != null) {
					System.out.println("[DEBUG] Succesfully found OWL-file for " + URL + ".");
					UpnpMapper.importOWLFile(URL, args[0].toString());
				} else {
					System.out.println("[DEBUG] Failed to fetch OWL-file for " + URL + ".");
				}
			}

			private void makeOWLFiles(RemoteDevice device) {
				System.out.println("[DEBUG] Making OWL-files for device " + device.getDisplayString() + ".");
				UpnpMapper.createOWLFiles(device);
			}

			@SuppressWarnings("rawtypes")
			private boolean hasOWLFile(Device device) throws InterruptedException {
				System.out.println("[DEBUG] Getting service and actions.");

				org.teleal.cling.model.meta.Service semanticService = null;

				for (org.teleal.cling.model.meta.Service service : device.getServices()) {

					// Looking for semanticService.
					if (service.getServiceId().getId().contains("Semantic")) {
						System.out.println("[DEBUG] Semantic service found.");
						semanticService = service;
					}
				}

				// If there is no semanticService this means this is not a
				// device of the swarm.

				Action hasOWLaction = null;
				if (semanticService != null) {
					for (Action action : semanticService.getActions()) {
						if (action.getName().contains("hasOWL")) {
							System.out.println("[DEBUG] hasOWL action found.");
							hasOWLaction = action;
						}
					}
				} else {
					return false;
				}

				if (hasOWLaction != null) {
					UpnpActionInvocation hasOWLinvocation = new UpnpActionInvocation(hasOWLaction, null);

					CallbackWrapper callBackWrapper = new CallbackWrapper(hasOWLinvocation, hasOWLaction.getName());

					// Asynchronous call.
					upnpService.getControlPoint().execute(callBackWrapper);

					// Wait for response.
					Object o = callBackWrapper.getResponse();
					ActionArgumentValue[] args = (ActionArgumentValue[]) o;
					while (o == null) {
						Thread.sleep(100);
						o = callBackWrapper.getResponse();
						args = (ActionArgumentValue[]) o;
					}
					// hasOWL returns true.
					if ((Boolean) args[0].getValue()) {
						return true;
					}
					// hasOWL returns false.
					else {
						return false;
					}
				} else {
					return false;
				}
			}

			@Override
			public void remoteDeviceRemoved(Registry registry, RemoteDevice device) {
				System.out.println("[DEBUG] Device " + device.getDisplayString() + " went offline.");

				if (device.getDisplayString().contains("Roomba")) {
					// Remove device from our registry and local mapping.
					registry.removeDevice(device);
					deviceMappings.remove(device.getDetails().getFriendlyName());
					/*
					 * update the environment for the taskAI
					 */
					try {
						try {
							TaskAI.updateEnvironment(false);
						} catch (InvalidConfigurationException e) {

							e.printStackTrace();
						}
					} catch (URISyntaxException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					UpnpMapper.removeOWLFile(device);
				}
			}
		};
	}

	private static void makeCache(OWLKnowledgeBase knowledgeBase) {
		OWLCache cache = knowledgeBase.getReader().getCache();
		cache.addCachedFile("http://www.daml.org/services/owl-s/1.0/Profile.owl", new File("Profile.owl"));
		cache.addCachedFile("http://www.daml.org/services/owl-s/1.1/Service.owl", new File("Service.owl"));
		cache.addCachedFile("http://www.daml.org/services/owl-s/1.0/Grounding.owl", new File("Grounding.owl"));
		cache.addCachedFile("http://www.daml.org/services/owl-s/1.0/ActorDefault.owl", new File("ActorDefault.owl"));
		cache.addCachedFile("http://www.daml.org/services/owl-s/1.0/generic/Expression.owl", new File("Expression.owl"));
		cache.addCachedFile("http://www.daml.org/services/owl-s/1.0/generic/ObjectList.owl", new File("ObjectList.owl"));
		cache.addCachedFile("http://www.daml.org/services/owl-s/1.0/MindswapProfileHierarchy.owl", new File("MindswapProfileHierarchy.owl"));
	}

	public static HashMap<String, ArrayList<String>> isRobotQualified(final String robotName, ArrayList<Process> tasks) throws URISyntaxException, IOException {

		// Get the OWL-files in the ServiceServer.
		String cwd = System.getProperty("user.dir");

		File dir = new File(APACHE_DIRECTORY);

		String[] OWLFiles = dir.list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				if (name.contains(".owl") && name.contains(robotName)) {
					return true;
				} else {
					return false;
				}
			};
		});

		System.out.println("[DEBUG] OWLFiles matched for " + robotName + " in ServiceServer:");
		for (String fileName : OWLFiles) {
			System.out.println(fileName);
		}

		/*
		 * per robotname, the sequence of matches is also put in a list, the
		 * actions should ofc match the number of processes.
		 */
		HashMap<String, ArrayList<String>> matches = new HashMap<String, ArrayList<String>>();
		matches.put(robotName, new ArrayList<String>());
		OWLKnowledgeBase kb = OWLFactory.createKB();

		kb.getReader().getCache().setForced(true); // als iets gevonden
													// wordt in de cache
													// dan
													// zal hij ZEKER
													// niet
													// online gaan
													// zoeken
		kb.getReader().getCache().setLocalCacheDirectory("cache");
		/*
		 * niet nodig om de files nog toe te voegen, er werd geen cache gebruikt
		 * omdat de cache naast de jar moet meegegeven worden, het is maw niet
		 * juist om de cache in de jar op te nemen.
		 */
		makeCache(kb);

		kb.setReasoner("Pellet");

		for (Process process : tasks) {

			System.out.println("process : " + process.getLocalName());
			for (String str : OWLFiles) {

				String serviceURL = DEVICES_REPOSITORY + str;
				URI uri = new URI(serviceURL);
				Service service = kb.readService(uri);

				System.out.println("reading service : " + serviceURL);
				Process repoProcess = service.getProcess();

				int result = Match.compareProcesses(repoProcess, process, false);

				System.out.println("result of the match between " + process.getLocalName() + " and " + str + " is : " + result + " , 0 = exact match, 1 = subsume, 2 = relaxed , 3 = fail");

				/*
				 * voeg enkel correcte matches toe
				 */
				if (result == 0) {

					// verwijder .owl van de filenaam
					str = (String) str.subSequence(0, str.length() - 4);
					String[] parts = str.split("_");
					if (matches.get(parts[0]) == null)
						matches.put(parts[0], new ArrayList<String>());
					matches.get(parts[0]).add(parts[1] + "_" + parts[2]);
				}
			}
		} // for all processes

		if (matches.get(robotName).size() == tasks.size()) {
			return matches;
		} else {
			return null;
		}
	}
	
	public static HashMap<String, ArrayList<String>> testMatchingDuration(ArrayList<Process> tasks) throws URISyntaxException, IOException {

		// Get the OWL-files in the ServiceServer.
		String cwd = System.getProperty("user.dir");

		File dir = new File(APACHE_DIRECTORY);

		String[] OWLFiles = dir.list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				if (name.contains(".owl")) {
					return true;
				} else {
					return false;
				}
			};
		});

		//System.out.println("[DEBUG] OWLFiles matched for " + robotName + " in ServiceServer:");
		//for (String fileName : OWLFiles) {
			//System.out.println(fileName);
		//}

		/*
		 * per robotname, the sequence of matches is also put in a list, the
		 * actions should ofc match the number of processes.
		 */
		HashMap<String, ArrayList<String>> matches = new HashMap<String, ArrayList<String>>();
		matches.put("bleh", new ArrayList<String>());
		OWLKnowledgeBase kb = OWLFactory.createKB();

		kb.getReader().getCache().setForced(true); // als iets gevonden
													// wordt in de cache
													// dan
													// zal hij ZEKER
													// niet
													// online gaan
													// zoeken
		kb.getReader().getCache().setLocalCacheDirectory("cache");
		/*
		 * niet nodig om de files nog toe te voegen, er werd geen cache gebruikt
		 * omdat de cache naast de jar moet meegegeven worden, het is maw niet
		 * juist om de cache in de jar op te nemen.
		 */
		makeCache(kb);

		kb.setReasoner("Pellet");

		for (Process process : tasks) {

			System.out.println("process : " + process.getLocalName());
			for (String str : OWLFiles) {

				String serviceURL = DEVICES_REPOSITORY + str;
				URI uri = new URI(serviceURL);
				Service service = kb.readService(uri);

				System.out.println("reading service : " + serviceURL);
				Process repoProcess = service.getProcess();

				int result = Match.compareProcesses(repoProcess, process, false);

				System.out.println("result of the match between " + process.getLocalName() + " and " + str + " is : " + result + " , 0 = exact match, 1 = subsume, 2 = relaxed , 3 = fail");

				/*
				 * voeg enkel correcte matches toe
				 */
				if (result == 0) {

					// verwijder .owl van de filenaam
					str = (String) str.subSequence(0, str.length() - 4);
					String[] parts = str.split("_");
					if (matches.get(parts[0]) == null)
						matches.put(parts[0], new ArrayList<String>());
					matches.get(parts[0]).add(parts[1] + "_" + parts[2]);
				}
			}
		} // for all processes

		if (matches.get("bleh").size() == tasks.size()) {
			return matches;
		} else {
			return null;
		}
	}

	
	public static HashMap<String, ArrayList<String>> doSearchForMatch(ArrayList<Process> processes) throws URISyntaxException, IOException {

		// Get the OWL-files in the ServiceServer.
		String cwd = System.getProperty("user.dir");

		File dir = new File(APACHE_DIRECTORY);

		String[] OWLFiles = dir.list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				if (name.contains(".owl")) {
					return true;
				} else {
					return false;
				}
			};
		});

		System.out.println("[DEBUG] OWLFiles in ServiceServer:");
		for (String fileName : OWLFiles) {
			System.out.println(fileName);
		}

		/*
		 * per robotname, the sequence of matches is also put in a list, the
		 * actions should ofc match the number of processes.
		 */
		HashMap<String, ArrayList<String>> matches = new HashMap<String, ArrayList<String>>();

		OWLKnowledgeBase kb = OWLFactory.createKB();

		kb.getReader().getCache().setForced(true);
		kb.getReader().getCache().setLocalCacheDirectory("cache");
		/*
		 * niet nodig om de files nog toe te voegen, er werd geen cache gebruikt
		 * omdat de cache naast de jar moet meegegeven worden, het is maw niet
		 * juist om de cache in de jar op te nemen.
		 */
		makeCache(kb);

		kb.setReasoner("Pellet");

		for (Process process : processes) {

			System.out.println("process : " + process.getLocalName());
			for (String str : OWLFiles) {

				String serviceURL = DEVICES_REPOSITORY + str;
				URI uri = new URI(serviceURL);
				Service service = kb.readService(uri);

				System.out.println("reading service : " + serviceURL);
				Process repoProcess = service.getProcess();

				int result = Match.compareProcesses(repoProcess, process, false);

				System.out.println("result of the match between " + process.getLocalName() + " and " + str + " is : " + result + " , 0 = exact match, 1 = subsume, 2 = relaxed , 3 = fail");

				/*
				 * voeg enkel correcte matches toe
				 */
				if (result == 0) {

					// verwijder .owl van de filenaam
					str = (String) str.subSequence(0, str.length() - 4);
					String[] parts = str.split("_");
					if (matches.get(parts[0]) == null)
						matches.put(parts[0], new ArrayList<String>());
					matches.get(parts[0]).add(parts[1] + "_" + parts[2]);
				}
			}
		} // for all processes

		/*
		 * get all robots that have an equal amount of matching processes as the
		 * amount of 'tosearch'processes
		 */
		for (String str : matches.keySet()) {
			if (matches.get(str).size() != processes.size()) {
				matches.remove(str);
			}
		}

		return matches;
	}

	public synchronized static void writeToLog(String input) {
		try {
			out = new FileWriter("LogControl.txt", true);
			out.append(input + "\n");
			out.close();
		} catch (IOException ex) {
			System.out.println("COULD NOT WRITE TO FILE");
		}
	}
}
