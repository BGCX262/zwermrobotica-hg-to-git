package upnp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.teleal.cling.model.meta.Action;
import org.teleal.cling.model.meta.ActionArgument;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.types.Datatype;

public class UpnpMapper {

	
	public static void importOWLFile(String fileName, String content){
		/*
		 * build filename -> unique devicename, service and action
		 */
		fileName+=".owl";
		String pathFile = ControlPoint.APACHE_DIRECTORY + fileName;
		
		
		File f = new File(pathFile);
		f.delete(); // we dont care about the result, either way after this command
		// there will be no (more) .owl file with the same name
		boolean succes = false;
		try {			
				succes = f.createNewFile();

				if (succes) {
					BufferedWriter out;					
					out = new BufferedWriter(new FileWriter(pathFile, true));
					out.write(content);
					out.close();
					System.out.println("creating new owl file : "
							+ pathFile + " was succesfully created");
					
				} else {
					System.out
							.println("creating new owl file : "
									+ pathFile
									+ " already exists, or something else went wrong.");
				}
			
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("could not create new file : "
					+ e.getMessage());
		}
		
	}
	
	@SuppressWarnings("rawtypes")
	public static void createOWLFiles(RemoteDevice device) {
		String URL = null;
		System.out.println("[DEBUG] Creating OWL-files for device "
				+ device.getDisplayString() + ".");

		// Voor elke actie van elke service van het device wordt een file
		// aangemaakt.
		for (Service service : device.getServices()) {

			String serviceID = service.getServiceId().getId();
			System.out.println("[DEBUG] Service " + serviceID + " found!");

			// SemanticService doens't need to be mapped
			if (!serviceID.contains("Semantic")) {
				for (Action action : service.getActions()) {

					System.out.println("[DEBUG] Action " + action.getName()
							+ " found!");
					URL = device.getDetails().getFriendlyName() + "_"
							+ serviceID + "_" + action.getName() + ".owl";

					String path = ControlPoint.APACHE_DIRECTORY + URL;
					File file = new File(path);

					try {
						if (file.createNewFile()) {
							System.out
									.println("[DEBUG] Succesfully created OWL-file "
											+ URL + ".");
							generateOWLFile(file, device.getDetails()
									.getFriendlyName(), serviceID, action, path);
						} else {
							System.out
									.println("[DEBUG] Error creating OWL-file "
											+ URL + ".");
						}
					} catch (IOException e) {
						System.out.println("[DEBUG] Couldn't create file ("
								+ path + ")");
						System.out.println(e.getMessage());
					}
				}
			}
		}
	}

	@SuppressWarnings({ "rawtypes", "unused" })
	private static void generateOWLFile(File file, String friendlyName,
			String serviceId, Action action, String path) {
		System.out.println("[DEBUG] Generating OWL-file.");
		BufferedWriter out;
		try {
			out = new BufferedWriter(new FileWriter(path, true));
			String nameFile = file.getName();
			String actionName = action.getName();

			out.append("<?xml version=\"1.0\"?>\n"
					+ "<!-- content toevoegen links voor wijzigingen -->\n"
					+ "<!DOCTYPE uridef [\n"
					+ "  <!ENTITY service \"http://www.daml.org/services/owl-s/1.1/Service.owl#\">\n"
					+ "  <!ENTITY profile \"http://www.daml.org/services/owl-s/1.1/Profile.owl#\">\n"
					+ "  <!ENTITY process \"http://www.daml.org/services/owl-s/1.1/Process.owl#\">\n"
					+ "  <!ENTITY grounding \"http://www.daml.org/services/owl-s/1.1/Grounding.owl#\">\n"
					+ "  <!ENTITY profileHierarchy \"http://services.com/DevicesHierarchy/owl/DevicesHierarchy.owl#\">\n "
					+ "  <!ENTITY swarm \"http://localhost:9090/Ontologie/SwarmRobotics.owl#\">\n "
					+ "]>\n" + "<rdf:RDF\n"
					+ "    xmlns=\"http://localhost:9090/devicerepository/"
					+ nameFile
					+ "\"\n "
					+ "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
					+ "    xmlns:process=\"http://www.daml.org/services/owl-s/1.1/Process.owl#\"\n"
					+ "    xmlns:list=\"http://www.daml.org/services/owl-s/1.1/ObjectList.owl#\"\n"
					+ "    xmlns:service=\"http://www.daml.org/services/owl-s/1.1/Service.owl#\"\n"
					+ "    xmlns:owl=\"http://www.w3.org/2002/07/owl#\"\n"
					+ "    xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"\n"
					+ "    xmlns:profile=\"http://www.daml.org/services/owl-s/1.1/Profile.owl#\"\n"
					+ "    xmlns:swrl=\"http://www.w3.org/2003/11/swrl#\"\n"
					+ "    xmlns:grounding=\"http://www.daml.org/services/owl-s/1.1/Grounding.owl#\"\n"
					+ "    xmlns:expression=\"http://www.daml.org/services/owl-s/1.1/Expression.owl#\"\n"
					+ "    xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"\n"
					+ "    xmlns:swarm=\"http://localhost:9090/Ontologie/SwarmRobotics.owl#\"\n"
					+ // TODO : path naar eigen ontologie ?
					"    xmlns:profileHierarchy=\"http://services.com/DevicesHierarchy/owl/DevicesHierarchy.owl#\"\n"
					+ "  xml:base=\"http://localhost:9090/devicerepository/"
					+ nameFile
					+ "\">\n "
					+ "  <!-- links aanpassen -->\n"
					+ "  <owl:Ontology rdf:about=\"\">\n"
					+ "    	<owl:imports rdf:resource=\"&service;\"/>\n"
					+ "    	<owl:imports rdf:resource=\"&profile;\"/>\n"
					+ "    	<owl:imports rdf:resource=\"&process;\"/>\n"
					+ "    	<owl:imports rdf:resource=\"&grounding;\"/>\n"
					+ "	<owl:imports rdf:resource=\"&swarm;\"/>\n"
					+ "	<owl:imports rdf:resource=\"&profileHierarchy;\"/>\n"
					+ "  </owl:Ontology>\n"
					+ "  "
					+ "  <!-- generic -->\n"
					+ "  <service:Service rdf:ID=\""
					+ serviceId
					+ "Service\">\n "
					+ "    <service:presents>\n"
					+ "      <profile:Profile rdf:ID=\""
					+ serviceId
					+ "Profile\"/>\n"
					+ "    </service:presents>\n"
					+ "    <service:describedBy>\n"
					+ "      <process:AtomicProcess rdf:ID=\""
					+ serviceId
					+ "Process\"/>\n"
					+ "    </service:describedBy>\n"
					+ "    <service:supports>\n"
					+ "      <grounding:WsdlGrounding rdf:ID=\""
					+ serviceId
					+ "Grounding\"/>\n"
					+ "    </service:supports>\n"
					+ "  </service:Service>\n"
					+ "  <!-- generic -->\n"
					+ "  <profileHierarchy:RoboticService rdf:about=\"#"
					+ serviceId
					+ "Profile\">\n"
					+ "    <service:presentedBy rdf:resource=\"#"
					+ serviceId
					+ "Service\"/>\n"
					+ "    <profile:serviceName>"
					+ serviceId
					+ "</profile:serviceName>\n"
					+ "    <profile:textDescription>"
					+ serviceId
					+ "</profile:textDescription>\n"
					+ "    <!--Input argument -->\n");

			for (ActionArgument argument : action.getInputArguments()) {
				String inputName = argument.getName();				
				
				out.append("<!-- input argument --> \n"
						+ "    <profile:hasInput>\n"
						+ "      <process:Input rdf:ID=\""
						+ inputName.toLowerCase()
						+ "\">\n"
						+ "        <rdfs:label>"
						+ inputName.toLowerCase()
						+ "</rdfs:label>\n"
						+ "        <process:parameterType rdf:datatype=\"http://www.w3.org/2001/XMLSchema#anyURI\"\n"
						+ "        >http://localhost:9090/Ontologie/SwarmRobotics.owl#"
						+ inputName + "</process:parameterType>\n"
						+ "      </process:Input>\n"
						+ "    </profile:hasInput>\n");
			}
			for (ActionArgument argument : action.getOutputArguments()) {
				String outputName = argument.getName();				
				Datatype type = argument.getDatatype();
				out.append("    <!--Output argument-->\n"
						+ "    <profile:hasOutput>\n"
						+ "      <process:Output rdf:ID=\""
						+ outputName.toLowerCase()
						+ "\">\n"
						+ "        <rdfs:label>"
						+ outputName.toLowerCase()
						+ "</rdfs:label>\n"
						+ "        <process:parameterType rdf:datatype=\"http://www.w3.org/2001/XMLSchema#anyURI\"\n"
						+ "        >http://www.w3.org/2001/XMLSchema#"
						+ type.getDisplayString()
						+ "</process:parameterType>\n"
						+ "      </process:Output>\n"
						+ "    </profile:hasOutput>\n");
			}
			out.append("  </profileHierarchy:RoboticService>\n");
			out.append("   <!--generic -->\n"
					+ "  <process:AtomicProcess rdf:about=\"#" + serviceId
					+ "Process\">\n" + "    <rdfs:label>" + serviceId
					+ "Process</rdfs:label>\n"
					+ "    <service:describes rdf:resource=\"#" + serviceId
					+ "Service\"/>\n" + "    <!-- Inputverwijzingen -->\n");
			for (ActionArgument argument : action.getInputArguments()) {
				String inputName = argument.getName();				
				Datatype type = argument.getDatatype();
				out.append("    <process:hasInput rdf:resource=\"#"
						+ inputName.toLowerCase() + "\"/>\n");
			}

			for (ActionArgument argument : action.getOutputArguments()) {
				String outputName = argument.getName();			
				Datatype type = argument.getDatatype();
				out.append("    <process:hasOutput rdf:resource=\"#"
						+ outputName.toLowerCase() + "\"/>\n");
			}

			out.append("  </process:AtomicProcess>\n"
					+ "  <grounding:WsdlGrounding rdf:about=\"#"
					+ serviceId
					+ "Grounding\">\n"
					+ "    <service:supportedBy rdf:resource=\"#"
					+ serviceId
					+ "Service\"/>\n"
					+ "    <grounding:hasAtomicProcessGrounding>\n"
					+ "      <grounding:WsdlAtomicProcessGrounding rdf:ID=\""
					+ serviceId
					+ "AtomicProcessGrounding\"/>\n"
					+ "    </grounding:hasAtomicProcessGrounding>\n"
					+ "  </grounding:WsdlGrounding>\n"
					+ "  <grounding:WsdlAtomicProcessGrounding rdf:about=\"#"
					+ serviceId
					+ "AtomicProcessGrounding\">\n"
					+ "    <grounding:owlsProcess rdf:resource=\"#"
					+ serviceId
					+ "Process\"/>\n"
					+ "    <grounding:wsdlOperation>\n"
					+ "      <grounding:WsdlOperationRef>\n"
					+ "        <grounding:operation rdf:datatype=\"http://www.w3.org/2001/XMLSchema#anyURI\"\n"
					+ "        >http://localhost:9090/MoveService/services/Move?wsdl#"
					+ serviceId
					+ "</grounding:operation>\n"
					+
					// TODO : MoveService wordt totaal niet gebruikt, zoals
					// elders al vermeld, kan het zijn dat er gewoon owl-files
					// kunnen gehost worden?
					"        <grounding:portType rdf:datatype=\"http://www.w3.org/2001/XMLSchema#anyURI\"\n"
					+ "        >http://localhost:9090/MoveService/services/Move?wsdl#MoveHttpSoap11Endpoint</grounding:portType>\n"
					+ "      </grounding:WsdlOperationRef>\n"
					+ "    </grounding:wsdlOperation>\n"
					+ "    <grounding:wsdlDocument rdf:datatype=\"http://www.w3.org/2001/XMLSchema#anyURI\"\n"
					+ "    >http://localhost:9090/MoveService/services/Move?wsdl</grounding:wsdlDocument>\n"
					+ // TODO : moet dit nog veranderen ?
					"    <grounding:wsdlInputMessage rdf:datatype=\"http://www.w3.org/2001/XMLSchema#anyURI\"\n"
					+ "    >http://ws.apache.org/axis2#"
					+ serviceId
					+ "Request</grounding:wsdlInputMessage>\n"
					+ "    <grounding:wsdlOutputMessage rdf:datatype=\"http://www.w3.org/2001/XMLSchema#anyURI\"\n"
					+ "    >http://ws.apache.org/axis2#"
					+ serviceId
					+ "Response</grounding:wsdlOutputMessage>\n");

			for (ActionArgument arg : action.getInputArguments()) {
				String argName = arg.getName();				
				out.append("    <grounding:wsdlInput>\n"
						+ "      <grounding:WsdlInputMessageMap>\n"
						+ "	<!-- Grounding linken Met owls parameters -->\n"
						+ "        <grounding:owlsParameter rdf:resource=\"#"
						+ argName.toLowerCase()
						+ "\"/>\n"
						+ "        <grounding:wsdlMessagePart rdf:datatype=\"http://www.w3.org/2001/XMLSchema#anyURI\"\n"
						+ "        >http://localhost:9090/MoveService/services/Move?wsdl#"
						+ argName.toLowerCase()
						+ "</grounding:wsdlMessagePart>\n"
						+ "	<!-- XSLT-transformatie INPUT-->\n"
						+ "	<grounding:xsltTransformationString>\n"
						+ " 	<![CDATA[\n"
						+ "		<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n"
						+ "		xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n "
						+ "		xmlns:swarm=\"http://localhost:9090/Ontologie/SwarmRobotics.owl#\">\n"
						+ "		<xsl:template match=\"//swarm:"
						+ argName
						+ "\">\n"
						+ "			<xsl:value-of select=\"swarm:has"
						+ argName
						+ "\"/>\n"
						+ "		</xsl:template>\n"
						+ "		</xsl:stylesheet>\n		"
						+ "	 ]]>				 \n "
						+ "	</grounding:xsltTransformationString>\n"
						+ "      </grounding:WsdlInputMessageMap>\n"
						+ "    </grounding:wsdlInput>\n");
			}

			for (ActionArgument arg : action.getOutputArguments()) {
				String argName = arg.getName();				
				out.append("    <grounding:wsdlOutput>\n"
						+ "      <grounding:WsdlOutputMessageMap>\n"
						+ "        <grounding:owlsParameter rdf:resource=\"#"
						+ argName.toLowerCase()
						+ "\"/>\n"
						+ "        <grounding:wsdlMessagePart rdf:datatype=\"http://www.w3.org/2001/XMLSchema#anyURI\"\n"
						+ "        >http://localhost:9090/MoveService/services/Move?wsdl#"
						+ argName.toLowerCase()
						+ "</grounding:wsdlMessagePart>\n"
						+ "      </grounding:WsdlOutputMessageMap>\n"
						+ "    </grounding:wsdlOutput>\n");

			}
			out.append("  </grounding:WsdlAtomicProcessGrounding>\n"
					+ "</rdf:RDF>");

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("rawtypes")
	public static void removeOWLFile(RemoteDevice device) {
		System.out.println("[DEBUG] Removing OWL-files for device "
				+ device.getDisplayString() + ".");
		String URL = null;

		// Voor elke actie van elke service van het device wordt een file
		// verwijderd.
		for (Service service : device.getServices()) {

			String serviceID = service.getServiceId().getId();

			System.out.println("[DEBUG] Service " + serviceID + " found!");
			// SemanticService doens't need to be mapped
			if (!serviceID.contains("SemanticService")) {
				for (Action action : service.getActions()) {

					System.out.println("[DEBUG] Action " + action.getName()
							+ " found!");
					URL = device.getDetails().getFriendlyName() + "_"
							+ serviceID + "_" + action.getName() + ".owl";

					String path = ControlPoint.APACHE_DIRECTORY + URL;
					File file = new File(path);

					if (file.delete()) {
						System.out
								.println("[DEBUG] Succesfully deleted OWL-file "
										+ URL + ".");
						generateOWLFile(file, device.getDetails()
								.getFriendlyName(), serviceID, action, path);
					} else {
						System.out.println("[DEBUG] Error deleting OWL-file "
								+ URL + ".");
					}
				}
			}
		}
	}
}
