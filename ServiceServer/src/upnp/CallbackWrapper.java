package upnp;

import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;

@SuppressWarnings("rawtypes")
public class CallbackWrapper extends ActionCallback {

	private Object response;
	private String outputName;
	
	public CallbackWrapper(UpnpActionInvocation invocation, String outputName){
		super(invocation);
		this.outputName = outputName;
	}
	
	@Override
	public void failure(ActionInvocation invocation,
			UpnpResponse operation, String defaultMsg) {
		System.out.println(defaultMsg);
		
	}
	
	@Override
	public void success(ActionInvocation invocation) {
		response = invocation.getOutput().clone();
		System.out.println("[DEBUG] Calling action ("+ outputName + ")!");
	}
	
	public Object getResponse(){			
		return response;
	}			
}
