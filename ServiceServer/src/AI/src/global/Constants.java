package AI.src.global;

/**
 * Interface with serveral enums so we don't
 * have to look everywhere to see which component
 * uses what constant. Also, switch-case, woo!
 * 
 * @author Everyone
 *
 */
public interface Constants {
	
	public String localURL = "tcp://127.0.0.1:61616";
		
	public String defaultURL = localURL;
	
	public boolean brokerPresent=true;
	
	public boolean logging=false;

	
	public enum DrawTypeIdentifier {
		GROUND,
		GRASS,
		SNOW,
		WATER,
		ROCK,
		FLAG_RED,
		FLAG_BLUE,
		SAND,
		TREE,
		HOUSE,
		BASE_GROUND_BLUE,
		BASE_GROUND_RED,
		DEFAULT		
	}
	
	/*
	 * Identifiers denoting if the message is high-level (logic, AI, client) 
	 * or low-level (robots)
	 */
	public enum Level {
		HIGH,
		LOW
	}
	
	/*
	 * The topics used in the event channel
	 */
	public enum Topic {
		
				
		// Anything to game logic
		GAMELOGIC,
			
		// High level orders (to game logic/AI/Mapping)
		ORDER_HIGH,

		// Low level orders (to robots)
		ORDER_LOW,

		// Low level status updates (to AI/Mapping(/logic?))
		STATUS_UPDATE_LOW, 
		
		// Map Information for AI
		MAPINFORMATION
		
	}		
	
	
	/*
	 * GameLogicMessage identifiers
	 * NOTE FOR GUI DEVS: When constructing the internal LejosEvent that will be passed
	 * to the CommModule for sending to the GameLogic, replace payLoad[0] with the 
	 * required GameLogicIdentifier. The CommModule will replace payLoad[0] with the
	 * client ID while creating the GameLogicMessage.
	 */
	public enum GameLogicIdentifier {
		/*
		 * Map information
		 */
		MAP_INFORMATION
	}
	
	/*
	 * OrderMessage identifiers
	 */
	public enum OrderIdentifier {

		/*
		 * HIGH  
		 */
		/*
		 * Highlevel goto
		 *
		 * payLoad[0] = (String) robot identifier
		 * payLoad[1] = een Command object bestemd voor robot (zie global.commands.high)
		 */
		
		GOTO_HIGH,
		
		/*
		 * LOW
		 */
		
		/*
		 * Sequence of commands
		 *
		 * payLoad[0] = (String) robot identifier
		 * payLoad[1] = een SequenceCommand object bestemd voor robot (zie global.commands.robot)
		 */
		SEQUENCE_COMMAND,
		
		/*
		 * A single command
		 *
		 * payLoad[0] = string (id of robot)
		 * payLoad[1] = een Command object bestemd voor robot (zie global.commands.robot)
		 */
		COMMAND
	}

	

}
