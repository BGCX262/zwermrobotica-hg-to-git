package AI.src.global.commands.robot;

public class ForwardCommand extends Command{

	private int distance;
	
	public ForwardCommand(int distance, int x_goal, int y_goal) {
		super(Command.Type.FORWARD,new Object[]{new Integer(distance),new Integer(x_goal),new Integer(y_goal)});
		this.distance = distance;
	}

	public int getDistance() {
		return distance;
	}
	
	
}
