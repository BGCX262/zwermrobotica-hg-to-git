package AI.src.global.commands.robot;

public class RotateCommand extends Command{

	private int angle;

	public RotateCommand(int angle) {
		super(Command.Type.ROTATE,new Object[]{new Integer(angle)});
		this.angle = angle;
	}

	public int getAngle(){
		return angle;
	}	
}
