package AI.src.global.commands.robot;

public class StopCommand extends Command{

	public StopCommand(){
		super(Command.Type.STOP, new Object[]{"STOP"});
	}
	
}
