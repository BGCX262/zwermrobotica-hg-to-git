package AI.src.global.commands.robot;

import java.io.Serializable;

public class Command implements Serializable{

	public static enum Type {
		GOTO,SEQUENCE,RESET,STOP,FORWARD,ROTATE,SEARCH
	}
	private Type type;
	protected Object[] args;
	
	public Command(Type type,Object[] args){
		this.type = type;
		this.args = args;
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @return the args
	 */
	public Object[] getArgs() {
		return args;
	}
	
	public String toCommandString(){
		return "CMD" + toSequenceString();
	}
	
	public String toSequenceString(){
		String robotString = "#" + type.toString();
		for(int i=0;i<args.length;i++){
			robotString += "#" + args[i];
		}
		return robotString;
	}
}
