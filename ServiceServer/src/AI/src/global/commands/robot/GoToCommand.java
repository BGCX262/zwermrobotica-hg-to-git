package AI.src.global.commands.robot;
public class GoToCommand extends Command{

	public GoToCommand(float x, float y) {
		super(Command.Type.GOTO,new Object[]{new Float(x),new Float(y)});
	}
	
}
