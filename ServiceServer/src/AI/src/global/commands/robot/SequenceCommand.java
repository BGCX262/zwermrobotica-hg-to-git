package AI.src.global.commands.robot;


public class SequenceCommand extends Command{

	private Command[] cmds;

	public SequenceCommand(Command[] cmds) {
		super(Type.SEQUENCE, new String[] {});
		this.cmds = cmds;
	}
	
	public Command[] getCommands(){
		return cmds;
	}
	
	public String toSequenceString(){
		String sequenceString = "CMD" + "#" + getType().toString() + "#" + cmds.length;
		for(Command cmd:cmds){
			sequenceString += cmd.toSequenceString();
		}
		return sequenceString;
	}
	
	private int curr_cmd = -1;
	
	/**
	 * Checks whether the queue is empty or not
	 * @return true if empty
	 */
	public boolean isEmpty() {
		return curr_cmd >= cmds.length;
	}
	/**
	 * Dequeues a command from the sequence queue
	 * @return the dequeued command
	 */
	public Command dequeueu() {
		curr_cmd++;
		Command cmd = curr_cmd >= cmds.length ? null : cmds[curr_cmd];
		return cmd;
	}
	@Override
	public Type getType() {
		return Type.SEQUENCE;
	}

}
