package AI.src.global.commands.robot;
public class ResetCommand extends Command{

	public ResetCommand(float x,float y,float heading){
		super(Command.Type.RESET,new Object[]{new Float(x),new Float(y),new Float(heading)});
	}
	
}
