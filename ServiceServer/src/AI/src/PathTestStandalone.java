package AI.src;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import AI.src.global.commands.robot.Command;
import AI.src.global.commands.robot.Command.Type;
import AI.src.global.commands.robot.ForwardCommand;
import AI.src.global.commands.robot.RotateCommand;
import AI.src.global.commands.robot.SequenceCommand;
import AI.src.pathfinding.PathModule;
import AI.src.pathfinding.algorithms.Path;
import AI.src.robot.AIModule;
import AI.src.robot.DataModel;


/**
 * 
 * @author nielsbouten
 *
 */

public class PathTestStandalone {

	private final static BufferedReader in =
		new BufferedReader(new InputStreamReader(System.in)) ;

	private static PathModule module;


	/**
	 * Entry point to our simple test game
	 * 
	 * @param argv The arguments passed into the game
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] argv) throws IOException, InterruptedException {

		execute();

		DataModel.getInstance();
		Thread.sleep(2000);

		module = DataModel.getInstance().getPathmodule();
		
		new AIModule();

		Thread.sleep(1000);
		
		String[] loc = {"250","250"};
		//GoToTest.execute(loc);
		//SearchTest.execute();
		
		float x = Float.parseFloat("250");
		float y = Float.parseFloat("250");
		DataModel dm = DataModel.getInstance();
		PathModule module = dm.getPathmodule();
		Path path = module.calculatePath(dm.getX(),dm.getY(), x, y);
		if(path!=null){								
			//dm.setSequenceCommand(module.getSequenceRoomba(path));
			SequenceCommand comm = module.getSequenceRoomba(path);
			for(Command com:comm.getCommands()){
				System.out.println(com.toCommandString());
				if(com.getType() == Type.FORWARD){
					ForwardCommand fwd = (ForwardCommand) com;
					System.out.println(fwd.getDistance());
				}	
				else {
					RotateCommand rot = (RotateCommand) com;
					System.out.println(rot.getAngle());
				}
			}		
		}
		else{
			System.out.println("path null");
		}

		
		while(true){
			Thread.sleep(200);
		}
		

	}

	public static void execute(){
		new PathTestStandalone();
	}
	

}
