package AI.src.robot.behavior;

import AI.src.global.commands.robot.Command;

public class RotateBehavior extends CommandBehaviorAdapter {
	
	/**
	 * Default constructor 
	 */
	public RotateBehavior() {
		super(Command.Type.ROTATE);
	}
	public void execute() {

		int angle = (Integer)myCommand.getArgs()[0];
		int start_angle = dm.getAngle();
		
		//dm.getRoombaComm().spin(angle);
		
		int stop_angle = start_angle + angle;
		dm.setAngle(stop_angle);
		System.out.println("Done rotating for " + angle + " to " + stop_angle);

	}
	public void suppress() {
		super.suppress();
	}	

}
