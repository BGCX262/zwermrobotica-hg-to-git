package AI.src.robot.behavior;

import AI.src.global.commands.robot.Command;


public class StopBehavior extends CommandBehaviorAdapter {

	public StopBehavior() {
		super(Command.Type.STOP);
	}
	public void execute() {
		//dm.getRoombaComm().stop();
	}
	public void suppress() {
		super.suppress();
	}
	
}
