package AI.src.robot.behavior;

import AI.src.global.commands.robot.Command;


/**
 * <p>This class provides the GoToBehavior associated with the GoToCommand,
 * for more information see the GoToCommand class.</p>
 * 
 * @author Enric Junqué de Fortuny
 * @date   Mar 5, 2010
 */

public class GoToBehavior extends CommandBehaviorAdapter {

	private static int stepsize = 2;
	
	/**
	 * Default constructor 
	 */
	public GoToBehavior() {
		super(Command.Type.GOTO);
	}
	public void execute() {
		
		/*float x = (Float)myCommand.getArgs()[0];
		float y = (Float)myCommand.getArgs()[1];

		//robot.goTo(destX, destY,true);	
		
		int j=0;
		float begin_x = dm.getCurrX();
		float begin_y = dm.getCurrY();
		float x_h = x-begin_x;
		float y_h = y-begin_y;
		float distance = (float) Math.sqrt((x_h*x_h)+(y_h*y_h));
		int number = (int) (distance/stepsize);
		
		while(!_suppressed && j<=number && x!=dm.getCurrX() && y!=dm.getCurrY()){
			
			float a = j*stepsize;
			float x_diff=0,y_diff=0;
			if(y_h!=0){
				y_diff = Math.abs((float) Math.sqrt((a*a)/((x_h*x_h)/(y_h*y_h)+1)));
				x_diff = Math.abs((float) x_h*y_diff/y_h);
				dm.setCurrX(begin_x + (x>begin_x?1:-1)*x_diff);
				dm.setCurrY(begin_y + (y>begin_y?1:-1)*y_diff);
			}else if(x_h!=0){
				x_diff = Math.abs((float) Math.sqrt((a*a)/((y_h*y_h)/(x_h*x_h)+1)));
				y_diff = Math.abs((float) y_h*x_diff/x_h);
				dm.setCurrX(begin_x + (x>begin_x?1:-1)*x_diff);
				dm.setCurrY(begin_y + (y>begin_y?1:-1)*y_diff);
			}
			int i=0;
			while(!_suppressed && i<10){
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					
				}
				i++;
			}
			j++;
		}
		//robot.updatePosition();*/
	}
	public void suppress() {
		super.suppress();
	}	

}
