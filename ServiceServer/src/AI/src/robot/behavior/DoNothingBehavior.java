package AI.src.robot.behavior;

/**
 * Do nothing, useful for behavior testing of other behaviors,
 * because an arbitrator won't execute the same behavior twice.
 * 
 * @author Enric Junqué de Fortuny
 * @date   Mar 5, 2010
 */
public class DoNothingBehavior extends BehaviorAdapter {
	public boolean takeControl() {
		return true;
	}
}

