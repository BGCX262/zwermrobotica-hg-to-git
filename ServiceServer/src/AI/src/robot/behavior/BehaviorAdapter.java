package AI.src.robot.behavior;

import AI.src.robot.Behavior;
import AI.src.robot.DataModel;


/**
 * This class provides a default implementation of a Behavior class and should 
 * be extended by all used behaviors. A behavior is used in conjunction with
 * an Arbitrator, for more information cfr. the lejos documentation or the 
 * subsumption architecture.
 * 
 * @author ciri
 */
public class BehaviorAdapter implements Behavior {
	protected DataModel dm = DataModel.getInstance(); 
	protected boolean _suppressed = true;

	/**
	 * Default constructor - does nothing.
	 */
	public BehaviorAdapter() {
	}

	/**
	 * This method is called in a control loop when the behavior is active, 
	 * please make sure that this method is non-blocking (ie. a runtime of 
	 * <200ms is preferred).
	 **/
	@Override
	public void action() {
		_suppressed = false;
		//Debug.debug("BehaviorAdapter.action");		
	}
	/**
	 * This method specifies what should happen when the behavior is suppressed
	 * or stopped. Make sure that you cancel out any behavior that you started.
	 * Eg. Stop the motors after starting them.
	 */
	@Override
	public void suppress() {
		//Debug.debug("BehaviorAdapter.suppress");		
		_suppressed = true;
	}
	/**
	 * This method should return . Note that returning true is not a guarantee
	 * for activation. Classes with higher priority are called in case of conflict.
	 * 
	 * @return boolean specifies whether the behavior wants to be active or not.
	 */
	@Override
	public boolean takeControl() {
		//Debug.debug("BehaviorAdapter.takeControl");
		return false;
	}
	
	/**
	 * @return true if suppressed
	 */
	public boolean isSuppressed() {
		return _suppressed;
	}
}
