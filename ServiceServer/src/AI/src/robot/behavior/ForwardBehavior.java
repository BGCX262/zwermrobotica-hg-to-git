package AI.src.robot.behavior;

import AI.src.global.commands.robot.Command;

public class ForwardBehavior extends CommandBehaviorAdapter {

	int status;

	/**
	 * Default constructor 
	 */
	public ForwardBehavior() {
		super(Command.Type.FORWARD);
	}
	public void execute() {

		int distance = (Integer)myCommand.getArgs()[0];
		int x = (Integer)myCommand.getArgs()[1];
		int y = (Integer)myCommand.getArgs()[2];

		//RoombaCommSerial roombacomm = dm.getRoombaComm();
		//roombacomm.goForward(distance*10);

		System.out.println("Done going forward for " + distance +" to " + x +"," +y);
		dm.setX(x);
		dm.setY(y);

	}
	public void suppress() {
		super.suppress();
	}	

}

