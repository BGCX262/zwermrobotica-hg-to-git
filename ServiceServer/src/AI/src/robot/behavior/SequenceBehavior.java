package AI.src.robot.behavior;

import AI.src.global.commands.robot.SequenceCommand;

/**
 * @see SequenceCommand this command does not follow regular
 * conventions.
 * 
 * @author Enric Junqué de Fortuny
 * @date   Mar 16, 2010
 */
public class SequenceBehavior extends BehaviorAdapter {
	/**
	 * Dequeues a command from the command queue and assigns it
	 * as the current command.
	 */
	public void action() {
		_suppressed = false;
		SequenceCommand seq_cmd = dm.getSequenceCommand();
		
		if(seq_cmd != null) {
			dm.setSingleCommand(seq_cmd.dequeueu());		
			if(seq_cmd.isEmpty())
				dm.setSequenceCommand(null);
		}
	}
	public boolean takeControl() {
		return dm.getSequenceCommand() != null && dm.getCommand() == null; 
	}

}
