package AI.src.robot.behavior;

import AI.src.global.commands.robot.Command.Type;


/**
 * Behavior to reset coordinates & heading to base location
 * 
 * @author nielsbouten
 *
 */
public class ResetBehavior extends CommandBehaviorAdapter {
	

	/**
	 * Default constructor 
	 */
	public ResetBehavior() {
		super(Type.RESET);
	}
	public void execute() {
		System.err.println("RESET" );
		float x = (Float)myCommand.getArgs()[0];
		float y = (Float)myCommand.getArgs()[1];
		float heading = (Float)myCommand.getArgs()[2];	
		//robot.setPose(x, y, heading);
		//dm.getPilot().reset();	
	}
	public void suppress() {
		super.suppress();
	}	
}
