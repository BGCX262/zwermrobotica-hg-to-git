package AI.src.robot.behavior;

import AI.src.global.commands.robot.Command;


/**
 * This class provides a default wrapper for behaviors that are to
 * be activated through user commands or a FLAG and provides a 
 * default takeControl() behavior .
 * 
 * Warning : do not use the same Command.Type in more than one
 * behavior
 * 
 * @author Enric Junqué de Fortuny
 * @date   Mar 16, 2010
 */
public abstract class CommandBehaviorAdapter extends BehaviorAdapter {
	protected Command.Type myType;
	protected Command myCommand;
	
	/**
	 * Do not use, this constructor only exists as a warning
	 * to programmers.
	 * 
	 * @throws Exception 
	 */
	public CommandBehaviorAdapter() throws Exception {
		throw new Exception("Default constructors should not be used for commands!");
	}
	
	/**
	 * Is to be initialized with the command's type.
	 * @param type command type
	 */
	public CommandBehaviorAdapter(Command.Type type) {
		this.myType = type;
	}
	
	/**
	 * Do not override!
	 */
	public void action() {
		super.action();
		myCommand = dm.getCommand();
		if(myCommand == null || myCommand.getType() != myType) return; //For thread safety
		
		execute();
		//robot.updatePosition();
		
		if(!_suppressed) {
			dm.setSingleCommand(null);
		}
	}
	/***
	 * This method executes the actual command. Should be
	 * non-blocking or check the _suppressed variable!
	 */
	public abstract void execute();
	
	public void suppress() {
		super.suppress();
		//robot.updatePosition();
	}
	/**
	 * Specifies that the action will take control if and only if the DataModel command flag is
	 * set to this behavior's type.
	 */
	public boolean takeControl() {
		super.takeControl();
		Command c = dm.getCommand();
		if( c == null ) return false;
		return (c.getType() == myType);
	}
}
