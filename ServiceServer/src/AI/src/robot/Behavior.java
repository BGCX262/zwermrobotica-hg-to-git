package AI.src.robot;


/**
* The Behavior interface represents an object embodying a specific behavior belonging 
* to a robot squad. Each behavior must define three things: <BR>
* <ol>
* 	<li>The circumstances to make this behavior seize control of the robot.
* 		e.g. When the touch sensor determines the robot has collided with an object.
*   </li>
*   <li> The action to exhibit when this behavior takes control. e.g. Back up and turn.
*   </li>
* 	<li>
* 		The actions to perform when another behavior has seized control from this
* 		behavior. e.g. Stop the current movement and update coordinates.
* 	</li>
* </ol>
* <p>These are represented by defining the methods takeControl(), action(),
* and suppress() respectively. </p>
* 
* <p>A behavior control system has one or more Behavior objects. When you have defined
* these objects, create an array of them and use that array to initialize an
* Arbitrator object.</p>
*
* @see Arbitrator
* @author <a href="mailto:bbagnall@escape.ca">Brian Bagnall</a>
* @version 0.1  27-July-2001
* 
* <P>Reuse for running squad behaviors on a server. Modified where I felt appropriate to do so.</P>
* 
* @author Enric Junque de Fortuny
* @version 0.1	01-April-2010
*/
public interface Behavior {
   
   /**
   * Returns a boolean to indicate if this behavior should seize control of the robot.
   * For example, a collision is inbound.

   * @return boolean Indicates if this Behavior should seize control.
   */
   public boolean takeControl();
   
   /**
   * The code in action() represents the actual action of the robot when this
   * behavior becomes active. Should divide the behavior in small subbehaviors
   * that are to be sent to each robot in the squad. 
   * 
   * <B>The contract for implementing this method is:</B><BR>
   * Any action can be started in this method. This method should not start a 
   * never ending loop. This method can return on its own, or when the suppress()
   * method is called; but it must return eventually. The action can run in
   * a seperate thread if the designer wishes it, and can therefore continue
   * running after this method call returns. 
   */
   public void action();
   
   /**
   * The code in suppress() should stop the current behavior. This can include
   * stopping a squad, or even calling methods to update internal data (such
   * as navigational coordinates). <BR>
   * 
   * <B>The contract for implementing this method is:</B><BR>
   * This method will stop the action running in this Behavior class. This method
   * will <I>not</I> return until that action has been stopped. It is acceptable for a 
   * delay to occur while the action() method finishes up.
   */
   public void suppress();
   
}