package AI.src.robot;

import AI.src.robot.behavior.DoNothingBehavior;
import AI.src.robot.behavior.ForwardBehavior;
import AI.src.robot.behavior.GoToBehavior;
import AI.src.robot.behavior.ResetBehavior;
import AI.src.robot.behavior.RotateBehavior;
import AI.src.robot.behavior.SearchBehavior;
import AI.src.robot.behavior.SequenceBehavior;
import AI.src.robot.behavior.StopBehavior;


/**
 * This class arbitrates the different behaviors. Currently only one class, could 
 * be extended to more classes for different game phases or for other different uses.
 * 
 * @author ciri
 */

public class RobotArbitrator extends Arbitrator {
		
	/**
	 * @param behaviorList list items with a lower index have a higher priority.
	 */
	public RobotArbitrator() {
		super(new Behavior[] {
				new DoNothingBehavior(),
				
				/* High-level - CMDS, ....*/			
				new SequenceBehavior(),
				new SearchBehavior(),
				new ForwardBehavior(),
				new RotateBehavior(),
				new GoToBehavior(),
				
				new StopBehavior(),
				new ResetBehavior()
		});
	}
}
