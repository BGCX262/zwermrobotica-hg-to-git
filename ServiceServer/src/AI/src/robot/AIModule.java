package AI.src.robot;


/**
 * 
 * Message to change phase:
 * 	PHA#{phasename} -> Message containing phase that is entered
 * 
 * CONFIG stage, possible messages to receive:
 *  LOC#{x_loc}#{y_loc}#{heading} -> Message containing the base location of the robot
 * 
 * GAME stage, possible messages to receive:
 * 
 * 	CMD#GOTO#{x_coord}#{y_coord} -> Command received from AI in middleware
 *  CMD#SEQ#{number}#{arraylength}#{COMMANDS} -> SequenceCommand received from AI in middleware
 *  CMD#RES#{x_loc}#{y_loc}#{heading} -> Message resetting location of the robot
 *  CMD#SHOT#{heading} -> Message for shooting command
 *  CMD#HIT#{velocity} -> Message for a hit command
 *  
 *  DIA#{a_diam}#{b_diam} -> Message for setting the diameter parameters of the robot
 * 
 * @author nielsbouten
 *
 */

public class AIModule  {


	public AIModule() {
		ArbitratorProcess ap = new ArbitratorProcess();
		ap.startProcess();
	}

}

class ArbitratorProcess implements Runnable {

	public void startProcess() {
		Thread t = new Thread(this);
		t.start();
	}

	public void run() {
		RobotArbitrator arb = new RobotArbitrator();
		arb.run();
	}

}

