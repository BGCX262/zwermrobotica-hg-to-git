package AI.src.robot;

import roombacomm.RoombaCommSerial;
import AI.src.global.commands.robot.Command;
import AI.src.global.commands.robot.SequenceCommand;
import AI.src.maps.MapParser;
import AI.src.pathfinding.PathModule;
import AI.src.util.map.MapInformation;
import AI.src.util.map.Types.VehicleType;
import AI.src.util.map.Vehicle;


public class DataModel {
	
	private static RoombaCommSerial roombaComm;
	private static String protocol;
	private static String serialPort;
	private static MapInformation mapinfo;
	private static Vehicle vehicle = new Vehicle(VehicleType.TANK,1);
	private static PathModule pathmodule;

	/**
	 * Needed to be able to return to base and set initial position
	 */
	private		   	int[] 			location;

	private			SequenceCommand		sequence_command;			
	private			Command				command;			

	/**
	 * Singleton constructor. 
	 */
	public DataModel() {
		this.mapinfo = MapParser.readMap("Default");
		this.pathmodule = new PathModule(mapinfo,vehicle);
		this.location = new int[3];
		this.location[0] = 0;
		this.location[1] = 0;
		this.location[2] = 0;
		//roombaStart();
	}
	
	private static DataModel myInstance;
	
	public static DataModel getInstance(){
		if (myInstance == null) myInstance = new DataModel();
		return myInstance;
	}	
	
	public RoombaCommSerial getRoombaComm(){
		return roombaComm;
	}


	/*******************************************************
	 * 
	 * 				Getters/Setters
	 * 
	 ******************************************************/
	
	/**
	 * Method returning location of the team base as [x_coord,y_coord,heading]
	 * @return baselocation
	 */
	public int[] getLocation() {
		return location;
	}
	
	public int getX(){
		return location[0];
	}
	
	public int getY(){
		return location[1];
	}
	
	public int getAngle(){
		return location[2];
	}
	
	public void setX(int x){
		location[0] = x;
	}
	
	public void setY(int y){
		location[1] = y;
	}
	
	public void setAngle(int angle){
		location[2] = angle;
	}
	
	public static Vehicle getVehicle() {
		return vehicle;
	}

	public static PathModule getPathmodule() {
		return pathmodule;
	}

	/**
	 * Method for setting the location of the base as [x_coord,y_coord,heading]
	 * @param baselocation
	 */
	public void setBaselocation(int x_loc,int y_loc, int heading) {
		this.location[0] = x_loc;
		this.location[1] = y_loc;
		this.location[2] = heading;
	}

	/**
	 * Method returning the current command in action.
	 * @return command
	 */
	public synchronized Command getCommand() {
		return command;
	}
	/**
	 * Method to set the current command (overrides previous one)
	 * remember to set the command arguments too!
	 * @param command
	 */
	public synchronized void setCommand(Command command){
		this.sequence_command = null;
		this.command = command;
	}
	/**
	 * Method to set the current command (overrides previous one)
	 * remember to set the command arguments too!
	 * @param command
	 */
	public synchronized void setSingleCommand(Command command) {
		this.command = command;
	}
	/**
	 * Method returning the current command in action.
	 * @return command
	 */
	public synchronized SequenceCommand getSequenceCommand() {
		return sequence_command;
	}

	/**
	 * Method to set the current command (overrides previous one)
	 * remember to set the command arguments too!
	 * @param sequence_command 
	 */
	public synchronized void setSequenceCommand(SequenceCommand sequence_command) {
		this.command = null;
		this.sequence_command = sequence_command;
	}

	
	// Startup function for communications with the Roomba.
	private static void roombaStart() {
		protocol = "OI";
		serialPort = "/dev/ttyUSB0";
		//serialPort = "COM5";
		roombaComm = new RoombaCommSerial();
		roombaComm.setProtocol(protocol);
		if (!roombaComm.connect(serialPort)) {
			System.out.println("[DEBUG] Can't connect to Roomba. Wrong port?");
			System.exit(9);
		}
		// Wake up signal.
		//roombaComm.port.setRTS(false);
		//roombaComm.pause(500);
		//roombaComm.port.setRTS(true);
		// Debugging.
		// roombaComm.debug = true;
		roombaComm.startup();
		roombaComm.pause(100);
		roombaComm.control();
		roombaComm.pause(100);
		roombaComm.pause(500);
		roombaComm.sensors();
		roombaComm.updateSensors();
	}

	
	
}
