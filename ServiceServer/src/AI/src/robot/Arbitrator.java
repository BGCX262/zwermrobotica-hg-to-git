package AI.src.robot;


/**
 * Arbitrator controls which behavior should currently be active in a behavior control system. 
 * 
 * @see SquadBehavior
 * @author <a href="mailto:bbagnall@escape.ca">Brian Bagnall</a>
 * @version 0.1  27-July-2001
 * 
 * Revised so the triggering condition is samples at all times 
 * for all behaviors.
 * @author Ole Caprani
 * @version 1.0 14.11.07
 * 
 * Adapted for usage in a server system as a top layer instead of
 * the intended target system (a robot).
 * 
 * @author Enric Junque de Fortuny
 * @version 2.0 04.1.10
 */
public class Arbitrator {

	private Behavior [] behaviors;
	private final int NONE = 99;
	private int currentBehavior;
	private BehaviorAction actionThread;
	private static int number;
	private int number_l;

	/**
	 * Allocates an Arbitrator object and initializes it with 
	 * an array of Behavior objects. The highest index in the 
	 * Behavior array will have the highest order behavior 
	 * level, and hence will suppress all lower level
	 * behaviors if it becomes active. The Behaviors in an 
	 * Arbitrator can not be changed once the arbitrator is 
	 * initialized.
	 * 
	 * Once the Arbitrator is initialized, the 
	 * method start() must be called to begin the arbitration.
	 * 
	 * @param behaviors An array of Behavior objects.
	 */
	public Arbitrator(Behavior [] behaviors) {
		this.behaviors = behaviors;
		currentBehavior = NONE;
		actionThread = new BehaviorAction();
		actionThread.start();
		this.number_l = number++;
	}

	private boolean running = true;
	
	/**
	 * stops the arbitrator
	 */
	public void stop() {
		running = false;
		actionThread.setDaemon(true);
		actionThread.interrupt();
	}
	
	/**
	 * This method starts the arbitration of Behaviors.
	 * Modifying the start() method is not recomended.
	 * Arbitrator does not run in a seperate thread, 
	 * and hence the start()method will never return.
	 */
	public void run() {

		while (running) {
			// Find triggered behavior with highest priority
			int highest = 0;
			for(int i=0;i< behaviors.length;i++) {        	
				if(behaviors[i].takeControl()) highest = i;
			}

			if ( currentBehavior == NONE | actionThread.done ) {
				// No active behavior, make highest priority behavior active
				currentBehavior = highest;
				actionThread.execute(highest);
			}
			else if ( highest == currentBehavior & actionThread.done) {
				// Only restart currentBehavior when it is done
				actionThread.execute(highest);
			}
			else if ( currentBehavior < highest){ 
				// A higher priority behavior is triggered while a lower priority
				//	behavior is active

				actionThread.interrupt();            	
				currentBehavior = highest;
				actionThread.execute(highest);
			}
			try {
				Thread.sleep(300);
			} catch(Exception e) {}
		}
	}

	/**
	 * This local thread perform the action() methods of the 
	 * behaviors.
	 */
	private class BehaviorAction extends Thread {
		public boolean done = true;
		int current = NONE;

		public void run() {
			while(true) {
				synchronized(this)
				{
					if(current != NONE) {
						done = false;
						behaviors[current].action();
						current = NONE;
						done = true;
					}
				}
				try {
					Thread.sleep(300);
				} catch(Exception e) {}
				Thread.yield();
			}
		}

		public synchronized void execute(int index) {
			current = index;
		}
	}
}
