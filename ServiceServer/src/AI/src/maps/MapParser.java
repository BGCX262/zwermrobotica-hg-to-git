package AI.src.maps;

import java.awt.Point;
import java.io.File;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import AI.src.util.map.GroundArea;
import AI.src.util.map.MapInformation;
import AI.src.util.map.Obstacle;
import AI.src.util.map.Types.GroundAreaType;

public class MapParser {

	private static String path = "";

	public static MapInformation readMap(String mapname) {
		MapInformation map = new MapInformation();

		try {

			File file = new File(path + mapname + ".xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();

			NodeList nodeLst = doc.getElementsByTagName("mapattributes");

			/*
			 * Read map attributes
			 */
			Element fstElmnt = (Element) nodeLst.item(0);
			NodeList fstNmElmntLst = fstElmnt.getElementsByTagName("name");
			Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
			NodeList fstNm = fstNmElmnt.getChildNodes();
			map.setName(((Node) fstNm.item(0)).getNodeValue());

			fstNmElmntLst = fstElmnt.getElementsByTagName("height");
			fstNmElmnt = (Element) fstNmElmntLst.item(0);
			fstNm = fstNmElmnt.getChildNodes();
			map.setHeight(Integer.parseInt((((Node) fstNm.item(0))
					.getNodeValue())));

			fstNmElmntLst = fstElmnt.getElementsByTagName("width");
			fstNmElmnt = (Element) fstNmElmntLst.item(0);
			fstNm = fstNmElmnt.getChildNodes();
			map.setWidth(Integer.parseInt((((Node) fstNm.item(0))
					.getNodeValue())));

			fstNmElmntLst = fstElmnt.getElementsByTagName("scalefactor");
			fstNmElmnt = (Element) fstNmElmntLst.item(0);
			fstNm = fstNmElmnt.getChildNodes();
			map.setScaleFactor(Double.parseDouble(((((Node) fstNm.item(0))
					.getNodeValue()))));

			/*
			 * Read the Obstacles
			 */

			nodeLst = doc.getElementsByTagName("obstacles");
			Element obstacles = (Element) nodeLst.item(0);
			NodeList obsLst = obstacles.getElementsByTagName("obstacle");
			for (int i = 0; i < obsLst.getLength(); i++) {
				Element obsElement = (Element) obsLst.item(i);
				Obstacle obs = new Obstacle();
				int xCoord = (int) Double
						.parseDouble((((Node) ((Element) obsElement
								.getElementsByTagName("topCoordX").item(0))
								.getChildNodes().item(0)).getNodeValue()));
				int yCoord = (int) Double
						.parseDouble((((Node) ((Element) obsElement
								.getElementsByTagName("topCoordY").item(0))
								.getChildNodes().item(0)).getNodeValue()));
				obs.setTopLeftCorner(new Point(xCoord, yCoord));
				obs.setHeight((int) Double
						.parseDouble((((Node) ((Element) obsElement
								.getElementsByTagName("height").item(0))
								.getChildNodes().item(0)).getNodeValue())));
				obs.setWidth((int) Double
						.parseDouble((((Node) ((Element) obsElement
								.getElementsByTagName("width").item(0))
								.getChildNodes().item(0)).getNodeValue())));
				map.addObstacle(obs);
			}

			/*
			 * Read the GroundAreas
			 */
			nodeLst = doc.getElementsByTagName("groundareas");
			Element gAreas = (Element) nodeLst.item(0);
			if (gAreas != null) {
				NodeList gAreaLst = gAreas.getElementsByTagName("groundarea");
				for (int i = 0; i < gAreaLst.getLength(); i++) {
					Element gAreaElement = (Element) gAreaLst.item(i);
					GroundArea groundArea = new GroundArea();
					int xCoord = (int) Double
							.parseDouble((((Node) ((Element) gAreaElement
									.getElementsByTagName("topCoordX").item(0))
									.getChildNodes().item(0)).getNodeValue()));
					int yCoord = (int) Double
							.parseDouble((((Node) ((Element) gAreaElement
									.getElementsByTagName("topCoordY").item(0))
									.getChildNodes().item(0)).getNodeValue()));
					groundArea.setTopCorner(new Point(xCoord, yCoord));
					groundArea.setHeight((int) Double
							.parseDouble((((Node) ((Element) gAreaElement
									.getElementsByTagName("height").item(0))
									.getChildNodes().item(0)).getNodeValue())));
					groundArea.setWidth((int) Double
							.parseDouble((((Node) ((Element) gAreaElement
									.getElementsByTagName("width").item(0))
									.getChildNodes().item(0)).getNodeValue())));
					groundArea.setName((((Node) ((Element) gAreaElement
							.getElementsByTagName("name").item(0))
							.getChildNodes().item(0)).getNodeValue()));
					groundArea.setType(GroundAreaType
							.valueOf((((Node) ((Element) gAreaElement
									.getElementsByTagName("type").item(0))
									.getChildNodes().item(0)).getNodeValue())));
					map.addGroundArea(groundArea);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/*
	 * Additional load methods
	 */

	/**
	 * 
	 * @return
	 */

	public static HashMap<String, String> getAllMaps() {

		HashMap<String, String> maps = new HashMap<String, String>();

		File dir = new File(path);
		String[] children = dir.list();
		for (int i = 0; i < children.length; i++) {
			if (children[i].endsWith(".xml")) {
				try {
					String name = children[i].replace(".xml", "");
					MapInformation mapInfo = readMap(name);
					maps.put(mapInfo.getName(), name);
				} catch (Exception e) {
					System.err.println("Problem finding or loading maps");
				}
			}
		}

		return maps;

	}

	public static HashMap<String, MapInformation> getAllMapInformation() {
		HashMap<String, MapInformation> maps = new HashMap<String, MapInformation>();

		File dir = new File(path);
		String[] children = dir.list();

		for (int i = 0; i < children.length; i++) {
			if (children[i].endsWith(".xml")) {
				try {
					String name = children[i].replace(".xml", "");
					MapInformation mapInfo = readMap(name);
					maps.put(mapInfo.getName(), mapInfo);
				} catch (Exception e) {
					System.err.println("Problem finding or loading maps");
				}
			}
		}

		return maps;
	}

}
