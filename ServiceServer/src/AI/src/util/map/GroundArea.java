package AI.src.util.map;

import java.awt.Point;

import AI.src.global.Constants.DrawTypeIdentifier;
import AI.src.util.map.Types.GroundAreaType;

public class GroundArea extends Area implements Drawable{
	
	private GroundAreaType type;
	
	private static final int PRIORITY = 80 ;

	
	private String name;
	
	public GroundArea(){
		super();
		this.name = "";
		this.type = GroundAreaType.DEFAULT;
	}
		
	public GroundArea(Point topCorner, int width, int height, String name, GroundAreaType type){
		super(topCorner, width, height);
		this.name = name;
		this.type = type;
	}
	public GroundArea(int topCornerX, int topCornerY, int width, int height, String name, GroundAreaType type){
		super(topCornerX, topCornerY, width, height);
		
		this.name = name;
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public GroundAreaType getType() {
		return type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(GroundAreaType type) {
		this.type = type;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public DrawTypeIdentifier getDrawTypeIdentifier() {
	
		// TODO : In before turbo
		return DrawTypeIdentifier.valueOf(this.type.toString());
		
	}

	@Override
	public int getDrawWidth() {
		
		return this.getWidth();
	}

	@Override
	public int getDrawX() {
	
		return this.getTopCornerXCoord();
	}

	@Override
	public int getDrawY() {
	
		return this.getTopCornerYCoord();
	}

	@Override
	public int getyDrawHeight() {
		
		return this.getHeight();
	}

	@Override
	public int getPriority() {
	
		return PRIORITY ;
		
	}
	public boolean equals(Object obj){
		
		try{
			GroundArea arrr = (GroundArea)obj;
			if((this.getHeight() == arrr.getHeight())&&(this.getWidth() == arrr.getWidth())&&(this.getTopCorner()==arrr.getTopCorner())&&(this.type==arrr.type)){
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			return false;
		}
		
		
	}

}
