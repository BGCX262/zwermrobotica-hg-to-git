package AI.src.util.map;

import java.awt.Point;

public class RobotLocation {
	
	public String name;
	
	public Point location;
	
	public float angle;

	public RobotLocation(){
		this.name = "";
		this.location = new Point(0, 0);
		this.angle = 0;
	}
	
	public RobotLocation(String name, Point loc, float angle){
		this.name = name;
		this.location = loc;
		this.angle = angle;
	}
	
	public RobotLocation(String name, int x, int y){
		this.name = name;
		this.location = new Point(x, y);
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the location
	 */
	public Point getLocation() {
		return location;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(Point location) {
		this.location = location;
	}

	/**
	 * @return the angle
	 */
	public float getAngle() {
		return angle;
	}

	/**
	 * @param angle the angle to set
	 */
	public void setAngle(float angle) {
		this.angle = angle;
	}
	
	

}
