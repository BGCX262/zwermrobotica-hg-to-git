package AI.src.util.map;

import java.util.ArrayList;
import java.util.HashMap;

public class Types {

	public enum VehicleType{TANK,SCOUT,INFANTRY,SQUAD};
	
	public enum ObstacleType{DEAD_ROBOT};
	
	private int VehicleWidth[] = {1,1,1,1};

	public enum GroundAreaType{SAND, WATER, TREE, SNOW, DEFAULT,OBSTACLE};

	private HashMap<VehicleType,ArrayList<GroundAreaType>> cancross;
	
	private static	Types myInstance;
	
	/**
	 * Singleton constructor. 
	 */
	private Types() {
		this.cancross = new HashMap<VehicleType,ArrayList<GroundAreaType>>();
		
		//LANDROBOT
		ArrayList<GroundAreaType> landrobot = new ArrayList<GroundAreaType>();
		landrobot.add(GroundAreaType.SAND);
		landrobot.add(GroundAreaType.DEFAULT);
		landrobot.add(GroundAreaType.SNOW);
		cancross.put(VehicleType.TANK, landrobot);
		
		//AMFEBIEROBOT
		ArrayList<GroundAreaType> amfebierobot = new ArrayList<GroundAreaType>();
		amfebierobot.add(GroundAreaType.SAND);
		amfebierobot.add(GroundAreaType.DEFAULT);
		amfebierobot.add(GroundAreaType.WATER);
		amfebierobot.add(GroundAreaType.SNOW);
		cancross.put(VehicleType.SCOUT, amfebierobot);
		
		//HELI
		ArrayList<GroundAreaType> helirobot = new ArrayList<GroundAreaType>();
		helirobot.add(GroundAreaType.TREE);
		helirobot.add(GroundAreaType.WATER);
		helirobot.add(GroundAreaType.SAND);
		helirobot.add(GroundAreaType.DEFAULT);
		helirobot.add(GroundAreaType.SNOW);
		cancross.put(VehicleType.INFANTRY, helirobot);
		
		//SQUAD
		ArrayList<GroundAreaType> squadrobot = new ArrayList<GroundAreaType>();
		squadrobot.add(GroundAreaType.SAND);
//		squadrobot.add(GroundAreaType.GRASS);
		squadrobot.add(GroundAreaType.DEFAULT);
		squadrobot.add(GroundAreaType.SNOW);
		cancross.put(VehicleType.SQUAD, squadrobot);
		
	}
	/**
	 * Singleton pattern.
	 * @return returns the existing Types or creates a new one otherwise
	 */
	public static Types getInstance() {
		if(myInstance == null) myInstance = new Types();
		return myInstance;
	}
	
	public boolean canCross(VehicleType v,GroundAreaType o){
		return cancross.get(v).contains(o);
	}
	
	public int getWidth(VehicleType v){
		return VehicleWidth[v.ordinal()];
	}
	
}
