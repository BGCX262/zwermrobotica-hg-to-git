package AI.src.util.map;

import java.io.Serializable;

import AI.src.util.map.Types.VehicleType;

/**
 * 
 * @author nielsbouten
 *
 */

public class Vehicle implements Serializable {


	private VehicleType type;	
	private int size;
	
	public Vehicle(VehicleType type,int size){
		this.type = type;
		this.size = size;
	}
	
	/**
	 * 
	 * @return
	 */
	public VehicleType getType(){
		return type;
	}
	
	public int getSize(){
		return size;
	}
	
}
