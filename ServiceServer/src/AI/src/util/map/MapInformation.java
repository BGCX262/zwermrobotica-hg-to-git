package AI.src.util.map;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public class MapInformation implements Serializable {
	
	private List<GroundArea> groundAreas;
			
	private List<Obstacle> obstacles;
		
	private int width;
	
	private int height;
	
	private String name;
	
	private double scaleFactor;
	
	
	 public MapInformation(){
		 this.width = 0;
		 this.height = 0;
		 this.name = "DEFAULT";
		 init();
	 }
	 
	 
	 public MapInformation(String name, int width, int height, String mapDescription, double scaleFactor){
			
		 this.width = width;
		 this.height = height;
		 this.name = name;
		 init();
		 this.scaleFactor = scaleFactor;
		
	 }
	 
	 public MapInformation(String name, int width, int height, String scenarioType, String mapDescription){
			
		 this.width = width;
		 this.height = height;
		 this.name = name;
		 init();
		
	 }
	 
	 public MapInformation(String name, int width, int height){
			
		 this.width = width;
		 this.height = height;
		 this.name = name;
		 init();		
	 }

	 
	 public MapInformation(String name, int width, int height, String mapDescription){
			
		 this.width = width;
		 this.height = height;
		 this.name = name;
		 init();		
	 }
	 
	 public void init(){
		 this.groundAreas = new ArrayList<GroundArea>(0);
		 this.obstacles = new ArrayList<Obstacle>(0);
		 this.scaleFactor = 1.0;
	 }
	 
	 /**
	  * @param obs the Obstacle to add
	  */
	 public void addObstacle(Obstacle obs){
		 obstacles.add(obs);
	 }
	 
	 /**
	  * @param obs the Obstacle to remove
	  */
	 public void removeObstacle(Obstacle obs){
		 obstacles.remove(obs);
	 }
	 
	 /**
	  * Clears all Obstacles
	  */
	 public void clearObstacles(){
		 obstacles.clear();
	 }
	 
	 /**
	  * @param area the GroundArea to add
	  */
	 public void addGroundArea(GroundArea area){
		 groundAreas.add(area);
	 }
	 
	 /**
	  * @param area the GroundArea to remove
	  */
	 public void removeGroundArea(GroundArea area){
		 groundAreas.remove(area);
	 }
	 
	 /**
	  * Clears all GroundAreas
	  */
	 public void clearGroundAreas(){
		 groundAreas.clear();
	 }


	/**
	 * @return the groundAreas
	 */
	public List<GroundArea> getGroundAreas() {
		return groundAreas;
	}


	/**
	 * @return the obstacles
	 */
	public List<Obstacle> getObstacles() {
		return obstacles;
	}


	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}


	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @param groundAreas the groundAreas to set
	 */
	public void setGroundAreas(List<GroundArea> groundAreas) {
		this.groundAreas = groundAreas;
	}

	/**
	 * @param obstacles the obstacles to set
	 */
	public void setObstacles(List<Obstacle> obstacles) {
		this.obstacles = obstacles;
	}


	/**
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}


	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}
	
	/**
	 * @param scaleFactor the scaleFactor to set
	 */
	public void setScaleFactor(double scaleFactor) {
		this.scaleFactor = scaleFactor;
	}

	/**
	 * @return the scaleFactor
	 */
	public double getScaleFactor() {
		return scaleFactor;
	}
	

	public List<Drawable> getAllDrawableObjectsOnMapExceptFlags(){
		
		ArrayList<Drawable> drawableObjects = new ArrayList<Drawable> ();
		drawableObjects.addAll(this.getGroundAreas());	
		return drawableObjects ;
		
	}

	public boolean equals(Object obj){
		
		try{
			
			MapInformation m = (MapInformation)obj;
			if((this.height == m.height) && (this.groundAreas == m.groundAreas) 
					&& (this.obstacles == m.obstacles)
					&& (this.width == m.width) 
					&&(this.name.equals(m.name)) && (this.scaleFactor == m.scaleFactor) 
					){
				return true;
			}else{
				return false;
			}
			
		}catch(Exception e){
			return false;
		}
	}	
}
