package AI.src.util.map;

import java.awt.Point;
import java.io.Serializable;

public abstract class Area implements Serializable{
	
	private Point topCorner;
	
	private int width;
	
	private int height;
	
	public Area(){
		this.topCorner = new Point(0, 0);
		this.width = 0;
		this.height = 0;
	}
	
	public Area(Point topCorner, int width, int height){
		this.topCorner = topCorner;
		this.width = width;
		this.height = height;
	}
	
	public Area(int topCornerX, int topCornerY, int width, int height){
		this.topCorner = new Point(topCornerX, topCornerY);
		this.width = width;
		this.height = height;
	}
	
	
	/**
	 * @return the topCorner
	 */
	public Point getTopCorner() {
		return topCorner;
	}
	
	/**
	 * @return the topCorner X coordinate
	 */
	public int getTopCornerXCoord() {
		return ((int)topCorner.getX());
	}
	
	/**
	 * @return the topCorner Y coordinate
	 */
	public int getTopCornerYCoord() {
		return ((int)topCorner.getY());
	}
	

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param topCorner the topCorner to set
	 */
	public void setTopCorner(Point topCorner) {
		this.topCorner = topCorner;

	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}
	public boolean equals(Object obj){
		
		try{
			Area arrr = (Area)obj;
			if((this.height == arrr.height)&&(this.width == arrr.width)&&(this.topCorner==arrr.topCorner)){
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			return false;
		}
		
		
	}

	

}
