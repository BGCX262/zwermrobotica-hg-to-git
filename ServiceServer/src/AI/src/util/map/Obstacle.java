package AI.src.util.map;

import java.awt.Point;
import java.io.Serializable;

import AI.src.util.map.Types.ObstacleType;

public class Obstacle implements Serializable{
	
	public Point topLeftCorner;
	
	public int width;
	
	public int height;
	
	public ObstacleType type;
	
	public Obstacle(){
		this.topLeftCorner = new Point(0, 0);
		this.width = 0;
		this.height = 0;
	}
	
	public Obstacle(Point topLeftCorner, int width, int height){
		this.topLeftCorner = topLeftCorner;
		this.width = width;
		this.height = height;
	}
	
	public Obstacle(int topLeftCornerX, int topLeftCornerY, int width, int height, ObstacleType type){
		this.topLeftCorner = new Point(topLeftCornerX, topLeftCornerY);
		this.width = width;
		this.height = height;
		this.type = type;
	}
	
	public Point getTopLeftCorner() {
		return topLeftCorner;
	}

	public void setTopLeftCorner(Point topLeftCorner) {
		this.topLeftCorner = topLeftCorner;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	public ObstacleType getType(){
		return type;
	}

	public boolean equals(Object obj){
		
		try{
			Obstacle arrr = (Obstacle)obj;
			if((this.height == arrr.height)&&(this.width == arrr.width)&&(this.topLeftCorner==arrr.topLeftCorner)&&(this.type==arrr.type)){
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			return false;
		}
		
		
	}
	
	

}
