package AI.src.util.map;

import AI.src.global.Constants.DrawTypeIdentifier;


public interface Drawable {

	
	public int getDrawX ();
	public int getDrawY() ;
	public int getDrawWidth();
	public int getyDrawHeight();
	
	public int getPriority();
	
	public DrawTypeIdentifier getDrawTypeIdentifier();
}
