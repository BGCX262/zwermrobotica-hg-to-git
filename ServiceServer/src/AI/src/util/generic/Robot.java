package AI.src.util.generic;

import java.io.Serializable;

public abstract class Robot implements Serializable {
	
	public String id;
	
	public String name;
	
	public int owner = -1;
	
	private float x;
	
	private float y;
	
	private float heading;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getOwner() {
		return owner;
	}
	public void setOwner(int owner) {
		this.owner = owner;
	}
	

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getHeading() {
		return heading;
	}

	public void setHeading(float heading) {
		this.heading = heading;
	}

	public boolean equals(Object arg0)
	{
		if(arg0 instanceof Robot) return this.id.equals(((Robot)arg0).id);
		else return false;
		
	}
	
	public abstract Robot clone();
	
	
}
