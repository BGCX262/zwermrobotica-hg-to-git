package AI.src.util.generic;

import java.io.Serializable;

import AI.src.util.map.Types.VehicleType;

public class RobotType implements Serializable {

	protected int attack,defense,range,health,cost;
	private VehicleType vehicletype;
	
	private RobotType(int attack,int defense,int range,int health,VehicleType type)
	{
		this.vehicletype = type;
		this.attack = attack;
		this.defense = defense;
		this.range = range;
		this.health = health;
		cost = calculateCost();
	}
	
	public RobotType(VehicleType type)
	{
		this.vehicletype = type;
		switch(type)
		{
			case TANK:
				this.attack = 10;
				this.defense = 5;
				this.range = 3;
				this.health = 400;
				break;
			case SCOUT:
				this.attack = 0;
				this.defense = 5;
				this.range = 5;
				this.health = 400;
				break;
			case INFANTRY:
				this.attack = 15;
				this.defense = 2;
				this.range = 1;
				this.health = 400;
				break;
		}
		cost = calculateCost();
	}
	
	public static RobotType createRobotType(String type)
	{
		System.out.println(type);

		if(type.equalsIgnoreCase("tank")) return new RobotType(VehicleType.TANK);
		if(type.equalsIgnoreCase("scout")) return new RobotType(VehicleType.SCOUT);
		if(type.equalsIgnoreCase("infantry")) return new RobotType(VehicleType.INFANTRY);

		return null;
		
	}
	
	public VehicleType getVehicleType(){
		return vehicletype;
	}
	
	private int calculateCost()
	{
		return (attack+defense+health+range*20);
	}

	public int getAttack() {
		return attack;
	}

	public int getDefense() {
		return defense;
	}

	public int getRange() {
		return range;
	}

	public int getHealth() {
		return health;
	}

	public int getCost() {
		return cost;
	}
	
}
