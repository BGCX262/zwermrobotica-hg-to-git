package AI.src.pathfinding.algorithms;

import AI.src.util.map.GroundArea;
import AI.src.util.map.MapInformation;
import AI.src.util.map.Obstacle;
import AI.src.util.map.Types;
import AI.src.util.map.Types.GroundAreaType;
import AI.src.util.map.Types.VehicleType;
import AI.src.util.map.Vehicle;


/**
 * The data map from our example game. This holds the state and context of each tile
 * on the map. It also implements the interface required by the path finder. It's implementation
 * of the path finder related methods add specific handling for the types of units
 * and terrain in the example game.
 * 
 * @author Kevin Glass : http://www.cokeandcode.com/pathfinding
 * @author Niels Bouten : adjustments
 */
public class GameMap implements TileBasedMap {
	/** The map width in tiles */
	public int WIDTH;
	/** The map height in tiles */
	public int HEIGHT;
	
	/** The terrain settings for each tile in the map */
	private GroundAreaType[][] terrain;

	/** Indicator if a given tile has been visited during the search */
	private boolean[][] visited;
	
	private Vehicle vehicle;

	private MapInformation mapinfo;
	
	private float scalefactor;
	
	/**
	 * Create a new test map with some default configuration
	 */
	public GameMap(Vehicle vehicle,MapInformation mapinfo){
		
		this.scalefactor = (int) mapinfo.getScaleFactor();
		
		this.WIDTH = (int) (mapinfo.getWidth()/this.scalefactor);
		this.HEIGHT = (int) (mapinfo.getHeight()/this.scalefactor);
		
		
		this.mapinfo = mapinfo;
		
		this.terrain = new GroundAreaType[WIDTH][HEIGHT];
		
		for(int i=0;i<WIDTH;i++){
			for(int j=0;j<HEIGHT;j++){
				this.terrain[i][j] = GroundAreaType.DEFAULT;
			}
		}

		this.visited = new boolean[WIDTH][HEIGHT];

		this.vehicle = vehicle;

		createMap();

	}
	
	public Vehicle getVehicle(){
		return vehicle;
	}
	
	/**
	 * Create the map from the obstacles given
	 */
	public void createMap(){
		for(GroundArea ob:mapinfo.getGroundAreas()){
			this.addObstacle(ob.getTopCornerXCoord(),
					ob.getTopCornerYCoord(),
					ob.getWidth(),
					ob.getHeight(),
					ob.getType());
			//fillArea(x_scaled,y_scaled,width_scaled,height_scaled,ob.getType());
		}
		for(Obstacle ob:mapinfo.getObstacles()){
			this.addObstacle((float)ob.getTopLeftCorner().getX(),
					(float)ob.getTopLeftCorner().getY(),
					ob.getWidth(),
					ob.getHeight(),
					GroundAreaType.OBSTACLE);
			//fillArea(x_scaled,y_scaled,width_scaled,height_scaled,Types.GroundAreaType.OBSTACLE);
		}
		//clearBorder();
	}
	
	public void clearBorder(){
		for(int i=0;i<WIDTH;i++){
			terrain[i][0] = GroundAreaType.OBSTACLE;
			terrain[i][HEIGHT-1] = GroundAreaType.OBSTACLE;
		}
		for(int i=0;i<HEIGHT;i++){
			terrain[0][i] = GroundAreaType.OBSTACLE;
			terrain[WIDTH-1][i] = GroundAreaType.OBSTACLE;
		}
	}
	
	public void addObstacle(float x,float y,float width,float height,GroundAreaType type){
		int size = vehicle.getSize();
		int x_scaled = (int)x/this.getScaleFactor();
		int y_scaled = (int)y/this.getScaleFactor();
		int width_scaled = (int)width/this.getScaleFactor();
		int height_scaled = (int)height/this.getScaleFactor();
		int x_begin = x_scaled - size;
		int y_begin = y_scaled - size;
		int left_padding = x_scaled > size ? size : x_scaled;
		int right_padding = x_scaled + width_scaled + size < WIDTH ? size : WIDTH-x_scaled-width_scaled;
		int top_padding = y_scaled > size ? size : y_scaled;
		int bottom_padding = y_scaled + height_scaled + size< HEIGHT ? size : HEIGHT-y_scaled-height_scaled;
		int x_end = width_scaled+ left_padding + right_padding;
		int y_end = height_scaled + top_padding + bottom_padding;
		fillArea(x_begin<0 ? 0:x_begin,
				y_begin<0 ? 0:y_begin,
						x_end>WIDTH-x_begin ? WIDTH-x_begin : x_end,
								y_end>HEIGHT-y_begin ? HEIGHT-y_begin : y_end,
										type);
	}

	/**
	 * Fill an area with a given terrain type
	 * 
	 * @param x The x coordinate to start filling at
	 * @param y The y coordinate to start filling at
	 * @param width The width of the area to fill
	 * @param height The height of the area to fill
	 * @param type The terrain type to fill with
	 */
	private void fillArea(int x, int y, int width, int height, GroundAreaType type) {
		for (int xp=x;xp<x+width;xp++) {
			for (int yp=y;yp<y+height;yp++) {
				terrain[xp][yp] = type;
			}
		}
	}
	
	/**
	 * Clear the array marking which tiles have been visted by the path 
	 * finder.
	 */
	public void clearVisited() {
		for (int x=0;x<WIDTH;x++) {
			for (int y=0;y<HEIGHT;y++) {
				visited[x][y] = false;
			}
		}
	}
	
	/**
	 * @see TileBasedMap#visited(int, int)
	 */
	public boolean visited(int x, int y) {
		return visited[x][y];
	}
	
	/**
	 * Get the terrain at a given location
	 * 
	 * @param x The x coordinate of the terrain tile to retrieve
	 * @param y The y coordinate of the terrain tile to retrieve
	 * @return The terrain tile at the given location
	 */
	public GroundAreaType getTerrain(int x, int y) {
		return terrain[x][y];
	}
	
	
	/**
	 * @see TileBasedMap#blocked(Mover, int, int)
	 */
	@Override
	public boolean blocked(int x, int y) {
		
		VehicleType v = vehicle.getType();
		GroundAreaType o = terrain[x][y];
		
		//TODO different types of vehicles on different types of terrain?
		
		return !Types.getInstance().canCross(v, o);
	}

	/**
	 * @see TileBasedMap#getCost(Mover, int, int, int, int)
	 */
	@Override
	public float getCost(int sx, int sy, int tx, int ty) {
		if(sx!=tx && sy!=ty){
			return (float) Math.sqrt(2.0);
		}
		else
			return 1;
	}

	/**
	 * @see TileBasedMap#getHeightInTiles()
	 */
	@Override
	public int getHeightInTiles() {
		return HEIGHT;
	}

	/**
	 * @see TileBasedMap#getWidthInTiles()
	 */
	@Override
	public int getWidthInTiles() {
		return WIDTH;
	}

	/**
	 * 
	 */
	public int getScaleFactor(){
		return (int)scalefactor;
	}
	
	/**
	 * @see TileBasedMap#pathFinderVisited(int, int)
	 */
	@Override
	public void pathFinderVisited(int x, int y) {
		visited[x][y] = true;
	}
	
	
}
