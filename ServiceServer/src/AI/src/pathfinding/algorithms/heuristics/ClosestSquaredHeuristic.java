package AI.src.pathfinding.algorithms.heuristics;

import AI.src.pathfinding.algorithms.AStarHeuristic;
import AI.src.pathfinding.algorithms.TileBasedMap;

/**
 * A heuristic that uses the tile that is closest to the target
 * as the next best tile. In this case the sqrt is removed
 * and the distance squared is used instead
 * 
 * @author Kevin Glass : http://www.cokeandcode.com/pathfinding
 * @author Niels Bouten : adjustments
 */
public class ClosestSquaredHeuristic implements AStarHeuristic {

	/**
	 * @see AStarHeuristic#getCost(TileBasedMap, Mover, int, int, int, int)
	 */
	@Override
	public float getCost(TileBasedMap map, int x, int y, int tx, int ty) {		
		float dx = tx - x;
		float dy = ty - y;
		
		return ((dx*dx)+(dy*dy));
	}

}
