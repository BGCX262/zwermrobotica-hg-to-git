package AI.src.pathfinding;

/**
 * One module for each robot or movement unit
 */


import java.util.ArrayList;

import AI.src.global.commands.robot.Command;
import AI.src.global.commands.robot.ForwardCommand;
import AI.src.global.commands.robot.GoToCommand;
import AI.src.global.commands.robot.RotateCommand;
import AI.src.global.commands.robot.SequenceCommand;
import AI.src.pathfinding.algorithms.AStarPathFinder;
import AI.src.pathfinding.algorithms.GameMap;
import AI.src.pathfinding.algorithms.Path;
import AI.src.pathfinding.algorithms.heuristics.ManhattanHeuristic;
import AI.src.robot.DataModel;
import AI.src.util.map.MapInformation;
import AI.src.util.map.Vehicle;


public class PathModule {
		
	private MapInformation mapinfo;
	
	private Vehicle vehicle;
	
	private GameMap gamemap;
	
	private int scalefactor;
	
	private AStarPathFinder pathfinder;
	
	//TODO scalefactor in mapInformation object
	public PathModule(MapInformation mapinfo,Vehicle vehicle){
		this.mapinfo = mapinfo;
		this.vehicle = vehicle;
		this.scalefactor = (int) mapinfo.getScaleFactor();
		createGameMap();
		createPathFinder();
	}
	
	/**
	 * Create the gamemap
	 */
	public void createGameMap(){
		gamemap = new GameMap(vehicle,mapinfo);
	}
	
	/**
	 * Create the pathfinder
	 */
	public void createPathFinder(){
		pathfinder = new AStarPathFinder(gamemap,500,true,new ManhattanHeuristic(1));
	}
	
	/**
	 * Returns the Path object (containing all the steps from one tile to a neighboring tile) for the path from (x,y) to (x_d,y_d)
	 * @param x
	 * @param y
	 * @param x2
	 * @param y2
	 * @return
	 */
	
	public Path calculatePath(int x,int y,double x2,double y2){
		int scaled_x = (int)(x/scalefactor);
		int scaled_y = (int)(y/scalefactor);
		int scaled_x_d = (int)(x2/scalefactor);
		int scaled_y_d = (int)(y2/scalefactor);
		gamemap.clearVisited();
		return pathfinder.findPath(scaled_x, scaled_y, scaled_x_d, scaled_y_d);
	}
	
	/**
	 * Returns a list of x,y coordinates for the path from (x,y) to (x_d,y_d)
	 * @param x
	 * @param y
	 * @param x_d
	 * @param y_d
	 * @return
	 */
	public synchronized int[][] getCoordinates(int x,int y,int x_d,int y_d){
		Path p=calculatePath(x,y,x_d,y_d);
		if(p != null)
			return toCoordinates(p);
		else
			return null;
	}
	
	/**
	 * Returns a sequence of GOTO commands for the path from (x,y) to (x_d,y_d)
	 * @param x
	 * @param y
	 * @param x_d
	 * @param y_d
	 * @return
	 */
	public SequenceCommand getSequenceGOTO(int x,int y,int x_d,int y_d){
		Path p=calculatePath(x,y,x_d,y_d);
		if(p != null)
			return getSequenceCommand(toCoordinates((p)));
		else
			return null;
	}
	
	/**
	 * Returns a sequence of GOTO commands for the path from (x,y) to (x_d,y_d)
	 * @param x
	 * @param y
	 * @param x_d
	 * @param y_d
	 * @return
	 */
	public SequenceCommand getSequenceGOTO(Path p){
		return getSequenceCommand(toCoordinates(p));
	}
	
	/**
	 * Creating a sequence of GOTO commands from a list of x-y coordinates
	 * @param coords
	 * @return
	 */
	public SequenceCommand getSequenceCommand(int[][] coords){
		Command[] cmds = new Command[coords.length];
		for(int i=0;i<coords.length;i++){
			cmds[i] = new GoToCommand(coords[i][0],coords[i][1]);
		}
		return new SequenceCommand(cmds);
	}
	
	public SequenceCommand getSequenceRoomba(Path path) {
		DataModel dm = DataModel.getInstance();

		int x_start = dm.getX();
		int y_start = dm.getY();
		int angle_start = dm.getAngle();
		int[][] coords = toCoordinates(path);
		Command[] cmds = new Command[coords.length*2];
		for(int i=0;i<cmds.length;i+=2){
			int x_goal = coords[i/2][0];
			int y_goal = coords[i/2][1];
			int distance = (int) Math.sqrt(Math.pow(x_goal-x_start,2)+Math.pow(y_goal-y_start,2));
			int x_dist = x_goal-x_start;
			int y_dist = y_goal-y_start;
			
			int angle_goal_tan = (int) (Math.atan2((double)y_dist, (double)x_dist) / (Math.PI) * 180);
			
			int angle_goal;
			/*if(y_dist >=0 && x_dist >=0)
				angle_goal = angle_goal_tan;
			else if(y_dist >=0 && x_dist <0 )
				angle_goal = (angle_goal_tan + 180) % 360;
			else if(y_dist <0 && x_dist <0)
				angle_goal = (angle_goal_tan + 270) % 360;
			else 
				angle_goal = (angle_goal_tan + 360) % 360;*/
			
			angle_goal = angle_goal_tan % 360;
			
			
			int rotate_angle = (int) (angle_goal-angle_start);
			
			if(rotate_angle > 180 || rotate_angle < -180){
				rotate_angle = - (rotate_angle%180);
			}
			
			cmds[i] = new RotateCommand(rotate_angle);
			cmds[i+1] = new ForwardCommand(distance,x_goal,y_goal);
			x_start = x_goal;
			y_start = y_goal;
			angle_start = angle_goal;
		}
		return new SequenceCommand(cmds);
	}
	
	/**
	 * DIAGONALRIGHT -> left top to right bottom corner
	 * DIAGONALLEFT -> right top to left vottom corner
	 */

	public enum MovementType{HORIZONTAL,VERTICAL,DIAGONALRIGHT,DIAGONALLEFT};

	/**
	 * 
	 * @return array with coordinates of path
	 */
	public int[][] toCoordinates(Path p){
		ArrayList<Integer> coords = new ArrayList<Integer>();
		int x_prev = p.getX(0);
		int y_prev = p.getY(0);
		MovementType mov_prev = null;
		for(int i=1;i<p.getLength();i++){

			int x_now = p.getX(i);
			int y_now = p.getY(i);

			MovementType mov_now = null;
			if(x_now > x_prev){
				if(y_now > y_prev){
					mov_now = MovementType.DIAGONALRIGHT;
				}else if(y_now < y_prev){
					mov_now = MovementType.DIAGONALLEFT;
				}else{
					mov_now = MovementType.HORIZONTAL;
				}
			}else if(x_now < x_prev){
				if(y_now > y_prev){
					mov_now = MovementType.DIAGONALLEFT;
				}else if(y_now < y_prev){
					mov_now = MovementType.DIAGONALRIGHT;
				}else{
					mov_now = MovementType.HORIZONTAL;
				}
			}else{
				mov_now = MovementType.VERTICAL;
			}
			if(mov_now!=mov_prev && mov_prev != null){
				coords.add(new Integer(scalefactor*x_prev));
				coords.add(new Integer(scalefactor*y_prev));
			}
			mov_prev = mov_now;
			x_prev = x_now;
			y_prev = y_now;
		}
		coords.add(new Integer(scalefactor*x_prev));
		coords.add(new Integer(scalefactor*y_prev));
		
		int[][]result = new int[coords.size()/2][2];
		
		for(int i=0;i<coords.size();i+=2){
			result[i/2][0] = coords.get(i);
			result[i/2][1] = coords.get(i+1);
		}
		return result;
	}

	/**
	 * @return the vehicle
	 */
	public Vehicle getVehicle() {
		return vehicle;
	}

	/**
	 * @return the gamemap
	 */
	public GameMap getGamemap() {
		return gamemap;
	}

	/**
	 * @return the scalefactor
	 */
	public float getScalefactor() {
		return scalefactor;
	}



}
