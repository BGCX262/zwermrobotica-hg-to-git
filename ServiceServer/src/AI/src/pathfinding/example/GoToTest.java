package AI.src.pathfinding.example;

import AI.src.pathfinding.PathModule;
import AI.src.pathfinding.algorithms.Path;
import AI.src.robot.DataModel;

public class GoToTest {


	public static void main(String [] args){
		execute(args);
	}

	public static void execute(String [] args){
		float x = Float.parseFloat("50");
		float y = Float.parseFloat("50");
		DataModel dm = DataModel.getInstance();
		PathModule module = dm.getPathmodule();
		Path path = module.calculatePath(dm.getX(),dm.getY(), x, y);

		if(path!=null){								
			dm.setSequenceCommand(module.getSequenceRoomba(path));
		}
		else{
			System.out.println("path null");
		}

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		x = Float.parseFloat("80");
		y = Float.parseFloat("80");
		path = module.calculatePath(dm.getX(),dm.getY(), x, y);

		if(path!=null){								
			dm.setSequenceCommand(module.getSequenceRoomba(path));
		}
		else{
			System.out.println("path null");
		}

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		x = Float.parseFloat("50");
		y = Float.parseFloat("50");
		path = module.calculatePath(dm.getX(),dm.getY(), x, y);

		if(path!=null){								
			dm.setSequenceCommand(module.getSequenceRoomba(path));
		}
		else{
			System.out.println("path null");
		}

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		x = Float.parseFloat("50");
		y = Float.parseFloat("0");
		path = module.calculatePath(dm.getX(),dm.getY(), x, y);

		if(path!=null){								
			dm.setSequenceCommand(module.getSequenceRoomba(path));
		}
		else{
			System.out.println("path null");
		}

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		x = Float.parseFloat("0");
		y = Float.parseFloat("0");
		path = module.calculatePath(dm.getX(),dm.getY(), x, y);

		if(path!=null){								
			dm.setSequenceCommand(module.getSequenceRoomba(path));
		}
		else{
			System.out.println("path null");
		}
	}




}
